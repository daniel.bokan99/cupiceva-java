package hr.fer.oprpp1.zemris.java.hw05.shell;

import java.util.Collections;
import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;

import hr.fer.oprpp1.zemris.java.hw05.shell.commands.CatShellCommand;
import hr.fer.oprpp1.zemris.java.hw05.shell.commands.CharsetsShellCommand;
import hr.fer.oprpp1.zemris.java.hw05.shell.commands.CopyShellCommand;
import hr.fer.oprpp1.zemris.java.hw05.shell.commands.ExitShellCommand;
import hr.fer.oprpp1.zemris.java.hw05.shell.commands.HelpShellCommand;
import hr.fer.oprpp1.zemris.java.hw05.shell.commands.HexdumpShellCommand;
import hr.fer.oprpp1.zemris.java.hw05.shell.commands.LsShellCommand;
import hr.fer.oprpp1.zemris.java.hw05.shell.commands.MkdirShellCommand;
import hr.fer.oprpp1.zemris.java.hw05.shell.commands.SymbolShellCommand;
import hr.fer.oprpp1.zemris.java.hw05.shell.commands.TreeShellCommand;

public class MyEnvironment implements Environment{
	
	private Scanner scanner;
	private SortedMap<String, ShellCommand> commands;
	private Character multilineSymbol;
	private Character morelinesSymbol;
	private Character promptSymbol;
	
	public MyEnvironment() {
		scanner = new Scanner(System.in);
		commands = new TreeMap<>();
		commands.put("exit", new ExitShellCommand());
		commands.put("ls", new LsShellCommand());
		commands.put("cat", new CatShellCommand());
		commands.put("charsets", new CharsetsShellCommand());
		commands.put("tree", new TreeShellCommand());
		commands.put("copy", new CopyShellCommand());
		commands.put("mkdir", new MkdirShellCommand());
		commands.put("hexdump", new HexdumpShellCommand());
		commands.put("help", new HelpShellCommand());
		commands.put("symbol", new SymbolShellCommand());
		
		multilineSymbol = '|';
		morelinesSymbol = '\\';
		promptSymbol = '>';
	}

	@Override
	public String readLine() throws ShellIOException {
		if(!scanner.hasNextLine())
			throw new ShellIOException("There is no next line");
		
		return scanner.nextLine();
	}

	@Override
	public void write(String text) throws ShellIOException {
		try {
			System.out.print(text);
		}catch(Exception e) {
			throw new ShellIOException();
		}
	}

	@Override
	public void writeln(String text) throws ShellIOException {
		try {
			System.out.println(text);
		}catch(Exception e) {
			throw new ShellIOException();
		}
	}

	@Override
	public SortedMap<String, ShellCommand> commands() {
		return Collections.unmodifiableSortedMap(commands);
	}

	@Override
	public Character getMultilineSymbol() {
		return multilineSymbol;
	}

	@Override
	public void setMultilineSymbol(Character symbol) {
		multilineSymbol = symbol;
	}

	@Override
	public Character getPromptSymbol() {
		return promptSymbol;
	}

	@Override
	public void setPromptSymbol(Character symbol) {
		promptSymbol = symbol;
	}

	@Override
	public Character getMorelinesSymbol() {
		return morelinesSymbol;
	}

	@Override
	public void setMorelinesSymbol(Character symbol) {
		morelinesSymbol = symbol;
	}

}
