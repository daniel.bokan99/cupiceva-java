package hr.fer.oprpp1.zemris.java.hw05.shell.commands;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.oprpp1.zemris.java.hw05.shell.Environment;
import hr.fer.oprpp1.zemris.java.hw05.shell.ShellCommand;
import hr.fer.oprpp1.zemris.java.hw05.shell.ShellStatus;

public class TreeShellCommand implements ShellCommand{
	
	private String commandName;
	private List<String> commandDescription;
	
	public TreeShellCommand() {
		commandName = "tree";
		commandDescription = new ArrayList<>();
		commandDescription.add("Tree command expects a single argument: directory name.");
		commandDescription.add("Command prints a tree.");
	}
	
	private static void print(File file, int depth, Environment env) {
		String str = String.format("%s%s%n", " ".repeat(depth * 2), file.getName());
		env.write(str);
		if(file.isDirectory()) {
			File[] children = file.listFiles();
			if(children == null) return;
			for(File c : children) {
				print(c, depth + 1, env);
			}
		}
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		
		if(arguments.startsWith("\"") && arguments.endsWith("\"")) {
			arguments = arguments.substring(1, arguments.length() - 1);
		}else {
			String[] args = arguments.split("\\s+");
			if(args.length != 1) {
				env.writeln("Illegal argument. Tree command needs to have one argument - path to a directory!");
				return ShellStatus.CONTINUE;
			}
		}
		
		File file = new File(arguments);
		
		if(file.isDirectory()) {
			print(file, 0, env);
		}else {
			env.writeln("File is not a directory. Cannot print tree!");
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return commandName;
	}

	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(commandDescription);
	}

}
