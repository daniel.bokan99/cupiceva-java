package hr.fer.oprpp1.zemris.java.hw05.shell.commands;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.oprpp1.crypto.Util;
import hr.fer.oprpp1.zemris.java.hw05.shell.Environment;
import hr.fer.oprpp1.zemris.java.hw05.shell.ShellCommand;
import hr.fer.oprpp1.zemris.java.hw05.shell.ShellStatus;

public class CatShellCommand implements ShellCommand{
	
	private String commandName;
	private List<String> commandDescription;
	
	public CatShellCommand() {
		commandName = "cat";
		commandDescription = new ArrayList<>();
		commandDescription.add("Cat takes one or two arguments");
		commandDescription.add("The first argument is path to some file and is mandatory");
		commandDescription.add("The second argument is charset name that should be used to interpret chars from bytes");
		commandDescription.add("If not provided, a default platform charset is used");
		commandDescription.add("This command opens given file and writes its content to console");
	}

	@SuppressWarnings("unused")
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		
		if(arguments == null) {
			env.write("Command cat must have at least one argument");
			return ShellStatus.CONTINUE;
		}
		
		String[] args;
		
		if(arguments.contains("\"")) {
			args = Util.parseArgs(arguments);
			if(args == null) {
				env.writeln("Invalid arguments");
				return ShellStatus.CONTINUE;
			}
		}else {
			args = arguments.split("\\s+");
		}
		
		if(args.length > 2) {
			env.writeln("Too many arguments. Try again.");
			return ShellStatus.CONTINUE;
		}
		String arg1 = args[0];
		String arg2 = null;
		if(args.length == 2) {
			arg2 = args[1];
		}else {
			arg2 = Charset.defaultCharset().toString();
		}
		
		File file = new File(arg1);
		if(file.exists()) {
			if(arg2 == null) {
				arg2 = Charset.defaultCharset().toString();
			}
			try(InputStreamReader is = new InputStreamReader(new FileInputStream(file), arg2)){
				int read = 1;
				char[] cbuf = new char[4096];
				while((read = is.read(cbuf)) != -1) {
					String str = new String(cbuf);
					env.writeln(str);
				}
			} catch (Exception e) {
				env.writeln("Exception while trying to read from a file");
			}
		}else {
			env.writeln("File does not exist");
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return commandName;
	}

	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(commandDescription);
	}

}
