package hr.fer.oprpp1.zemris.java.hw05.shell;

public enum ShellStatus {
	CONTINUE, TERMINATE
}
