package hr.fer.oprpp1.zemris.java.hw05.shell.commands;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.oprpp1.zemris.java.hw05.shell.Environment;
import hr.fer.oprpp1.zemris.java.hw05.shell.ShellCommand;
import hr.fer.oprpp1.zemris.java.hw05.shell.ShellStatus;

public class CharsetsShellCommand implements ShellCommand {
	
	private String commandName;
	private List<String> commandDescription;
	
	public CharsetsShellCommand() {
		commandName = "charsets";
		commandDescription = new ArrayList<>();
		commandDescription.add("Command lists names of supported charsets for your Java platform");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		
		if(arguments != null) {
			env.writeln("Charset command takes no arguments");
			return ShellStatus.CONTINUE;
		}
		
		var charsetColl = Charset.availableCharsets().values();
		for(var charset : charsetColl) {
			env.writeln(charset.displayName());
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return commandName;
	}

	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(commandDescription);
	}

}
