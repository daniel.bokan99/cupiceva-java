package hr.fer.oprpp1.zemris.java.hw05.shell.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.oprpp1.zemris.java.hw05.shell.Environment;
import hr.fer.oprpp1.zemris.java.hw05.shell.ShellCommand;
import hr.fer.oprpp1.zemris.java.hw05.shell.ShellStatus;

public class SymbolShellCommand implements ShellCommand{
	
	private String commandName;
	private List<String> commandDescription;
	
	public SymbolShellCommand() {
		commandName = "symbol";
		commandDescription = new ArrayList<>();
		commandDescription.add("Command takes one or two arguments.");
		commandDescription.add("First possible arguments are PROMPT, MULTILINE, MORELINES.");
		commandDescription.add("If there is only a single argument, current symbol for the first argument is printed out.");
		commandDescription.add("If the second argument is given then the first argument becomes the second given argument.");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		
		String[] args = arguments.split("\\s+");
		if(args.length == 1) {
			if(args[0].equals("PROMPT")) {
				env.writeln("Symbol for PROMPT is '" + env.getPromptSymbol() + "'");
			}else if(args[0].equals("MORELINES")) {
				env.writeln("Symbol for MORELINES is '" + env.getMorelinesSymbol() + "'");
			}else if(args[0].equals("MULTILINE")) {
				env.writeln("Symbol for MULTILINE is '" + env.getMultilineSymbol() + "'");
			}else {
				env.writeln("Invalid first argument.");
				return ShellStatus.CONTINUE;
			}
		}else if(args.length == 2) {
			if(args[0].equals("PROMPT")) {
				env.setPromptSymbol(args[1].charAt(0));
			}else if(args[0].equals("MORELINES")) {
				env.setMorelinesSymbol(args[1].charAt(0));
			}else if(args[0].equals("MULTILINE")) {
				env.setMultilineSymbol(args[1].charAt(0));
			}else {
				env.writeln("Invalid second argument.");
				return ShellStatus.CONTINUE;
			}
		}else {
			env.writeln("Invalid number of arguments!");
			return ShellStatus.CONTINUE;
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return commandName;
	}

	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(commandDescription);
	}

}
