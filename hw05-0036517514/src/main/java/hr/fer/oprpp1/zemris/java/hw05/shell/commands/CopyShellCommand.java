package hr.fer.oprpp1.zemris.java.hw05.shell.commands;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.oprpp1.crypto.Util;
import hr.fer.oprpp1.zemris.java.hw05.shell.Environment;
import hr.fer.oprpp1.zemris.java.hw05.shell.ShellCommand;
import hr.fer.oprpp1.zemris.java.hw05.shell.ShellStatus;

public class CopyShellCommand implements ShellCommand{
	
	private String commandName;
	private List<String> commandDescription;
	
	public CopyShellCommand() {
		commandName = "copy";
		commandDescription = new ArrayList<>();
		commandDescription.add("Command expects two arguments: source file and destination file name");
		commandDescription.add("If destionation file exists, you need to give the permission to overwrite it.");
		commandDescription.add("Copy command works only with files.");
		commandDescription.add("If the second argument is directory, it means that the file will be copied into the directory.");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		
		String[] args;
		
		if(arguments.contains("\"")) {
			args = Util.parseArgs(arguments);
			if(args == null) {
				env.writeln("Invalid arguments");
				return ShellStatus.CONTINUE;
			}
		}else {
			args = arguments.split("\\s+");
		}
		
		if(args.length != 2) {
			env.writeln("Invalid number of arguments");
		}
		
		String arg1 = args[0];
		String arg2 = args[1];
		
		File file1 = new File(arg1);
		if(!file1.exists()) {
			env.writeln("File that you're trying to copy does not exist");
			return ShellStatus.CONTINUE;
		}
		
		File file2 = new File(arg2);
		if(file2.exists() && !file2.isDirectory()) {
			env.writeln("Specified destination file already exits. Do you want to overwrite it?");
			env.writeln("Type anything for yes, N for no.");
			String response = env.readLine();
			if(response.equals("N")) {
				return ShellStatus.CONTINUE;
			}
			
		}
		
		if(file2.isDirectory()) {
			String name1 = file1.getName();
			String name = arg2 + name1;
			name = name.endsWith("\\") ? name : name + "\\";
			file2 = new File(name);
		}
		
		try(FileInputStream is = new FileInputStream(file1);
			FileOutputStream os = new FileOutputStream(file2)){
			
			byte[] data = new byte[4096];
			int read = is.read(data);
			
			while(read != -1) {
				os.write(data);
				read = is.read(data);
			}
			
		}catch(Exception e) {
			env.writeln("Error while trying to copy file");
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return commandName;
	}

	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(commandDescription);
	}

}
