package hr.fer.oprpp1.zemris.java.hw05.shell.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.oprpp1.zemris.java.hw05.shell.Environment;
import hr.fer.oprpp1.zemris.java.hw05.shell.ShellCommand;
import hr.fer.oprpp1.zemris.java.hw05.shell.ShellStatus;

public class HelpShellCommand implements ShellCommand{
	
	private String commandName;
	private List<String> commandDescription;
	
	public HelpShellCommand() {
		commandName = "help";
		commandDescription = new ArrayList<>();
		commandDescription.add("Command help lists the names of all supported commands if started with no arguments.");
		commandDescription.add("If started with single argument, it prints name and the description of selected command");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(arguments == null) {
			var commands = env.commands().values();
			for(var c : commands) {
				env.writeln(c.getCommandName());
			}
		}else {
			
			String[] args = arguments.split("\\s+");
			if(args.length != 1) {
				env.writeln("Help command takes one or no arguments only!");
				return ShellStatus.CONTINUE;
			}
			
			ShellCommand command = env.commands().get(arguments);
			if(command == null) {
				env.writeln("No such command. Type \"help\" for a list of available commands.");
				return ShellStatus.CONTINUE;
			}
			
			var list = command.getCommandDescription();
			for(var line : list) {
				env.writeln(line);
			}
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return commandName;
	}

	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(commandDescription);
	}

}
