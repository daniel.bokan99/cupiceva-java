package hr.fer.oprpp1.zemris.java.hw05.shell.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.oprpp1.zemris.java.hw05.shell.Environment;
import hr.fer.oprpp1.zemris.java.hw05.shell.ShellCommand;
import hr.fer.oprpp1.zemris.java.hw05.shell.ShellStatus;

public class ExitShellCommand implements ShellCommand {
	
	private String commandName;
	private List<String> commandDescription;
	
	public ExitShellCommand() {
		commandName = "exit";
		commandDescription = new ArrayList<>();
		commandDescription.add("Command that is used for exiting MyShell");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		
		if(arguments != null) {
			env.writeln("Exit command takes no arguments!");
			return ShellStatus.CONTINUE;
		}
		
		return ShellStatus.TERMINATE;
	}

	@Override
	public String getCommandName() {
		return commandName;
	}

	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(commandDescription);
	}
	

}
