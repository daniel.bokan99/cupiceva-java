package hr.fer.oprpp1.zemris.java.hw05.shell;

import java.util.SortedMap;

public interface Environment {
	
	public String readLine() throws ShellIOException;
	
	public void write(String text) throws ShellIOException;
	
	public void writeln(String text) throws ShellIOException;
	
	public SortedMap<String, ShellCommand> commands();
	
	public Character getMultilineSymbol();
	
	public void setMultilineSymbol(Character symbol);
	
	public Character getPromptSymbol();
	
	public void setPromptSymbol(Character symbol);
	
	public Character getMorelinesSymbol();
	
	public void setMorelinesSymbol(Character symbol);

}
