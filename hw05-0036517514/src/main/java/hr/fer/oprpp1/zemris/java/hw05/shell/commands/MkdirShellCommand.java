package hr.fer.oprpp1.zemris.java.hw05.shell.commands;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.oprpp1.zemris.java.hw05.shell.Environment;
import hr.fer.oprpp1.zemris.java.hw05.shell.ShellCommand;
import hr.fer.oprpp1.zemris.java.hw05.shell.ShellStatus;

public class MkdirShellCommand implements ShellCommand{
	
	private String commandName;
	private List<String> commandDescription;
	
	public MkdirShellCommand() {
		commandName = "mkdir";
		commandDescription = new ArrayList<>();
		commandDescription.add("Command takes a single argument, directory name.");
		commandDescription.add("And then creates the appropriate directory structure.");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		
		if(arguments.startsWith("\"") && arguments.endsWith("\"")) {
			arguments = arguments.substring(1, arguments.length() - 1);
		}else {
			String[] args = arguments.split("\\s+");
			if(args.length != 1) {
				env.writeln("Invalid number of args!");
				return ShellStatus.CONTINUE;
			}
		}
		
		Path p = Paths.get(arguments);
		try {
			Files.createDirectories(p);
		} catch (IOException e) {
			env.writeln("Error while trying to create directory");
		}
		
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return commandName;
	}

	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(commandDescription);
	}

}
