package hr.fer.oprpp1.zemris.java.hw05.shell.commands;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import hr.fer.oprpp1.zemris.java.hw05.shell.Environment;
import hr.fer.oprpp1.zemris.java.hw05.shell.ShellCommand;
import hr.fer.oprpp1.zemris.java.hw05.shell.ShellStatus;

public class LsShellCommand implements ShellCommand {
	
	private String commandName;
	private List<String> commandDescription;
	
	public LsShellCommand() {
		commandName = "ls";
		commandDescription = new ArrayList<>();
		commandDescription.add("Command to list computer files");
	}
	
	private static String stringFormatHelp(File f) {
		
		String s = "";
		s += (f.isDirectory()) ?  "d" : "-";
		s += (Files.isReadable(f.toPath())) ?  "r" : "-";
		s += (Files.isWritable(f.toPath())) ?  "w" : "-";
		s += (Files.isExecutable(f.toPath())) ?  "x" : "-";
		
		s += " " + String.format("%10d", f.length());
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Path path = Paths.get(f.getAbsolutePath());
		BasicFileAttributeView faView = Files.getFileAttributeView(
		path, BasicFileAttributeView.class, LinkOption.NOFOLLOW_LINKS
		);
		BasicFileAttributes attributes = null;
		try {
			attributes = faView.readAttributes();
		} catch (IOException e) {
			e.printStackTrace();
		}
		FileTime fileTime = attributes.creationTime();
		String formattedDateTime = sdf.format(new Date(fileTime.toMillis()));
		
		s += " " + formattedDateTime + " " + f.getName();
		
		return s;
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		
		if(arguments.startsWith("\"") && arguments.endsWith("\"")) {
			arguments = arguments.substring(1, arguments.length() - 1);
		}else {
			String[] args = arguments.split("\\s+");
			if(args.length != 1) {
				env.writeln("Invalid number of args!");
				return ShellStatus.CONTINUE;
			}
		}
		
		try {
			File file = new File(arguments);
			
			if(file.isDirectory()) {
				File[] children = file.listFiles();
				for(File f : children) {
					env.writeln(stringFormatHelp(f));
				}
			}else {
				env.writeln(stringFormatHelp(file));
			}
		}catch(InvalidPathException e) {
			env.writeln("Invalid path!");
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return commandName;
	}

	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(commandDescription);
	}

}
