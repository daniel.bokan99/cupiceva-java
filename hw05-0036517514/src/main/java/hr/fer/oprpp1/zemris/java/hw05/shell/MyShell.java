package hr.fer.oprpp1.zemris.java.hw05.shell;

public class MyShell {
	
	public static void main(String[] args) {
		System.out.println("Welcome to MyShell v 1.0");
		
		MyEnvironment env = new MyEnvironment();
		
		ShellStatus status = null;
		do {
			env.write(env.getPromptSymbol().toString() + " ");
			String line = env.readLine();
			String multiline = "";
			if(line.endsWith(" " + String.valueOf(env.getMorelinesSymbol()))){
				while(line.endsWith(String.valueOf(env.getMorelinesSymbol()))) {
					multiline += line.substring(0, line.length() - 1);
					env.write(String.valueOf(env.getMultilineSymbol()) + " ");
					line = env.readLine();
				}
				multiline += line;
			}
			if(!multiline.equals("")) {
				line = multiline;
			}
			String[] split = line.split("\\s+", 2);
			String commandName = split[0];
			String arguments = null;
			if(split.length > 1) {
				arguments = split[1];
			}
			
			ShellCommand command = env.commands().get(commandName);
			
			if(command == null) {
				env.writeln("Unsupported command. Try again.");
				continue;
			}
			
			try {
				status = command.executeCommand(env, arguments);
			}catch(ShellIOException e) {
				status = ShellStatus.TERMINATE;
			}
			
			
		}while(status != ShellStatus.TERMINATE);
		
	}

}
