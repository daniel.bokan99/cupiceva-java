package hr.fer.oprpp1.zemris.java.hw05.shell.commands;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.oprpp1.zemris.java.hw05.shell.Environment;
import hr.fer.oprpp1.zemris.java.hw05.shell.ShellCommand;
import hr.fer.oprpp1.zemris.java.hw05.shell.ShellStatus;

public class HexdumpShellCommand implements ShellCommand{
	
	private String commandName;
	private List<String> commandDescription;
	
	public HexdumpShellCommand() {
		commandName = "hexdump";
		commandDescription = new ArrayList<>();
		commandDescription.add("Command takes a single argument, file name.");
		commandDescription.add("And then produces hex-output.");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		
		if(arguments.startsWith("\"") && arguments.endsWith("\"")) {
			arguments = arguments.substring(1, arguments.length() - 1);
		}else {
			String[] args = arguments.split("\\s+");
			if(args.length != 1) {
				env.writeln("Invalid number of args!");
				return ShellStatus.CONTINUE;
			}
		}
		
		File file = new File(arguments);
		
		if(file.isDirectory()) {
			env.writeln("Invalid argument! Argument should not be directory");
			return ShellStatus.CONTINUE;
		}
		
	    int i = 0;
	 
	    try(InputStream is = new FileInputStream(file);) {
			while (is.available() > 0) {
				StringBuilder sb1 = new StringBuilder();
			    StringBuilder sb2 = new StringBuilder("   ");
			    System.out.printf("%04X  ", i * 16);
			    for (int j = 0; j < 16; j++) {
			    	if (is.available() > 0) {
			    		int value = (int) is.read();
			    		sb1.append(String.format("%02X ", value));
			    		if(j == 7) {
			    			sb1.append("|");
			    		}
			    		if (value >= 32 && value <= 127) {
			    			sb2.append((char)value);
			    		}else {
			    			sb2.append(".");
			    		}
			    	}else {
			    		for (;j < 16;j++) {
			    			sb1.append("   ");
			    		}
			       }
			    }
			    sb1.append("|");
			    System.out.print(sb1);
			    System.out.println(sb2);
			    i++;
			}
		} catch (IOException e) {
			env.writeln("Error while trying to make a hexdump");
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return commandName;
	}

	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(commandDescription);
	}

}
