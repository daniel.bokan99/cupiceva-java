package hr.fer.oprpp1.zemris.java.hw05.shell;

import java.util.List;

public interface ShellCommand {

	public ShellStatus executeCommand(Environment env, String arguments);
	
	public String getCommandName();
	
	public List<String> getCommandDescription();
	
}
