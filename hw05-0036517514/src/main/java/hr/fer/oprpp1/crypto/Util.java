package hr.fer.oprpp1.crypto;

import java.util.ArrayList;
import java.util.List;

public class Util {

	public static byte[] hextobyte(String keytext) {
		
		if(keytext.length() % 2 != 0) throw new IllegalArgumentException("Length of keytext must be divisible by 2.");
		
		if(keytext.length() == 0) return new byte[0];
		
		byte[] data = new byte[keytext.length()/2];
		
		int index = 0;
		while(index < keytext.length()) {
			char c = keytext.charAt(index);
			int value = 0;
			if(c >= 'A' && c <= 'F') {
				value = (10 + (c - 'A'));
			}else if(c >= 'a' && c <= 'f') {
				value = (10 + (c - 'a'));
			}else if(c >= '0' && c <= '9') {
				value = (c - '0');
			}else {
				throw new IllegalArgumentException("Illegal argument!");
			}
			
			data[index/2] += (byte) (value << (((index + 1) % 2) * 4));
			
			index++;
			
		}
		
		return data;
		
	}
	
	public static String[] parseArgs(String arguments) {
		List<String> result = new ArrayList<>();
		
		char[] charArray = arguments.toCharArray();
		String arg = "";
		boolean invalid = false;
		for(int i = 0; i < charArray.length; i++) {
			if(charArray[i] == '"'){
				i++;
				while(charArray[i] != '"') {
					arg += String.valueOf(charArray[i]);
					i++;
				}
				result.add(arg);
				arg = "";
			}else if(charArray[i] == ' ') {
				continue;
			}else {
				invalid = true;
				break;
			}
		}
		
		if(invalid) {
			return null;
		}else {
			String[] bla = new String[2];
			return result.toArray(bla);
		}
		
	}
	
	
	public static String bytetohex(byte[] bytearray) {
		if(bytearray.length == 0) return "";
		
		String s = "";
		for(int i = 0; i < bytearray.length; i++) {
			s += String.format("%02x", bytearray[i]);
		}
		
		return s;
	}
	
	
	
}
