package hr.fer.oprpp1.crypto;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Scanner;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Crypto {
	
	public static void checksha(String hex, String file){
		
		try(FileInputStream is = new FileInputStream("C:\\Faks\\OPRPP1\\Zadace\\hw05-0036517514\\" + file)){
			
			byte[] data = new byte[4096];
			byte[] result = null;
			int read = is.read(data);
			
			try {
				MessageDigest md = MessageDigest.getInstance("SHA-256");
				while(read > 0) {
					md.update(data, 0, read);
					read = is.read(data);
				}
				
				result = md.digest();
				String hexResult = Util.bytetohex(result);
				
				if(hexResult.equals(hex)) {
					System.out.println("Digesting completed. Digest of " + file + " matches expected digest.");
				}else {
					System.out.println("Digesting completed. Digest of " + file + "does not match the expected digest.");
					System.out.println("Digest was: " + hex);
				}
				
			} catch (NoSuchAlgorithmException e) {
				System.out.println("Incorrect algorithm!");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void crypt(boolean type, String pass, String initVector, String file, String outputFile) {
		
		try(FileInputStream is = new FileInputStream("C:\\Faks\\OPRPP1\\Zadace\\hw05-0036517514\\" + file);
			FileOutputStream os = new FileOutputStream("C:\\Faks\\OPRPP1\\Zadace\\hw05-0036517514\\" + outputFile)){
				
			SecretKeySpec keySpec = new SecretKeySpec(Util.hextobyte(pass), "AES");
			AlgorithmParameterSpec paramSpec = new IvParameterSpec(Util.hextobyte(initVector));
			
			byte[] data = new byte[4096];
			//byte[] result = null;
			int read = is.read(data);
			
			try {
				Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
				cipher.init(type ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, keySpec, paramSpec);
				
				while(read != -1) {
					os.write(cipher.update(data, 0, read));
					read = is.read(data);
				}
				
				os.write(cipher.doFinal());
				
				System.out.println("Decryption completed. Generated file " + outputFile + " based on file " + file + ".");
				
				
			} catch (Exception e) {
				e.printStackTrace();
			}
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	public static void main(String[] args) {
		
		String[] arguments = args[0].split("\\s+");
		String method = arguments[0];
		String file = arguments[1];
		String outputFile = null;
		if(arguments.length == 3) {
			outputFile = arguments[2];
		}
		
		
		try(Scanner sc = new Scanner(System.in)){
			if(method.equals("checksha")) {
				System.out.println("Please provide expected sha-256 digest for " + file + ":");
				System.out.printf("> ");
				if(sc.hasNextLine()) {
					String hex = sc.nextLine();
					checksha(hex, file);
				}
			}else if(method.equals("encrypt") || method.equals("decrypt")) {
				boolean type = method.equals("encrypt") ? true : false;
				String pass = null;
				String initVector = null;
				System.out.println("Please provide password as hex-encoded text(16 bytes, i.e. 32 hex-digits):");
				System.out.printf("> ");
				if(sc.hasNextLine()) {
					pass = sc.nextLine();
				}
				System.out.println("Please provide initialization vector as hex-encoded text (32 hex-digits):");
				System.out.printf("> ");
				if(sc.hasNextLine()) {
					initVector = sc.nextLine();
				}
				crypt(type, pass, initVector, file, outputFile);
			}else {
				throw new IllegalArgumentException("Incorrect method name!");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
