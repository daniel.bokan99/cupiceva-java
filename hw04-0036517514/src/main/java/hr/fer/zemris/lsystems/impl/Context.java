package hr.fer.zemris.lsystems.impl;

import hr.fer.oprpp1.custom.collections.ObjectStack;

public class Context {
	
	private ObjectStack<TurtleState> stack;
	
	public Context() {
		this.stack = new ObjectStack<>();
	}
	
	/**Method returns current state on top of stack without removing it from the stack.
	 * @return TurtleState that's at the top of the stack
	 */
	public TurtleState getCurrentState() {
		return stack.peek();
	}
	
	/**Method pushes given TurtleState to the top of this Stack.
	 * @param state that is pushed to the top of this Stack.
	 */
	public void pushState(TurtleState state) {
		stack.push(state);
	}
	
	/**Method removes the TurtleState that is on top of Stack.
	 * 
	 */
	public void popState() {
		stack.pop();
	}
	
}
