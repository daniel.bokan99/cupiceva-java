package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;

public class ScaleCommand implements Command{
	
	private double factor;
	
	public ScaleCommand(double factor) {
		this.factor = factor;
	}

	/**Method scales the current shift of the current state with the given factor.
	 *
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		
		TurtleState currState = ctx.getCurrentState();
		currState.setCurrentShift(currState.getCurrentShift() * factor);
		
	}

}
