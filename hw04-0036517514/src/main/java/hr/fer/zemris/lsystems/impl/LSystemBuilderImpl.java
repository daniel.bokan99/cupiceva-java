package hr.fer.zemris.lsystems.impl;

import java.awt.Color;

import hr.fer.oprpp1.custom.collections.SimpleHashtable;
import hr.fer.zemris.lsystems.LSystem;
import hr.fer.zemris.lsystems.LSystemBuilder;
import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.commands.ColorCommand;
import hr.fer.zemris.lsystems.impl.commands.DrawCommand;
import hr.fer.zemris.lsystems.impl.commands.PopCommand;
import hr.fer.zemris.lsystems.impl.commands.PushCommand;
import hr.fer.zemris.lsystems.impl.commands.RotateCommand;
import hr.fer.zemris.lsystems.impl.commands.ScaleCommand;
import hr.fer.zemris.lsystems.impl.commands.SkipCommand;

public class LSystemBuilderImpl implements LSystemBuilder {
	
	public static class LocalLSystem implements LSystem{
		
		private LSystemBuilderImpl builder;
		private double initalUnitLength;
		private SimpleHashtable<Integer, String> map;
		
		private LocalLSystem(LSystemBuilderImpl builder) {
			this.builder = builder;
			this.initalUnitLength = builder.unitLength;
			this.map = new SimpleHashtable<>();
			map.put(0, builder.axiom);
		}

		@Override
		public void draw(int level, Painter painter) {
			
			char[] commands = generate(level).toCharArray();
			Context ctx = new Context();
			TurtleState ts = new TurtleState(builder.origin, new Vector2D(Math.cos(builder.angle), Math.sin(builder.angle)), Color.black, builder.unitLength);
			ctx.pushState(ts);
			
			for(int i = 0; i < commands.length; i++) {
				if(Character.isWhitespace(commands[i])) continue;
				Command command = builder.commands.get(commands[i]);
				if(command == null) continue;
				command.execute(ctx, painter);
				
			}
			
		}

		@Override
		public String generate(int arg0) {
			builder.unitLength = initalUnitLength * Math.pow(builder.unitLengthDegreeScaler, arg0);
			
			if(arg0 == 0) return builder.axiom;
			
			String str = map.get(arg0);
			if(str == null) {
				str = map.get(arg0 - 1);
			}else {
				return map.get(arg0);
			}
				
			char[] beginning = str.toCharArray();
			String endString = "";
				
			for(int i = 0; i < beginning.length; i++) {
				if(builder.productions.containsKey(beginning[i])) {
					String replacement = builder.productions.get(beginning[i]);
					endString += replacement;
				}else {
					endString += beginning[i];
				}
			}
			map.put(arg0, endString);
			return endString;
			
		}
		
	}
	
	private SimpleHashtable<Character, String> productions;
	private SimpleHashtable<Character, Command> commands;
	private double unitLength;
	private double unitLengthDegreeScaler;
	private Vector2D origin;
	private String axiom;
	private double angle;
	
	public LSystemBuilderImpl() {
		productions = new SimpleHashtable<>();
		commands = new SimpleHashtable<>();
		unitLength = 0.1;
		unitLengthDegreeScaler = 1;
		origin = new Vector2D(0 , 0);
		axiom = "";
	}

	@Override
	public LSystem build() {
		return new LocalLSystem(this);
	}

	@Override
	public LSystemBuilder configureFromText(String[] arg0) {
		for(int i = 0; i < arg0.length; i++) {
			String[] split = arg0[i].split("\\s+");
			switch (split[0]) {
			case "origin": 
				setOrigin(Double.parseDouble(split[1]), Double.parseDouble(split[2]));
				break;
			case "angle":
				setAngle(Double.parseDouble(split[1]));
				break;
			case "unitLength":
				setUnitLength(Double.parseDouble(split[1]));
				break;
			case "unitLengthDegreeScaler":
				double num = Double.parseDouble(split[1]) / Double.parseDouble(split[3]);
				setUnitLengthDegreeScaler(num);
				break;
			case "command":
				if(split.length == 4) {
					String value = split[2] + " " + split[3];
					registerCommand(split[1].charAt(0), value);
				}else {
					registerCommand(split[1].charAt(0), split[2]);
				}
				break;
			case "axiom":
				setAxiom(split[1]);
				break;
			case "production":
				registerProduction(split[1].charAt(0), split[2]);
				break;
			}
			
		}
		return this;
	}

	@Override
	public LSystemBuilder registerCommand(char arg0, String arg1) {
		String[] split = arg1.split("\\s+");
		Command c = null;
		switch (split[0]) {
		case "draw":
			c = new DrawCommand(Double.parseDouble(split[1]));
			break;
		case "skip":
			c = new SkipCommand(Double.parseDouble(split[1]));
			break;
		case "scale":
			c = new ScaleCommand(Double.parseDouble(split[1]));
			break;
		case "rotate":
			c = new RotateCommand(Double.parseDouble(split[1]));
			break;
		case "push":
			c = new PushCommand();
			break;
		case "pop":
			c = new PopCommand();
			break;
		case "color":
			c = new ColorCommand(split[1]);
			break;
		default:
			System.out.println("Invalid command");
		}
		commands.put(arg0, c);
		return this;
	}

	@Override
	public LSystemBuilder registerProduction(char arg0, String arg1) {
		productions.put(arg0, arg1);
		return this;
	}

	@Override
	public LSystemBuilder setAngle(double arg0) {
		double angle = arg0 * (Math.PI/180.0);
		this.angle = angle;
		return this;
	}

	@Override
	public LSystemBuilder setAxiom(String arg0) {
		this.axiom = arg0;
		return this;
	}

	@Override
	public LSystemBuilder setOrigin(double arg0, double arg1) {
		this.origin = new Vector2D(arg0, arg1);
		return this;
	}

	@Override
	public LSystemBuilder setUnitLength(double arg0) {
		this.unitLength = arg0;
		return this;
	}

	@Override
	public LSystemBuilder setUnitLengthDegreeScaler(double arg0) {
		this.unitLengthDegreeScaler = arg0;
		return this;
	}

}
