package hr.fer.zemris.lsystems.impl;

public class Vector2D {

	private double x;
	private double y;
	
	public Vector2D(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	/**Method returns the value of x this Vector2D.
	 * @return double x
	 */
	public double getX() {
		return this.x;
	}
	
	/**Method returns the value of y of this Vector2D.
	 * @return
	 */
	public double getY() {
		return this.y;
	}
	
	/**Method adds this and offset vector.
	 * @param offset another vector that is being added to this vector.
	 */
	public void add(Vector2D offset) {
		this.x += offset.x;
		this.y += offset.y;
	}
	
	/**Method adds this and offset vector.
	 * @param offset another vector that is being added to this vector.
	 * @return new Vector2D that is the sum of this and offset vector.
	 */
	public Vector2D added(Vector2D offset) {
		return new Vector2D(this.x + offset.x, this.y + offset.y);
	}
	
	/**Method rotates this vector for the specified angle. Angle is in radians.
	 * @param angle
	 */
	public void rotate(double angle) {
		double newX = Math.cos(angle)*this.x - Math.sin(angle)*this.y;
		double newY = Math.sin(angle)*this.x + Math.cos(angle)*this.y;
		this.x = newX;
		this.y = newY;
	}
	
	/**Method rotates this vector for the specified angle and returns new rotated Vector2D.
	 * Angle is in radians.
	 * @param angle
	 * @return new rotated Vector2D
	 */
	public Vector2D rotated(double angle) {
		double newX = Math.cos(angle)*this.x - Math.sin(angle)*this.y;
		double newY = Math.sin(angle)*this.x + Math.cos(angle)*this.y;
		return new Vector2D(newX, newY);
	}
	
	/**Method scales this vector with scaler.
	 * @param scaler
	 */
	public void scale(double scaler) {
		this.x *= scaler;
		this.y *= scaler;
	}
	
	/**Method scales this vector with scaler and returns new Vector2D.
	 * @param scaler
	 * @return new scaled Vector2D.
	 */
	public Vector2D scaled(double scaler) {
		double newX = this.x * scaler;
		double newY = this.y * scaler;
		return new Vector2D(newX, newY);
	}
	
	/**Method copies this Vector2D and returns new Vector2D.
	 * @return new Vector2D.
	 */
	public Vector2D copy() {
		return new Vector2D(this.x, this.y);
	}
	
	
}
