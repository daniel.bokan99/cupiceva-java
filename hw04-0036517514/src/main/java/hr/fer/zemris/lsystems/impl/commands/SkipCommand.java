package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;
import hr.fer.zemris.lsystems.impl.Vector2D;

public class SkipCommand implements Command {
	
	private double step;
	
	public SkipCommand(double step) {
		this.step = step;
	}

	/**Same as DrawCommand, just that it does not draw a line.
	 *
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		
		TurtleState currState = ctx.getCurrentState();
		Vector2D offset = currState.getDirection().scaled(step * currState.getCurrentShift());
		Vector2D nextStep = currState.getCurrentState().added(offset);
		TurtleState newState = new TurtleState(nextStep, currState.getDirection(), currState.getColor(), currState.getCurrentShift());
		ctx.popState();
		ctx.pushState(newState);

	}

}
