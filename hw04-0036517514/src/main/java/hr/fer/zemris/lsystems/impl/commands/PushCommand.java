package hr.fer.zemris.lsystems.impl.commands;

import java.awt.Color;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;
import hr.fer.zemris.lsystems.impl.Vector2D;

public class PushCommand implements Command{

	/**Method copies the state from the top and pushes it to the top of the Stack.
	 *
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		TurtleState state = ctx.getCurrentState();
		Vector2D newStateVector = new Vector2D(state.getCurrentState().getX(), state.getCurrentState().getY());
		Vector2D newDirectionVector = new Vector2D(state.getDirection().getX(), state.getDirection().getY());
		Color newColor = new Color(state.getColor().getRGB());
		@SuppressWarnings("deprecation")
		double newShift = new Double(state.getCurrentShift());
		TurtleState newState = new TurtleState(newStateVector, newDirectionVector, newColor, newShift);
		ctx.pushState(newState);
		
	}

}
