package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;
import hr.fer.zemris.lsystems.impl.Vector2D;

public class DrawCommand implements Command {
	
	private double step;
	
	public DrawCommand(double step) {
		this.step = step;
	}

	/**Method calculates where is the turtles next step and draws the line from the beginning to
	 *the new position.
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		
		TurtleState currState = ctx.getCurrentState();
		
		Vector2D offset = currState.getDirection().scaled(step * currState.getCurrentShift());
		
		Vector2D nextStep = currState.getCurrentState().added(offset);
		TurtleState newState = new TurtleState(nextStep, currState.getDirection(), currState.getColor(), currState.getCurrentShift());
		painter.drawLine(currState.getCurrentState().getX(), currState.getCurrentState().getY(), newState.getCurrentState().getX(), 
				newState.getCurrentState().getY(), currState.getColor(), 1.0f);
		ctx.popState();
		ctx.pushState(newState);

	}

}
