package hr.fer.zemris.lsystems.impl;

import java.awt.Color;

public class TurtleState {
	
	private Vector2D currentState;
	private Vector2D direction;
	private Color color;
	private double currentShift;
	
	public TurtleState(Vector2D currentState, Vector2D direction, Color color, double currentShift) {
		super();
		this.currentState = currentState;
		this.direction = direction;
		this.color = color;
		this.currentShift = currentShift;
	}
	
	/**
	 * @return the currentState
	 */
	public Vector2D getCurrentState() {
		return currentState;
	}
	
	/**
	 * @param currentState the currentState to set
	 */
	public void setCurrentState(Vector2D currentState) {
		this.currentState = currentState;
	}
	
	/**
	 * @return the direction
	 */
	public Vector2D getDirection() {
		return direction;
	}
	
	/**
	 * @param direction the direction to set
	 */
	public void setDirection(Vector2D direction) {
		this.direction = direction;
	}
	
	/**
	 * @return the color
	 */
	public Color getColor() {
		return color;
	}
	
	/**
	 * @param color the color to set
	 */
	public void setColor(Color color) {
		this.color = color;
	}
	
	/**
	 * @return the currentShift
	 */
	public double getCurrentShift() {
		return currentShift;
	}
	
	/**
	 * @param currentShift the currentShift to set
	 */
	public void setCurrentShift(double currentShift) {
		this.currentShift = currentShift;
	}
	
	/**Method returns copy of this TurtleState.
	 * @return new TurtleState
	 */
	public TurtleState copy() {
		return new TurtleState(currentState, direction, color, currentShift);
	}
	
}
