package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

public class PopCommand implements Command{

	/**Method removes the state at the top of the Stack.
	 *
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		ctx.popState();
	}
	
	

}
