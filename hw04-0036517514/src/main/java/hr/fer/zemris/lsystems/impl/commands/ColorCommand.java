package hr.fer.zemris.lsystems.impl.commands;

import java.awt.Color;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;

public class ColorCommand implements Command {
	
	private Color color;
	
	public ColorCommand(String color) {
		int num = Integer.parseInt(color, 16);
		this.color = new Color(num);
	}

	/**Method changes the current state to be the given color.
	 *
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		TurtleState currState = ctx.getCurrentState();
		currState.setColor(this.color);
	}

}
