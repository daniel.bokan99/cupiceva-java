package hr.fer.zemris.lsystems.impl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;


import hr.fer.zemris.lsystems.LSystem;
import hr.fer.zemris.lsystems.LSystemBuilderProvider;
import hr.fer.zemris.lsystems.gui.LSystemViewer;

public class Demo {
	
	private static LSystem createKochCurve2(LSystemBuilderProvider provider) {
		List<String> lines = null;
		try {
			lines = Files.readAllLines(Paths.get("C:\\Faks\\OPRPP1\\Zadace\\hw04-0036517514\\examples\\plant1.txt"),StandardCharsets.UTF_8);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String[] data = lines.toArray(new String[0]);
		return provider.createLSystemBuilder().configureFromText(data).build();
	}
	
	public static void main(String[] args) {
		
		//LSystemViewer.showLSystem(createKochCurve2(LSystemBuilderImpl::new));
		
		LSystemViewer.showLSystem(LSystemBuilderImpl::new);

		
	}

}
