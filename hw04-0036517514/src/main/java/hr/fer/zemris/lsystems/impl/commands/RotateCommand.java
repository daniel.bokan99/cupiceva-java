package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;

public class RotateCommand implements Command{
	
	private double angle;
	
	public RotateCommand(double angle) {
		if(angle < 0) angle = 360 + angle;
		this.angle = angle* (Math.PI/180.0);
	}

	/**Method rotates the state on top of the Stack for the given angle.
	 *
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		TurtleState currState = ctx.getCurrentState();
		currState.getDirection().rotate(angle);
		
	}

}
