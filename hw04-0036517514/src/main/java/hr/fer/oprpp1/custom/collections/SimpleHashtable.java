package hr.fer.oprpp1.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class SimpleHashtable<K,V> implements Iterable<SimpleHashtable.TableEntry<K, V>>{
	
	public static class TableEntry<K,V>{
		
		private K key;
		private V value;
		private TableEntry<K,V> next;
		
		public TableEntry(K key, V value) {
			if(key == null) throw new IllegalArgumentException("Key must not be null!");
			this.key = key;
			this.value = value;
		}
		
		/**Getter for pairs key.
		 * @return key of type K.
		 */
		public K getKey() {
			return this.key;
		}
		
		/**Getter for pairs value.
		 * @return value of type V.
		 */
		public V getValue() {
			return this.value;
		}
		
		/**Setter for value of pair.
		 * @param value of type V.
		 */
		public void setValue(V value) {
			this.value = value;
		}
		
	}
	
	private class IteratorImpl implements Iterator<SimpleHashtable.TableEntry<K, V>>{
		
		private SimpleHashtable<K,V> hashTable;
		private int currentElement;
		private TableEntry<K,V> currEntry;
		private boolean canRemove;
		private int savedModificationCount;
		
		public IteratorImpl(SimpleHashtable<K,V> hashTable) {
			this.hashTable = hashTable;
			this.currentElement = 0;
			this.canRemove = false;
			this.savedModificationCount = hashTable.modificationCount;
		}

		@Override
		public boolean hasNext() {
			if(savedModificationCount != hashTable.modificationCount) throw new ConcurrentModificationException();
			if(currentElement < hashTable.size) return true;
			return false;
		}

		@Override
		public TableEntry<K, V> next() {
			if(savedModificationCount != hashTable.modificationCount) throw new ConcurrentModificationException();
			
			int tempElement = 0;
			if(hasNext()) {
				for(int i = 0; i < hashTable.array.length; i++) {
					currEntry = hashTable.array[i];
					while(currEntry != null) {
						if(tempElement == currentElement) {
							currentElement++;
							canRemove = true;
							return currEntry;
						}
						tempElement++;
						currEntry = currEntry.next;
					}
				}
			}
			
			throw new NoSuchElementException();
		}
		
		@Override
		public void remove() {
			if(!canRemove) throw new IllegalStateException();
			int index = Math.abs(currEntry.getKey().hashCode() % hashTable.array.length);
			TableEntry<K,V> temp = hashTable.array[index];
			TableEntry<K,V> prev = null;
			while(temp != null) {
				if(temp.key.equals(currEntry.key)) {
					if(prev != null) {
						prev.next = temp.next;
					}else {
						hashTable.array[index] = temp.next;
					}
					hashTable.size--;
					currentElement--;
					hashTable.modificationCount++;
					savedModificationCount++;
					temp.value = null;
					canRemove = false;
					return;
				}
				prev = temp;
				temp = temp.next;
			}
		}
	}
	
	private TableEntry<K,V>[] array;
	private int size;
	private int modificationCount;
	
	@SuppressWarnings("unchecked")
	public SimpleHashtable(int initialCapacity) {
		if(initialCapacity < 1) throw new IllegalArgumentException("Initial capacity must be larger or equal than 1!");
		int powerOfTwo = 1;
		for(int i = 0; powerOfTwo < initialCapacity; i++) {
			powerOfTwo = (int)Math.pow(2.0, i);
		}
		
		this.array = (TableEntry<K,V>[]) new TableEntry[powerOfTwo];
		this.modificationCount = 0;
		this.size = 0;
	}
	
	public SimpleHashtable() {
		this(16);
	}
	
	/**Method puts the key, value pair into the Hashtable.
	 * @param key
	 * @param value
	 * @return null if there was no record for the specified key, old value otherwise.
	 */
	@SuppressWarnings("unchecked")
	public V put(K key, V value) {
		if(key == null) throw new NullPointerException("Key must not be null!");
		
		//Check what's the ratio of size to array length
		//If it's larger or equal to 0.75, double the array length
		double ratio = this.size / (double)this.array.length;
		if(ratio >= 0.75) {
			Object[] oldTable = this.toArray();
//			for(int i = 0; i < oldTable.length; i++) {
//				TableEntry<K,V> temp = (TableEntry<K,V>)oldTable[i];
//				temp.next = null;
//				oldTable[i] = temp;
//			}
			TableEntry<K,V>[] newTable = (TableEntry<K,V>[])new TableEntry[this.array.length * 2];
			
			for(int i = 0; i < oldTable.length; i++) {
				TableEntry<K,V> currEntry = (TableEntry<K, V>) oldTable[i];
				int index = Math.abs(currEntry.getKey().hashCode() % newTable.length);
				if(newTable[index] == null) {
					newTable[index] = currEntry;
				}else {
					TableEntry<K,V> tempEntry = (TableEntry<K, V>) newTable[index];
					while(tempEntry.next != null) {
						tempEntry = tempEntry.next;
					}
					tempEntry.next = currEntry;
				}
			}
			modificationCount++;
			this.array = newTable;
		}
		
		
		int index = Math.abs(key.hashCode() % this.array.length);
		TableEntry<K,V> currEntry = (TableEntry<K, V>) array[index];
		
		if(this.containsKey(key)) {
			while(currEntry != null) {
				if(currEntry.getKey().equals(key)) {
					V oldValue = currEntry.getValue();
					currEntry.setValue(value);
					return oldValue;
				}
				currEntry = currEntry.next;
			}
		}else {
			if(currEntry == null) {
				array[index] = new TableEntry<K,V>(key,value);;
			}else {
				while(currEntry.next != null) {
					currEntry = currEntry.next;
				}
				currEntry.next = new TableEntry<K,V>(key,value);
			}
			modificationCount++;
			this.size++;
		}
		
		return null;
		
	}
	
	/**Method returns the value under the specified key. If there is no key in map, the method returns null.
	 * @param key
	 * @return null if there is no key, value otherwise.
	 */
	public V get(Object key) {
		int index = Math.abs(key.hashCode() % this.array.length);
		TableEntry<K,V> currEntry = array[index];
		while(currEntry != null) {
			if(currEntry.getKey().equals(key)) {
				return currEntry.getValue();
			}
			currEntry = currEntry.next;
		}
		
		return null;
	}
	
	/**Method returns the number of elements currently in the Map.
	 * @return int number of elements.
	 */
	public int size() {
		return this.size;
	}
	
	/**Method checks if there is a specified key in this Map.
	 * @param key
	 * @return true if there is a key, false otherwise.
	 */
	public boolean containsKey(Object key) {
		int index = Math.abs(key.hashCode() % this.array.length);
		TableEntry<K,V> currEntry = array[index];
		
		while(currEntry != null) {
			if(currEntry.getKey().equals(key)) return true;
			currEntry = currEntry.next;
		}
		return false;
	}
	
	/**Method checks if there is a specified value in this Map.
	 * @param value
	 * @return true if there is a key, false otherwise.
	 */
	public boolean containsValue(Object value) {
		TableEntry<K,V> currEntry;
		for(int i = 0; i < array.length; i++) {
			currEntry = array[i];
			while(currEntry != null) {
				if(currEntry.getValue().equals(value)) return true;
				currEntry= currEntry.next;
			}
		}
		return false;
	}
	
	/**Method checks whether the map is empty or not.
	 * @return true if it is empty, false otherwise.
	 */
	public boolean isEmpty() {
		if(this.size() == 0) return true;
		return false;
	}
	
	@Override
	public String toString() {
		String s = "[";
		TableEntry<K,V> currEntry;
		for(int i = 0; i < array.length; i++) {
			currEntry = array[i];
			while(currEntry != null) {
				s += currEntry.getKey() + "=" + currEntry.getValue() + ", ";
				currEntry = currEntry.next;
			}
		}
		char[] temp = s.toCharArray();
		s = "";
		for(int i = 0; i < temp.length - 2; i++) {
			s += temp[i];
		}
		s += ']';
		
		return s;
		
	}
	
	/**Method fills the array with maps content and returns it.
	 * @return new Object array.
	 */
	@SuppressWarnings("unchecked")
	public TableEntry<K,V>[] toArray(){
		TableEntry<K,V>[] obj = (TableEntry<K,V>[])new TableEntry[this.size];
		int index = 0;
		TableEntry<K,V> currEntry;
		for(int i = 0; i < array.length; i++) {
			currEntry = array[i];
			while(currEntry != null) {
				obj[index++] = new TableEntry<K,V>(currEntry.key, currEntry.value);
				currEntry = currEntry.next;
			}
		}
		
		return obj;
	}
	
	/**Method deletes all elements from the map.
	 * 
	 */
	public void clear() {
		for(int i = 0; i < this.array.length; i++) {
			this.array[i] = null;
		}
		this.size = 0;
	}
	
	
	/**Removes the key and its value from this hashtable. This method does nothing if the 
	 * key is not in the hashtable.
	 * @param key the key that needs to be removed.
	 */
	public V remove(Object key) {
		int index = Math.abs(key.hashCode() % array.length);
		TableEntry<K,V> temp = array[index];
		TableEntry<K,V> prev = null;
		while(temp != null) {
			if(temp.key.equals(key)) {
				if(prev != null) {
					prev.next = temp.next;
				}else {
					array[index] = temp.next;
				}
				size--;
				V oldValue = temp.value;
				temp.value = null;
				modificationCount++;
				return oldValue;
			}
			prev = temp;
			temp = temp.next;
		}
		
		return null;
	}
		
	
	/**Method returns new iterator.
	 *
	 */
	public Iterator<SimpleHashtable.TableEntry<K,V>> iterator(){
		return new IteratorImpl(this);
	}

}
