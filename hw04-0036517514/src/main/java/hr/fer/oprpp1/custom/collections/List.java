package hr.fer.oprpp1.custom.collections;

public interface List<T> extends Collection<T>{
	
	/**Returns the object that is stored in linked list at position index. Valid indexes are
	 * 0 to size - 1.
	 * @param index at which the object is that we wish to get
	 * @return Object that is at the wished index
	 * @throws IndexOutOfBoundsException
	 */
	public T get(int index);
	
	/**Inserts(does not overwrite) the given value at the given position in linked-list. 
	 * Elements starting from this position are shifted one position. The legal positions are 
	 * 0 to size. 
	 * @param value that is to be inserted 
	 * @param position at which the value is to be inserted
	 * @throws IndexOutOfBoundsException
	 */
	public void insert(T value, int position);
	
	/**Searches the collection and returns the index of the first occurrence of the given value
	 * or -1 if the value is not found.
	 * @param value whose index is being searched
	 * @return value of index that is being searched for
	 */
	public int indexOf(Object value);
	
	/**Removes element at specified index from collection. Legal indexes are 0 to size - 1.
	 * @param index at which the element should be removed
	 * @throws IndexOutOfBoundsException
	 */
	public void remove(int index);
}
