package hr.fer.oprpp1.hw04.db;

import java.util.List;

public class QueryFilter implements IFilter{
	private List<ConditionalExpression> exprList;

	public QueryFilter(List<ConditionalExpression> exprList) {
		this.exprList = exprList;
	}
	
	@Override
	public boolean accepts(StudentRecord record) {
		
		boolean satisfies = true;
		
		for(int i = 0; i < exprList.size(); i++) {
			ConditionalExpression expr = exprList.get(i);
			satisfies = satisfies && expr.getComparisonOperator().satisfied(expr.getFieldGetter().get(record), expr.getStringLiteral());
		}
		
		return satisfies;
		
	}

}
