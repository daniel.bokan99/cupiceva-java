package hr.fer.oprpp1.hw04.db;

import java.util.ArrayList;
import java.util.List;

public class QueryLexer {
	
	private char[] data;
	private List<Token> tokens;
	
	
	public QueryLexer(String data) {
		this.data = data.toCharArray();
		this.tokens = new ArrayList<>();
		analyze();
	}
	
	private void analyze() {
		
		if(data.length == 0) {
			return;
		}
		
		for(int i = 0; i < data.length; i++) {
			
			String literal = "";
			String operator = "";
			String number = "";
			
			
			if(Character.isLetter(data[i]) || data[i] == '*') {
				while(Character.isLetter(data[i]) || data[i] == '*') {
					literal += String.valueOf(data[i]);
					i++;
				}
				
				if(literal.equals("jmbag") || literal.equals("firstName") || literal.equals("lastName") || literal.equals("finalGrade")) {
					tokens.add(new Token(TokenType.LITERAL, literal));
					i--;
					continue;
				}else if(literal.toUpperCase().equals("LIKE")){
					tokens.add(new Token(TokenType.OPERATOR, literal));
					i--;
					continue;
				}else if(literal.toUpperCase().equals("AND")) {
					tokens.add(new Token(TokenType.AND, literal));
					i--;
					continue;
				}else if(literal.contains("*")) {
					tokens.add(new Token(TokenType.LITERAL, literal));
					i--;
					continue;
				}else {
					tokens.add(new Token(TokenType.LITERAL, literal));
					i--;
					continue;
				}
			}
			
			if(data[i] == '<' || data[i] == '>') {
				if(data[i + 1] == '=') continue; 
				operator = String.valueOf(data[i]);
				tokens.add(new Token(TokenType.OPERATOR, operator));
				continue;
			}
			
			if(data[i] == '=') {
				if(data[i - 1] == '!' || data[i - 1] == '<' || data[i - 1] == '>') {
					operator = String.valueOf(data[i - 1]) + String.valueOf(data[i]);
				}else {
					operator = String.valueOf(data[i]);
				}
				
				tokens.add(new Token(TokenType.OPERATOR, operator));
				continue;
			}
			
			if(Character.isDigit(data[i])) {
				while(Character.isDigit(data[i])) {
					number += String.valueOf(data[i]);
					i++;
				}
				
				tokens.add(new Token(TokenType.NUMBER, number));
				i--;
				continue;
				
			}
			
		}
		
		tokens.add(new Token(TokenType.EOF, null));
		
	}
	
	public List<Token> getTokenList(){
		return tokens;
	}

}
