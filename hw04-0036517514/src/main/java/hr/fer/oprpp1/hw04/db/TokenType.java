package hr.fer.oprpp1.hw04.db;

public enum TokenType {
	LITERAL, OPERATOR, EOF, AND, NUMBER
}
