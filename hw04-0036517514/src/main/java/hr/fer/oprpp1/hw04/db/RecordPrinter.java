package hr.fer.oprpp1.hw04.db;

import java.util.List;

public class RecordPrinter {
	
	public static void print(List<StudentRecord> records) {
		if(records.size() > 0) {
			
			int maxFirstName = 0;
			int maxLastName = 0;
			for(StudentRecord rec : records) {
				if(rec.getFirstName().length() > maxFirstName) maxFirstName = rec.getFirstName().length();
				if(rec.getLastName().length() > maxLastName) maxLastName = rec.getLastName().length();
			}
			
			System.out.printf("+============+=");
			for(int i = 0; i < maxLastName; i++) {
				System.out.printf("=");
			}
			System.out.printf("=+=");
			for(int i = 0; i < maxFirstName; i++) {
				System.out.printf("=");
			}
			System.out.printf("=+===+%n");
			
			for(int i = 0; i < records.size(); i++) {
				StudentRecord rec = records.get(i);
				System.out.printf("| %s | %s", rec.getJmbag(), rec.getLastName());
				for(int j = 0; j < maxLastName - rec.getLastName().length(); j++) {
					System.out.printf(" ");
				}
				System.out.printf(" | %s ", rec.getFirstName());
				for(int j = 0; j < maxFirstName - rec.getFirstName().length(); j++) {
					System.out.printf(" ");
				}
				System.out.printf("| %s |%n", rec.getFinalGrade());
			}
			
			System.out.printf("+============+=");
			for(int i = 0; i < maxLastName; i++) {
				System.out.printf("=");
			}
			System.out.printf("=+=");
			for(int i = 0; i < maxFirstName; i++) {
				System.out.printf("=");
			}
			System.out.printf("=+===+%n");
		}
		
		System.out.println("Records selected: " + records.size());
	}

}
