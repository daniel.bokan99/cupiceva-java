package hr.fer.oprpp1.hw04.db;

public class ConditionalExpression {
	
	private IFieldValueGetter fieldGetter;
	private String stringLiteral;
	private IComparisonOperator comparisonOperator;
	
	public ConditionalExpression(IFieldValueGetter strategy, String literal, IComparisonOperator comparisonOperator) {
		this.fieldGetter = strategy;
		this.stringLiteral = literal;
		this.comparisonOperator = comparisonOperator;
	}

	public IFieldValueGetter getFieldGetter() {
		return fieldGetter;
	}

	public String getStringLiteral() {
		return stringLiteral;
	}

	public IComparisonOperator getComparisonOperator() {
		return comparisonOperator;
	}
}
