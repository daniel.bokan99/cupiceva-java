package hr.fer.oprpp1.hw04.db.demo;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

import hr.fer.oprpp1.hw04.db.QueryParser;
import hr.fer.oprpp1.hw04.db.RecordPrinter;
import hr.fer.oprpp1.hw04.db.StudentDatabase;
import hr.fer.oprpp1.hw04.db.StudentRecord;

public class Demo {

	public static void main(String[] args) {
		List<String> lines = null;
		try {
			lines = Files.readAllLines(Paths.get("C:\\Faks\\OPRPP1\\Zadace\\hw04-0036517514\\src\\test\\resources\\database.txt"),StandardCharsets.UTF_8);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		StudentDatabase dataBase = new StudentDatabase(lines);
		
		
		Scanner scanner = new Scanner(System.in);
		QueryParser parser;
		String input = scanner.nextLine();
		while(!(input.equals("exit"))) {
			String[] splitted = input.split("\\s+");
			if(splitted[0].equals("query")) {
				
				String data = "";
				
				for(int i = 1; i < splitted.length; i++) {
					data += splitted[i] + " ";
				}
				parser = new QueryParser(data, dataBase);
				List<StudentRecord> queriedRecords = parser.getRecords();
				RecordPrinter.print(queriedRecords);
						
			}else {
				System.out.println("Incorrect query. Try again.");
			}
			input = scanner.nextLine();
		}
		scanner.close();

	}
	
}
