package hr.fer.oprpp1.hw04.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StudentDatabase {
	
	private Map<String,Integer> studentRecordsMap;
	private List<StudentRecord> studentRecordsList; 
	
	public StudentDatabase(List<String> input) {
		studentRecordsMap = new HashMap<>();
		studentRecordsList = new ArrayList<>();
		
		for(int i = 0; i < input.size(); i++) {
			String[] data = input.get(i).split("\\s+");
			if(data.length != 4 && data.length != 5)
				throw new IllegalArgumentException("Invalid number of arguments");
			
			String jmbag, firstName, lastName, finalGrade;
			jmbag = data[0];
			if(data.length == 4) {
				lastName = data[1];
				firstName = data[2];
				finalGrade = data[3];
			}else{
				lastName = data[1] + " " + data[2];
				firstName = data[3];
				finalGrade = data[4];
			}
			
			if(!studentRecordsMap.containsKey(jmbag) && Integer.parseInt(finalGrade) >= 1 && Integer.parseInt(finalGrade) <= 5) {
				StudentRecord tempRecord = new StudentRecord(jmbag, firstName, lastName, finalGrade);
				studentRecordsList.add(tempRecord);
				studentRecordsMap.put(jmbag, i);
			}
		}
	}
	
	public StudentRecord forJMBAG(String jmbag) {
		Integer index = studentRecordsMap.get(jmbag);
		if(index == null) return null;
		StudentRecord record = studentRecordsList.get(index);
		return record;
	}
	
	public List<StudentRecord> filter(IFilter filter){
		List<StudentRecord> list = new ArrayList<>();
		
		for(int i = 0; i < studentRecordsList.size(); i++) {
			if(filter.accepts(studentRecordsList.get(i))) {
				list.add(studentRecordsList.get(i));
			}
		}
		
		return list;
	}
	

}
