package hr.fer.oprpp1.hw04.db;

import java.util.ArrayList;
import java.util.List;

public class QueryParser {

	private List<Token> tokenList;
	private StudentDatabase db;
	
	public QueryParser(String data, StudentDatabase db) {
		this.db = db;
		QueryLexer lexer = new QueryLexer(data);
		tokenList = lexer.getTokenList();
	}
	
	public List<StudentRecord> getRecords() {
		
		List<ConditionalExpression> exprList = getQuery();
		QueryFilter filter = new QueryFilter(exprList);
		
		List<StudentRecord> recordList = db.filter(filter);
		
		return recordList;
	}
	
	public boolean isDirectQuery() {
		if(tokenList.size() == 3 && tokenList.get(0).getValue().equals("jmbag")
				&& tokenList.get(1).getValue().equals("=") 
				&& tokenList.get(2).getType() == TokenType.NUMBER) {
			return true;
		}
		return false;
	}
	
	public String getQueriedJMBAG() {
		if(!isDirectQuery()) throw new IllegalStateException("Query is not a direct query!");
		return (String)tokenList.get(2).getValue();
		
	}
	
	public List<ConditionalExpression> getQuery(){
		
		List<ConditionalExpression> list = new ArrayList<>();
		
		ConditionalExpression expr;
		IFieldValueGetter getter = null; 
		String literal = null;
		IComparisonOperator operator = null;
		
		for(int i = 0; i < tokenList.size(); i++) {
			Token token = tokenList.get(i);
			
			if(token.getType() == TokenType.LITERAL) {
				if(token.getValue().equals("firstName")) {
					getter = FieldValueGetters.FIRST_NAME;
					continue;
				}else if(token.getValue().equals("lastName")) {
					getter = FieldValueGetters.LAST_NAME;
					continue;
				}else if(token.getValue().equals("jmbag")) {
					getter = FieldValueGetters.JMBAG;
					continue;
				}
				
			}
			
			if(token.getType() == TokenType.OPERATOR) {
				switch ((String)token.getValue()) {
				case "=":
					operator = ComparisonOperators.EQUALS;
					break;
				case "!=":
					operator = ComparisonOperators.NOT_EQUALS;
					break;
				case "<=":
					operator = ComparisonOperators.LESS_OR_EQUALS;
					break;
				case ">=":
					operator = ComparisonOperators.GREATER_OR_EQUALS;
					break;
				case "<":
					operator = ComparisonOperators.LESS;
					break;
				case ">":
					operator = ComparisonOperators.GREATER;
					break;
				case "LIKE":
					operator = ComparisonOperators.LIKE;
					break;
				default:
					break;
				}
				continue;
			}
			
			if(token.getType() == TokenType.LITERAL || token.getType() == TokenType.NUMBER) {
				literal = (String)token.getValue();
				continue;
			}
			
			if(token.getType() == TokenType.AND || token.getType() == TokenType.EOF) {
				expr = new ConditionalExpression(getter, literal, operator);
				list.add(expr);
				expr = null;
				getter = null;
				literal = null;
				operator = null;
			}
		}
		
		return list;
	}
	

	
	
}
