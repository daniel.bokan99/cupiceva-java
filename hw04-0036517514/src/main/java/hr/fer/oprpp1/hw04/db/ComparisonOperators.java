package hr.fer.oprpp1.hw04.db;

public class ComparisonOperators {
	
	public static final IComparisonOperator LESS;
	public static final IComparisonOperator LESS_OR_EQUALS;
	public static final IComparisonOperator GREATER;
	public static final IComparisonOperator GREATER_OR_EQUALS;
	public static final IComparisonOperator EQUALS;
	public static final IComparisonOperator NOT_EQUALS;
	public static final IComparisonOperator LIKE;
	
	static {
		LESS = (value1, value2) -> {
			return value1.compareTo(value2) < 0;
		};
		
		LESS_OR_EQUALS = (value1, value2) -> {
			return value1.compareTo(value2) <= 0;
		};
		
		GREATER = (value1, value2) -> {
			return value1.compareTo(value2) > 0;
		};
		
		GREATER_OR_EQUALS = (value1, value2) -> {
			return value1.compareTo(value2) >= 0;
		};
		
		EQUALS = (value1, value2) -> {
			return value1.compareTo(value2) == 0;
		};
		
		NOT_EQUALS = (value1, value2) -> {
			return value1.compareTo(value2) != 0;
		};
		
		LIKE = (value1, value2) -> {
			
			String[] splitted = value2.split("\\*");
			if(splitted.length == 2) {
				if(value1.startsWith(splitted[0]) && value1.endsWith(splitted[1])) {
					if(value1.length() >= value2.length() - 1) return true;
				}
				return false;
			}else if (splitted.length == 1) {
				if(value2.startsWith("*")) {
					if(value1.endsWith(splitted[0])) return true;
					return false;
				}else {
					if(value1.startsWith(splitted[0])) return true;
					return false;
				}
			}else {
				throw new IllegalArgumentException("Too many * in argument");
			}
			
		};
		
	}

}
