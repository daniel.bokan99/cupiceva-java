package hr.fer.oprpp1.hw08.local;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;

public class FormLocalizationProvider extends LocalizationProviderBridge{
	
	public FormLocalizationProvider(LocalizationProvider lp, JFrame frame) {
		super(lp);
		
		WindowListener wl = new WindowAdapter(){
			
			@Override
			public void windowOpened(WindowEvent e) {
				FormLocalizationProvider.super.connect();
			}
			
			@Override
			public void windowClosing(WindowEvent e) {
				FormLocalizationProvider.super.disconnect();
			}
		};
		
		frame.addWindowListener(wl);
		
	}

}
