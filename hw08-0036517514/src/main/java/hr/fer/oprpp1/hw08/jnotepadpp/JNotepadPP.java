package hr.fer.oprpp1.hw08.jnotepadpp;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.nio.file.Path;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;

import hr.fer.oprpp1.hw08.local.FormLocalizationProvider;
import hr.fer.oprpp1.hw08.local.LJMenu;
import hr.fer.oprpp1.hw08.local.LocalizableAction;
import hr.fer.oprpp1.hw08.local.LocalizationProvider;

public class JNotepadPP extends JFrame {

	private static final long serialVersionUID = 1L;
	private DefaultMultipleDocumentModel documents;
	private Clock clock;
	private FormLocalizationProvider flp = new FormLocalizationProvider(LocalizationProvider.getInstance(), this);

	public JNotepadPP() {
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		setTitle("JNotepad++ v1.0");
		initGUI();
		setSize(600,300);

		this.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				boolean hasModified = false;
				List<SingleDocumentModel> list = new ArrayList<>();
				
				for (var d : documents) {
					if (d.isModified()) {
						hasModified = true;
						list.add(d);
					}
				}

				String[] opcije = new String[] { flp.getString("yes"), flp.getString("no"), flp.getString("cancel")};

				if (hasModified) {
					int result = JOptionPane.showOptionDialog(JNotepadPP.this,
							flp.getString("warningMsg"), flp.getString("warning"),
							JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, opcije, opcije[0]);
					
					switch(result) {
					case JOptionPane.CLOSED_OPTION:
						case 2:
							return;
					case 0:
						for(var d : list) {
							JNotepadPP.this.documents.saveDocument(d, d.getFilePath());
						}
						clock.setDispose(true);
						dispose();
						break;
					case 1:
						clock.setDispose(true);
						dispose();
						break;
					}
					
				}else {
					clock.setDispose(true);
					dispose();
				}
			}

		});

	}

	private void initGUI() {

		documents = new DefaultMultipleDocumentModel(this, flp);

		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		cp.add(documents, BorderLayout.CENTER);

		createActions();
		createMenu();
		createToolbar();
		createStatusBar();
	}

	private Action newDocumentAction = (Action)new LocalizableAction("new", "newDesc", flp) {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			documents.createNewDocument();
		}
	};

	private Action openDocumentAction = (Action)new LocalizableAction("open", "openDesc", flp) {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser fc = new JFileChooser();
			fc.setDialogTitle("Open file");
			if (fc.showOpenDialog(JNotepadPP.this) != JFileChooser.APPROVE_OPTION) {
				return;
			}
			File fileName = fc.getSelectedFile();
			Path filePath = fileName.toPath();
			documents.loadDocument(filePath);
		}
	};

	private Action saveDocumentAction = (Action)new LocalizableAction("save", "saveDesc", flp) {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			documents.saveDocument(documents.getCurrentDocument(), documents.getCurrentDocument().getFilePath());
		}
	};
	
	private Action saveAsDocumentAction = (Action)new LocalizableAction("saveAs", "saveAsDesc", flp) {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			documents.saveDocument(documents.getCurrentDocument(), null);
		}
	};

	private Action closeDocumentAction = (Action)new LocalizableAction("close", "closeDesc", flp) {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			documents.closeDocument(documents.getCurrentDocument());
		}
	};
	
	private Action exitApplicationAction = (Action)new LocalizableAction("exit", "exitDesc", flp) {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			JNotepadPP.this.dispatchEvent(new WindowEvent(JNotepadPP.this, WindowEvent.WINDOW_CLOSING));
		}
	};
	
	private Action cutAction = (Action)new LocalizableAction("cut", "cutDesc", flp) {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			JTextArea editor = documents.getCurrentDocument().getTextComponent();
			Document doc = documents.getCurrentDocument().getTextComponent().getDocument();
			int len = Math.abs(editor.getCaret().getDot()-editor.getCaret().getMark());
			if(len==0) return;
			int offset = Math.min(editor.getCaret().getDot(),editor.getCaret().getMark());
			
			try {
				documents.setClipboard(doc.getText(offset, len));
				doc.remove(offset, len);
			}catch(BadLocationException ex) {
				ex.printStackTrace();
			}
		}
	};
	
	private Action copyAction = (Action)new LocalizableAction("copy", "copyDesc", flp) {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			JTextArea editor = documents.getCurrentDocument().getTextComponent();
			Document doc = documents.getCurrentDocument().getTextComponent().getDocument();
			int len = Math.abs(editor.getCaret().getDot()-editor.getCaret().getMark());
			if(len==0) return;
			int offset = Math.min(editor.getCaret().getDot(),editor.getCaret().getMark());
			
			try {
				documents.setClipboard(doc.getText(offset, len));
			}catch(BadLocationException ex) {
				ex.printStackTrace();
			}
		}
	};
	
	private Action pasteAction = (Action)new LocalizableAction("paste", "pasteDesc", flp) {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			JTextArea editor = documents.getCurrentDocument().getTextComponent();
			Document doc = documents.getCurrentDocument().getTextComponent().getDocument();
			int len = Math.abs(editor.getCaret().getDot()-editor.getCaret().getMark());
			if(len==0) return;
			int offset = Math.min(editor.getCaret().getDot(),editor.getCaret().getMark());
			
			try {
				doc.remove(offset, len);
				doc.insertString(offset, documents.getClipboard(), null);
			}catch(BadLocationException ex) {
				ex.printStackTrace();
			}
		}
	};
	
	private final Action statsDocumentAction = (Action)new LocalizableAction("stats", "statsDesc", flp) {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			JTextArea editor = documents.getCurrentDocument().getTextComponent();
			String text = editor.getText();
			char[] array = text.toCharArray();
			int blankCounter = 0;
			int numberOfChars = editor.getText().length();
			
			for(char c : array) {
				if(c == ' ' || c == '\n' || c == '\t') {
					blankCounter++;
				}
			}
			
			int numberOfNonBlank = numberOfChars - blankCounter;
			int numberOfLines = editor.getLineCount();
			
			String stats = String.format("Dokument ima %d znakova, %d znakova koji nisu praznine i %d linija", numberOfChars, numberOfNonBlank, numberOfLines);
			
			JOptionPane.showMessageDialog(JNotepadPP.this, stats, "Statistika",
					JOptionPane.INFORMATION_MESSAGE);
			
		}
	};
	
	private Action enAction = (Action)new LocalizableAction("english", "englishDesc", flp) {
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			LocalizationProvider.getInstance().setLanguage("en");
		}
	};
	
	private Action deAction = (Action)new LocalizableAction("german", "germanDesc", flp) {
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			LocalizationProvider.getInstance().setLanguage("de");
		}
	};
	
	private Action hrAction = (Action)new LocalizableAction("croatian", "croatianDesc", flp) {
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			LocalizationProvider.getInstance().setLanguage("hr");
		}
	};
	
	private String changeCase(String text, int type) {
		char[] znakovi = text.toCharArray();
		for(int i = 0; i < znakovi.length; i++) {
			char c = znakovi[i];
			
			if(type == 0) {
				znakovi[i] = Character.toUpperCase(c);
			}else if(type == 1) {
				znakovi[i] = Character.toLowerCase(c);
			}else {
				if(Character.isLowerCase(c)) {
					znakovi[i] = Character.toUpperCase(c);
				} else if(Character.isUpperCase(c)) {
					znakovi[i] = Character.toLowerCase(c);
				}
			}
		}
		return new String(znakovi);
	}
	
	private void doCaseAction(int type) {
		JTextArea editor = documents.getCurrentDocument().getTextComponent();
		Document doc = editor.getDocument();
		int len = Math.abs(editor.getCaret().getDot()-editor.getCaret().getMark());
		int offset = 0;
		if(len!=0) {
			offset = Math.min(editor.getCaret().getDot(),editor.getCaret().getMark());
		} else {
			len = doc.getLength();
		}
		try {
			String text = doc.getText(offset, len);
			text = changeCase(text, type);
			doc.remove(offset, len);
			doc.insertString(offset, text, null);
		} catch(BadLocationException ex) {
			ex.printStackTrace();
		}
	}
	
	private Action uppercaseAction = (Action)new LocalizableAction("uppercase", "uppercaseDesc", flp) {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			doCaseAction(0);
		}
	};
	
	private Action lowercaseAction = (Action)new LocalizableAction("lowercase", "lowercaseDesc", flp) {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			doCaseAction(1);
		}
	};
	
	 private Action invertcaseAction = (Action)new LocalizableAction("invertcase", "invertcaseDesc", flp) {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			doCaseAction(3);
		}
	};
	
	private void sort(boolean asc) {
		Locale locale = new Locale(flp.getCurrentLanguage());
		Collator collator = Collator.getInstance(locale);
		
		JTextArea editor = documents.getCurrentDocument().getTextComponent();
		Document doc = editor.getDocument();
		int len = Math.abs(editor.getCaret().getDot()-editor.getCaret().getMark());
		int offset = 0;
		if(len!=0) {
			offset = Math.min(editor.getCaret().getDot(),editor.getCaret().getMark());
			len = Math.max(editor.getCaret().getDot(),editor.getCaret().getMark());
		}else {
			return;
		}
		try {
			int line = editor.getLineOfOffset(offset);
			int line2 = editor.getLineOfOffset(len);
			int lineStart = editor.getLineStartOffset(line);
			int lineEnd = editor.getLineEndOffset(line2);
			
			String text = doc.getText(lineStart, lineEnd);
			String[] array = text.split("\\s+");
			
			Comparator<String> comparator = (o1, o2) -> asc ? collator.compare(o1, o2) : collator.reversed().compare(o1, o2);
			
			Arrays.sort(array, comparator);
			text = Arrays.toString(array);
			doc.remove(lineStart, lineEnd);
			doc.insertString(lineStart, text, null);
		} catch(BadLocationException ex) {
			ex.printStackTrace();
		}
	}
	
	private Action ascendingAction = (Action)new LocalizableAction("ascending", "ascendingDesc", flp) {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			sort(true);
		}
	};
	
	private Action descendingAction = (Action)new LocalizableAction("descending", "descendingDesc", flp) {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			sort(false);
		}
	};
	
	private Action uniqueAction = (Action)new LocalizableAction("unique", "uniqueDesc", flp) {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			JTextArea editor = documents.getCurrentDocument().getTextComponent();
			Document doc = editor.getDocument();
			int len = Math.abs(editor.getCaret().getDot()-editor.getCaret().getMark());
			int offset = 0;
			if(len!=0) {
				offset = Math.min(editor.getCaret().getDot(),editor.getCaret().getMark());
				len = Math.max(editor.getCaret().getDot(),editor.getCaret().getMark());
			}else {
				return;
			}
			try {
				int lineStart = editor.getLineOfOffset(offset);
				int lineEnd = editor.getLineOfOffset(len);
				Element root = doc.getDefaultRootElement();
				Set<String> set = new TreeSet<>();
				for(int i = lineStart; i <= lineEnd; i++) {
					String tekst = root.getElement(i).getDocument().getText(0, root.getElement(i).getDocument().getLength());
					set.add(root.getElement(i).getDocument().getText(0, root.getElement(i).getDocument().getLength()));
				}
				
				doc.remove(offset, len);
				String[] array = set.toArray(new String[] {"String"});
				String text = String.join(" ", array);
				doc.insertString(offset, text, null);
				
			}catch (Exception exc) {
				exc.printStackTrace();
			}
			
		}
	};

	private void createActions() {
		
		openDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control O"));
		openDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_O);

		newDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control N"));
		newDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_N);

		saveDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control S"));
		saveDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);
		
		saveAsDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control shift S"));
		saveAsDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);
		
		statsDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control I"));
		statsDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_I);

		closeDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control W"));
		closeDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_W);
		
		exitApplicationAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control E"));
		exitApplicationAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_E);
		
		cutAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control X"));
		cutAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_X);
		
		copyAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control C"));
		copyAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_C);
		
		pasteAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control V"));
		pasteAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_V);
		
		enAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control V"));
		enAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_V);
		
		deAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control V"));
		deAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_V);
		
		hrAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control V"));
		hrAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_V);
		
		uppercaseAction.setEnabled(false);
		lowercaseAction.setEnabled(false);
		invertcaseAction.setEnabled(false);
		ascendingAction.setEnabled(false);
		descendingAction.setEnabled(false);
		
	}

	private void createMenu() {
		JMenuBar menuBar = new JMenuBar();

		LJMenu fileMenu = new LJMenu("file", flp);
		menuBar.add(fileMenu);
		fileMenu.add(new JMenuItem(openDocumentAction));
		fileMenu.add(new JMenuItem(newDocumentAction));
		fileMenu.add(new JMenuItem(saveDocumentAction));
		fileMenu.add(new JMenuItem(saveAsDocumentAction));
		fileMenu.add(new JMenuItem(statsDocumentAction));
		fileMenu.add(new JMenuItem(closeDocumentAction));
		fileMenu.add(new JMenuItem(exitApplicationAction));
		
		LJMenu editMenu = new LJMenu("edit", flp);
		menuBar.add(editMenu);
		editMenu.add(new JMenuItem(cutAction));
		editMenu.add(new JMenuItem(copyAction));
		editMenu.add(new JMenuItem(pasteAction));
		
		LJMenu languageMenu = new LJMenu("languages", flp);
		menuBar.add(languageMenu);
		languageMenu.add(new JMenuItem(enAction));
		languageMenu.add(new JMenuItem(deAction));
		languageMenu.add(new JMenuItem(hrAction));
		
		LJMenu toolsMenu = new LJMenu("tools", flp);
		LJMenu changeCase = new LJMenu("changeCase", flp);
		menuBar.add(toolsMenu);
		toolsMenu.add(changeCase);
		changeCase.add(new JMenuItem(uppercaseAction));
		changeCase.add(new JMenuItem(lowercaseAction));
		changeCase.add(new JMenuItem(invertcaseAction));
		
		LJMenu sort = new LJMenu("sort", flp);
		toolsMenu.add(sort);
		sort.add(new JMenuItem(ascendingAction));
		sort.add(new JMenuItem(descendingAction));
		
		toolsMenu.add(new JMenuItem(uniqueAction));
		

		this.setJMenuBar(menuBar);
	}
	
	private void createToolbar() {
		JToolBar toolBar = new JToolBar("Alati");
		toolBar.setFloatable(true);
		
		toolBar.add(new JButton(openDocumentAction));
		toolBar.add(new JButton(newDocumentAction));
		toolBar.add(new JButton(saveDocumentAction));
		toolBar.add(new JButton(saveAsDocumentAction));
		toolBar.add(new JButton(statsDocumentAction));
		toolBar.add(new JButton(closeDocumentAction));
		toolBar.add(new JButton(exitApplicationAction));
		toolBar.addSeparator();
		toolBar.add(new JButton(cutAction));
		toolBar.add(new JButton(copyAction));
		toolBar.add(new JButton(pasteAction));
//		toolBar.addSeparator();
//		LJMenu toolsMenu = new LJMenu("tools", flp);
//		LJMenu changeCase = new LJMenu("changeCase", flp);
//		toolBar.add(toolsMenu);
//		toolsMenu.add(changeCase);
//		changeCase.add(new JMenuItem(uppercaseAction));
//		changeCase.add(new JMenuItem(lowercaseAction));
//		changeCase.add(new JMenuItem(invertcaseAction));
		
		getContentPane().add(toolBar, BorderLayout.PAGE_START);
	}
	
	private void createStatusBar() {
		
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(1, 5));

		JLabel label1 = new JLabel("Length: 0");
		JLabel label2 = new JLabel("Ln: 0");
		JLabel label3 = new JLabel("Col: 0");
		JLabel label4 = new JLabel("Sel: 0");
		clock = new Clock();
		panel.add(label1);
		panel.add(label2);
		panel.add(label3);
		panel.add(label4);
		panel.add(clock);
		panel.setBorder(BorderFactory.createLineBorder(Color.black, 1));
		
		documents.setStatusBar(panel);
		
		getContentPane().add(panel, BorderLayout.PAGE_END);
	}
	
	public Action getUppercase() {
		return uppercaseAction;
	}
	
	public Action getLowercase() {
		return lowercaseAction;
	}
	
	public Action getinvertCase() {
		return invertcaseAction;
	}
	
	public Action getAscendingAction() {
		return ascendingAction;
	}
	
	public Action getDescendingAction() {
		return descendingAction;
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			new JNotepadPP().setVisible(true);
		});
	}

}
