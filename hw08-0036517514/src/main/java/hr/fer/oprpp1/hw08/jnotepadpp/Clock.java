package hr.fer.oprpp1.hw08.jnotepadpp;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import javax.swing.JLabel;
import javax.swing.SwingUtilities;

public class Clock extends JLabel{
	private static final long serialVersionUID = 1L;
	
	private volatile String time;
	private DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy/MM/dd");
	private DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("HH:mm:ss");
	private boolean dispose;
	
	public Clock() {
		updateTime();
		
		Thread t = new Thread(()->{
			while(true) {
				try {
					Thread.sleep(900);
				}catch(Exception e) {}
				SwingUtilities.invokeLater(()->{
					updateTime();
				});
				if(dispose){
					System.exit(1);
				}
			}
		});
		t.setDaemon(true);
		t.start();
		
	}
	
	private void updateTime() {
		time = formatter1.format(LocalDate.now()) + " " + formatter2.format(LocalTime.now());
		this.setText(time);
	}
	
	public void setDispose(boolean b) {
		dispose = b;
	}
}
