package hr.fer.oprpp1.hw08.local;

import java.util.Locale;
import java.util.ResourceBundle;

public class LocalizationProvider extends AbstractLocalizationProvider{
	
	private String language;
	private ResourceBundle bundle;
	private static final LocalizationProvider instance = new LocalizationProvider();
	
	private LocalizationProvider() {
		super();
		this.language = "hr";
		bundle = ResourceBundle.getBundle("hr.fer.oprpp1.hw08.local.prijevodi", Locale.forLanguageTag(language));
	}
	
	public static LocalizationProvider getInstance() {
		return instance;
	}
	
	public void setLanguage(String language) {
		this.language = language;
		bundle = ResourceBundle.getBundle("hr.fer.oprpp1.hw08.local.prijevodi", Locale.forLanguageTag(language));
		fire();
	}
	
	@Override
	public String getString(String s) {
		return bundle.getString(s);
	}

	@Override
	public String getCurrentLanguage() {
		return this.language;
	}

}
