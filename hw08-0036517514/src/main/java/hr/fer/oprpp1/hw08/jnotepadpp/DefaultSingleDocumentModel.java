package hr.fer.oprpp1.hw08.jnotepadpp;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class DefaultSingleDocumentModel implements SingleDocumentModel{
	
	private Path filePath;
	private JTextArea textArea;
	private boolean modified;
	private List<SingleDocumentListener> listeners;
	
	public DefaultSingleDocumentModel(Path path, String textContent) {
		
		this.filePath = path;
		modified = false;
		textArea = new JTextArea(textContent);
		listeners = new ArrayList<>();
		
		textArea.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				DefaultSingleDocumentModel.this.setModified(true);
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				DefaultSingleDocumentModel.this.setModified(true);
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				DefaultSingleDocumentModel.this.setModified(true);
			}
		});
		
	}
	
	public DefaultSingleDocumentModel() {
		this(null,null);
	}
	
	@Override
	public JTextArea getTextComponent() {
		return textArea;
	}

	@Override
	public Path getFilePath() {
		return filePath;
	}

	@Override
	public void setFilePath(Path path) {
		if(path == null) {
			throw new IllegalArgumentException("Path cannot be null");
		}
		this.filePath = path;
		for(var l : listeners) {
			l.documentFilePathUpdated(this);
		}
	}

	@Override
	public boolean isModified() {
		return modified;
	}

	@Override
	public void setModified(boolean modified) {
		this.modified = modified;
		for(var l : listeners) {
			l.documentModifyStatusUpdated(this);
		}
	}

	@Override
	public void addSingleDocumentListener(SingleDocumentListener l) {
		listeners.add(l);
	}

	@Override
	public void removeSingleDocumentListener(SingleDocumentListener l) {
		listeners.remove(l);
	}
	
	
	
}
