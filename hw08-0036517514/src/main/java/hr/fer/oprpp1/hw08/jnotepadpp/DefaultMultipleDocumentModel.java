package hr.fer.oprpp1.hw08.jnotepadpp;


import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import hr.fer.oprpp1.hw08.local.ILocalizationProvider;

public class DefaultMultipleDocumentModel extends JTabbedPane implements MultipleDocumentModel {

	private static final long serialVersionUID = 1L;
	private List<SingleDocumentModel> documents;
	private List<MultipleDocumentListener> listeners;
	private SingleDocumentModel currentDocument;
	private JNotepadPP parent;
	private ImageIcon savedIcon;
	private ImageIcon unsavedIcon;
	private String clipboard;
	private JPanel statusBar;
	private ILocalizationProvider flp;

	public DefaultMultipleDocumentModel(JNotepadPP parent, ILocalizationProvider flp) {
		documents = new ArrayList<>();
		listeners = new ArrayList<>();
		currentDocument = null;
		this.parent = parent;
		this.clipboard = null;
		this.flp = flp;

		savedIcon = loadIcon("icons/saved.png");
		unsavedIcon = loadIcon("icons/unsaved.png");

		this.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				int index = DefaultMultipleDocumentModel.this.getSelectedIndex();
				if (index != -1) {
					DefaultMultipleDocumentModel.this.setCurrentDocument(documents.get(index));
					JFrame parent = DefaultMultipleDocumentModel.this.parent;
					String path = DefaultMultipleDocumentModel.this.getCurrentDocument().getFilePath() == null
							? flp.getString("unnamed")
							: DefaultMultipleDocumentModel.this.getCurrentDocument().getFilePath().toString();
					parent.setTitle(path + " - JNotepad++");
				}
				
				DefaultMultipleDocumentModel.this.updateStatusBar();

			}
		});
	}
	
	public static class DocumentIterator implements Iterator<SingleDocumentModel>{
		
		private DefaultMultipleDocumentModel model;
		private int currentIndex;
		
		public DocumentIterator(DefaultMultipleDocumentModel model) {
			this.model = model;
			this.currentIndex = 0;
		}

		@Override
		public boolean hasNext() {
			if(currentIndex <= model.documents.size() - 1) {
				return true;
			}
			return false;
		}

		@Override
		public SingleDocumentModel next() {
			SingleDocumentModel document = model.documents.get(currentIndex);
			currentIndex++;
			return document;
		}
		
	}

	@Override
	public Iterator<SingleDocumentModel> iterator() {
		return new DocumentIterator(this);
	}

	@Override
	public SingleDocumentModel createNewDocument() {
		DefaultSingleDocumentModel newDocument = new DefaultSingleDocumentModel(null, null);
		documents.add(newDocument);
		setCurrentDocument(newDocument);
		
		newDocument.addSingleDocumentListener(new SingleDocumentListener() {

			@Override
			public void documentModifyStatusUpdated(SingleDocumentModel model) {
				DefaultMultipleDocumentModel.this.setIconAt(DefaultMultipleDocumentModel.this.getSelectedIndex(),
						DefaultMultipleDocumentModel.this.unsavedIcon);
			}

			@Override
			public void documentFilePathUpdated(SingleDocumentModel model) {
			}
		});
		
		newDocument.getTextComponent().addCaretListener((e) -> {
			updateStatusBar();
			JTextArea editor = currentDocument.getTextComponent();
			int len = Math.abs(editor.getCaret().getDot() - editor.getCaret().getMark());
			boolean enabled = len != 0;
			parent.getinvertCase().setEnabled(enabled);
			parent.getLowercase().setEnabled(enabled);
			parent.getUppercase().setEnabled(enabled);
			parent.getAscendingAction().setEnabled(enabled);
			parent.getDescendingAction().setEnabled(enabled);
		});

		JScrollPane pane = new JScrollPane(newDocument.getTextComponent());

		this.addTab(flp.getString("unnamed"), unsavedIcon, pane);
		this.setSelectedIndex(documents.size() - 1);
		return newDocument;
	}

	private ImageIcon loadIcon(String path) {
		try (InputStream is = this.getClass().getResourceAsStream(path)) {
			if (is == null) {
				throw new IllegalArgumentException("Invalid path");
			}

			try {
				byte[] bytes = is.readAllBytes();
				ImageIcon icon = new ImageIcon(bytes);
				BufferedImage resizedImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
				Graphics2D g2 = resizedImg.createGraphics();

				g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
				g2.drawImage(icon.getImage(), 0, 0, 16, 16, null);
				g2.dispose();
				return new ImageIcon(resizedImg);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}

	public void setCurrentDocument(SingleDocumentModel doc) {
		SingleDocumentModel previousModel = this.currentDocument;
		this.currentDocument = doc;
		for (var l : listeners) {
			l.currentDocumentChanged(previousModel, currentDocument);
		}
	}

	@Override
	public SingleDocumentModel getCurrentDocument() {
		return currentDocument;
	}

	@Override
	public SingleDocumentModel loadDocument(Path filePath) {
		int index = 0;
		for(var d : documents) {
			if(filePath.equals(d.getFilePath())) {
				setCurrentDocument(d);
				this.setSelectedIndex(index);
				this.setTitleAt(this.getSelectedIndex(), currentDocument.getFilePath().getFileName().toString());
				this.setToolTipTextAt(this.getSelectedIndex(), currentDocument.getFilePath().toString());
				parent.setTitle(currentDocument.getFilePath().toString() + " - JNotepad++");
				return d;
			}
			index++;
		}
		
		if (!Files.isReadable(filePath)) {
			JOptionPane.showMessageDialog(this.getParent(),
					"Datoteka " + filePath.toFile().getAbsolutePath() + " ne postoji!", "Pogreška",
					JOptionPane.ERROR_MESSAGE);
			return null;
		}
		byte[] okteti;
		try {
			okteti = Files.readAllBytes(filePath);
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(this.getParent(),
					"Pogreška prilikom čitanja datoteke " + filePath.toFile().getAbsolutePath() + ".", "Pogreška",
					JOptionPane.ERROR_MESSAGE);
			return null;
		}
		String text = new String(okteti, StandardCharsets.UTF_8);
		DefaultSingleDocumentModel doc = new DefaultSingleDocumentModel(filePath, text);
		documents.add(doc);
		setCurrentDocument(doc);

		doc.addSingleDocumentListener(new SingleDocumentListener() {

			@Override
			public void documentModifyStatusUpdated(SingleDocumentModel model) {
				DefaultMultipleDocumentModel.this.setIconAt(DefaultMultipleDocumentModel.this.getSelectedIndex(),
						DefaultMultipleDocumentModel.this.unsavedIcon);
			}

			@Override
			public void documentFilePathUpdated(SingleDocumentModel model) {
			}
		});

		JScrollPane pane = new JScrollPane(doc.getTextComponent());

		this.addTab(doc.getFilePath().getFileName().toString(), savedIcon, pane);
		this.setSelectedComponent(pane);
		this.setTitleAt(this.getSelectedIndex(), currentDocument.getFilePath().getFileName().toString());
		this.setToolTipTextAt(this.getSelectedIndex(), currentDocument.getFilePath().toString());
		parent.setTitle(currentDocument.getFilePath().toString() + " - JNotepad++");
		
		doc.getTextComponent().addCaretListener((e) -> {
			updateStatusBar();
			JTextArea editor = currentDocument.getTextComponent();
			int len = Math.abs(editor.getCaret().getDot() - editor.getCaret().getMark());
			boolean enabled = len != 0;
			parent.getinvertCase().setEnabled(enabled);
			parent.getLowercase().setEnabled(enabled);
			parent.getUppercase().setEnabled(enabled);
			parent.getAscendingAction().setEnabled(enabled);
			parent.getDescendingAction().setEnabled(enabled);
		});

		return doc;
	}
	
	private void helpSave(SingleDocumentModel model, Path newPath) {
		byte[] podatci = model.getTextComponent().getText().getBytes(StandardCharsets.UTF_8);
		try {
			Files.write(newPath, podatci);
		} catch (IOException e1) {
			JOptionPane.showMessageDialog(this.getParent(),
					"Pogreška prilikom zapisivanja datoteke " + model.getFilePath().toFile().getAbsolutePath()
							+ ".\nPažnja: nije jasno u kojem je stanju datoteka na disku!",
					"Pogreška", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		JOptionPane.showMessageDialog(this.getParent(), flp.getString("fileSavedMsg"), flp.getString("information"),
				JOptionPane.INFORMATION_MESSAGE);
		
		model.setFilePath(newPath);
		currentDocument.setModified(false);

		this.setTitleAt(this.getSelectedIndex(), currentDocument.getFilePath().getFileName().toString());
		this.setToolTipTextAt(this.getSelectedIndex(), currentDocument.getFilePath().toString());
		this.setIconAt(this.getSelectedIndex(), savedIcon);
		parent.setTitle(currentDocument.getFilePath().toString() + " - JNotepad++");
		return;
	}

	@Override
	public void saveDocument(SingleDocumentModel model, Path newPath) {
		boolean wasNull = newPath == null;
		
		if (newPath == null) {
			JFileChooser jfc = new JFileChooser();
			jfc.setDialogTitle("Save document");
			if (jfc.showSaveDialog(this.getParent()) != JFileChooser.APPROVE_OPTION) {
				JOptionPane.showMessageDialog(this.getParent(), "Ništa nije snimljeno.", "Upozorenje",
						JOptionPane.WARNING_MESSAGE);
				return;
			}
			newPath = jfc.getSelectedFile().toPath();
		}
		
		if(wasNull && newPath.toFile().exists()) {
			String[] options = new String[] { flp.getString("yes"), flp.getString("no"), flp.getString("cancel")};
			
			int result = JOptionPane.showOptionDialog(this.getParent(),
					flp.getString("fileExistsError"), flp.getString("warning"),
					JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
			
			switch(result) {
			case JOptionPane.CLOSED_OPTION:
				return;
			case 0:
				helpSave(model, newPath);
				return;
			case 1:
				JOptionPane.showMessageDialog(this.getParent(),
						flp.getString("pathError"),
						flp.getString("Error"), JOptionPane.ERROR_MESSAGE);
				saveDocument(model, null);
				return;
			case 2:
				return;
			}
		}else {
			helpSave(model, newPath);
		}
	}

	@Override
	public void closeDocument(SingleDocumentModel model) {
		int index = documents.indexOf(model);
		
		if(model.isModified()) {
			
			String[] opcije = new String[] { flp.getString("yes"), flp.getString("no"), flp.getString("cancel")};
			
			int result = JOptionPane.showOptionDialog(this.getParent(),
					flp.getString("warningMsg"), flp.getString("warning"),
					JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, opcije, opcije[0]);
			
			switch(result) {
			case JOptionPane.CLOSED_OPTION:
				return;
			case 0:
				saveDocument(model, model.getFilePath());
				break;
			case 1:
				break;
			case 2:
				return;
			}
			
			if (index != 0) {
				setCurrentDocument(documents.get(index - 1));
				this.setSelectedIndex(index - 1);
				documents.remove(model);
			} else if (index != documents.size() - 1) {
				setCurrentDocument(documents.get(index + 1));
				this.setSelectedIndex(index + 1);
				documents.remove(model);
			} else {
				setCurrentDocument(null);
				documents.remove(model);
			}

			this.remove(index);

			if (documents.size() != 0) {
//				this.setTitleAt(this.getSelectedIndex(), currentDocument.getFilePath().getFileName().toString());
//				this.setToolTipTextAt(this.getSelectedIndex(), currentDocument.getFilePath().toString());
				Path path = currentDocument.getFilePath();
				if(path != null) {
					String name = path.toString();
					parent.setTitle(name + " - JNotepad++");
				}else {
					parent.setTitle(flp.getString("unnamed") + " - JNotepad++");
				}
				
			} else {
				parent.setTitle("JNotepad++");
			}
			
		}else {
			if (index != 0) {
				setCurrentDocument(documents.get(index - 1));
				this.setSelectedIndex(index - 1);
				documents.remove(model);
			} else if (index != documents.size() - 1) {
				setCurrentDocument(documents.get(index + 1));
				this.setSelectedIndex(index + 1);
				documents.remove(model);
			} else {
				setCurrentDocument(null);
				documents.remove(model);
			}

			this.remove(index);

			if (documents.size() != 0) {
//				this.setTitleAt(this.getSelectedIndex(), currentDocument.getFilePath().getFileName().toString());
//				this.setToolTipTextAt(this.getSelectedIndex(), currentDocument.getFilePath().toString());
				Path path = currentDocument.getFilePath();
				if(path != null) {
					String name = path.toString();
					parent.setTitle(name + " - JNotepad++");
				}else {
					parent.setTitle(flp.getString("unnamed") + " - JNotepad++");
				}
				
			} else {
				parent.setTitle("JNotepad++");
			}
		}
	}

	@Override
	public void addMultipleDocumentListener(MultipleDocumentListener l) {
		listeners.add(l);
	}

	@Override
	public void removeMultipleDocumentListener(MultipleDocumentListener l) {
		listeners.remove(l);
	}

	@Override
	public int getNumberOfDocuments() {
		return documents.size();
	}

	@Override
	public SingleDocumentModel getDocument(int index) {
		return documents.get(index);
	}
	
	public void setClipboard(String string) {
		this.clipboard = string;
	}
	
	public String getClipboard() {
		return this.clipboard;
	}
	
	public JPanel getStatusBar() {
		return statusBar;
	}
	
	public void setStatusBar(JPanel panel) {
		this.statusBar = panel;
	}
	
	private void updateStatusBar() {
		int length = 0;
		int line = 0;
		int column = 0;
		int sel = 0;
		
		if(currentDocument != null) {
			JTextArea editor = currentDocument.getTextComponent();
			length = editor.getText().length();
			try {
				int caretPos = editor.getCaretPosition();
				line = editor.getLineOfOffset(caretPos);
				column = caretPos - editor.getLineStartOffset(line) + 1;
				line += 1;
				sel = Math.abs(editor.getCaret().getDot() - editor.getCaret().getMark());
			}catch(Exception exc) {}
		}
		JPanel panel = DefaultMultipleDocumentModel.this.getStatusBar();
		JLabel label0 = ((JLabel)panel.getComponent(0));
		label0.setText("Length: " + length);
		JLabel label1 = ((JLabel)panel.getComponent(1));
		label1.setText("Ln: " + line);
		JLabel label2 = ((JLabel)panel.getComponent(2));
		label2.setText("Col: " + column);
		JLabel label3 = ((JLabel)panel.getComponent(3));
		label3.setText("Sel: " + sel);
	}
}
