package hr.fer.oprpp1.hw08.local;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

public class LocalizableAction extends AbstractAction{
	
	private static final long serialVersionUID = 1L;

	public LocalizableAction(String key, String desc, ILocalizationProvider lp) {
		super();
		this.putValue(Action.NAME, lp.getString(key));
		this.putValue(Action.SHORT_DESCRIPTION, lp.getString(desc));
		lp.addLocalizationListener(() -> {
			LocalizableAction.this.putValue(Action.NAME, lp.getString(key));
			LocalizableAction.this.putValue(Action.SHORT_DESCRIPTION, lp.getString(desc));
		});
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
	}

}
