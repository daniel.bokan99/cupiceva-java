package hr.fer.oprpp1.hw08.local;

import javax.swing.JMenu;

public class LJMenu extends JMenu{
	private static final long serialVersionUID = 1L;
	
	public LJMenu(String key, ILocalizationProvider lp) {
		super();
		this.setText(lp.getString(key));
		lp.addLocalizationListener(() -> LJMenu.this.setText(lp.getString(key)));
	}

}
