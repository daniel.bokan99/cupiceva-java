package hr.fer.oprpp1.hw08.local;

public class LocalizationProviderBridge extends AbstractLocalizationProvider implements ILocalizationListener{
	
	private boolean connected;
	private String language;
	private LocalizationProvider lp;
	
	public LocalizationProviderBridge(LocalizationProvider lp) {
		super();
		this.lp = lp;
		connected = false;
		
	}
	
	public void connect() {
		if(!connected) {
			lp.addLocalizationListener(this);
			connected = true;
			if(!lp.getCurrentLanguage().equals(language)) {
				language = lp.getCurrentLanguage();
			}
		}
	}
	
	public void disconnect() {
		if(connected) {
			lp.removeLocalizationListener(this);
			connected = false;
			language = lp.getCurrentLanguage();
		}
	}
	

	@Override
	public String getString(String s) {
		return lp.getString(s);
	}

	@Override
	public String getCurrentLanguage() {
		return language;
	}

	@Override
	public void localizationChanged() {
		fire();
	}
	
	

}
