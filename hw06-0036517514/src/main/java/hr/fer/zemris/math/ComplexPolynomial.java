package hr.fer.zemris.math;

import java.util.ArrayList;
import java.util.List;

public class ComplexPolynomial {
	
	List<Complex> factors;
	
	public ComplexPolynomial(Complex ... factors) {
		
		this.factors = new ArrayList<>();
		for(Complex c : factors) {
			this.factors.add(c);
		}
	}
	
	public short order() {
		return (short) (factors.size() - 1);
	}
	
	public ComplexPolynomial multiply(ComplexPolynomial p) {
		
		Complex[] product = new Complex[p.factors.size() + factors.size() - 1];
		
		for(int i = 0; i < product.length; i++) {
			product[i] = new Complex(0,0);
		}
		
		for(int i = 0; i < factors.size(); i++) {
			for(int j = 0; j < p.factors.size(); j++) {
				
				product[i + j] = product[i + j].add(factors.get(i).multiply(p.factors.get(j)));
				
			}
		}
		
		return new ComplexPolynomial(product);
	}
	
	public ComplexPolynomial derive() {
		
		int derivativeOrder = order();
		Complex[] derivative = new Complex[derivativeOrder];
		
		for(int i = 0; i < derivativeOrder; i++) {
			derivative[i] = factors.get(i + 1).scale(i + 1);
		}
		
		return new ComplexPolynomial(derivative);
		
	}
	
	public Complex apply(Complex z) {
		
		int order = this.order();
		
		z = z.power(order);
		
		Complex c = factors.get(factors.size() - 1).multiply(z);
		
		for(int i = factors.size() - 2; i >= 0; i--) {
			c = c.add(factors.get(i).multiply(z.power(i)));
		}
		
		return c;
		
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for(int i = factors.size() - 1; i > 0; i--) {
				sb.append("(" + factors.get(i).toString() + ")" + "z^" + i + "+");
		}
		
		sb.append("(" + factors.get(0) + ")");
		
		return sb.toString();
	}

}
