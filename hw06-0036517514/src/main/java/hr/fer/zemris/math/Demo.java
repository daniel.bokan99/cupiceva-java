package hr.fer.zemris.math;

public class Demo {
	
	public static void main(String[] args) {
//		ComplexRootedPolynomial crp = new ComplexRootedPolynomial(
//				new Complex(2,0), Complex.ONE, Complex.ONE_NEG);
//		System.out.println(crp.indexOfClosestRootFor(new Complex(6, 0), 10));
//		System.out.println(crp);
//		System.out.println(cp);
//		System.out.println(cp.derive());
//		System.out.println(cp.derive().order());
		
//		cp = new ComplexPolynomial(new Complex(1,0), new Complex(2,0));
//		System.out.println(cp.apply(new Complex(4,2)));
//		
//		crp = new ComplexRootedPolynomial(new Complex(2,0), new Complex(0,0), new Complex(2,0), new Complex(7,0));
//		System.out.println(crp.apply(new Complex(4,0)));
		
		Complex num1 = new Complex(-3,4);
		Complex num2 = new Complex(3,4);
		Complex num3 = num1.sub(num2);
		System.out.println(num3.module());
		
	}


}
