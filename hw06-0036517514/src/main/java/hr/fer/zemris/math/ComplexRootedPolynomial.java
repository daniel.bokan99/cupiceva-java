package hr.fer.zemris.math;

import java.util.ArrayList;
import java.util.List;


public class ComplexRootedPolynomial {
	
	private Complex constant;
	private List<Complex> roots;
	
	public ComplexRootedPolynomial(Complex constant, Complex ... roots) {
		this.constant = constant;
		this.roots = new ArrayList<>();
		for(Complex c : roots) {
			this.roots.add(c);
		}
	}
	
	public Complex apply (Complex z) {
		Complex result = constant;
		
		for(Complex c : roots) {
			result = result.multiply(z.sub(c));
		}
		
		return result;
	}
	
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("(");
		sb.append(constant.toString());
		sb.append(")");
		
		for(Complex c : roots) {
			sb.append("*(z-(" + c.toString() + "))");
		}
		
		return sb.toString();
		
	}
	
	public ComplexPolynomial toComplexPolynom() {
		
		ComplexPolynomial result = new ComplexPolynomial(this.constant);
		
		for(int i = 0; i < roots.size(); i++) {
			ComplexPolynomial temp = new ComplexPolynomial(roots.get(i),Complex.ONE);
			result = result.multiply(temp);
		}
		
		return result;
		
	}
	
	public int indexOfClosestRootFor(Complex z, double threshold) {
		
		double minDistance = Double.MAX_VALUE;
		int rootIndex = -1;
		for(int i = 0; i < roots.size(); i++) {
			double distance = roots.get(i).sub(z).module();
			
			if(Double.compare(distance, minDistance) < 0) {
				minDistance = distance;
				rootIndex = i;
			}
		}
		
		//System.out.println(minDistance);
		if(Double.compare(minDistance, threshold) < 0) {
			return rootIndex;
		}else {
			return -1;
		}
		
	}

}
