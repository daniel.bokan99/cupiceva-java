package hr.fer.zemris.math;

import java.util.ArrayList;
import java.util.List;

public class Complex {
	
	private double real;
	private double imaginary;
	
	public static final Complex ZERO = new Complex(0,0);
	public static final Complex ONE = new Complex(1,0);
	public static final Complex ONE_NEG = new Complex(-1,0);
	public static final Complex IM = new Complex(0,1);
	public static final Complex IM_NEG = new Complex(0,-1);
	
	public Complex() {
		real = 0;
		imaginary = 0;
	}
	
	public Complex(double real, double imaginary) {
		this.real = real;
		this.imaginary = imaginary;
	}
	
	public double getAngle() {
		return Math.atan2(imaginary, real);
	}
	
	public double module() {
		return Math.sqrt(real*real + imaginary*imaginary);
	}
	
	public Complex multiply(Complex c) {
		double resReal = (double)(this.real*c.real - this.imaginary*c.imaginary);
		double resImaginary = (double)(this.real*c.imaginary + this.imaginary*c.real);
		return new Complex(resReal, resImaginary);
	}
	
	public Complex divide(Complex c) {
		double resReal = (this.real*c.real + this.imaginary*c.imaginary)/(c.real*c.real + c.imaginary*c.imaginary);
		double resImaginary = (this.imaginary*c.real - this.real*c.imaginary)/(c.real*c.real + c.imaginary*c.imaginary);
		return new Complex(resReal, resImaginary);
	}
	
	public Complex add(Complex c) {
		double resReal = this.real + c.real;
		double resImaginary = this.imaginary + c.imaginary;
		return new Complex(resReal, resImaginary);
	}
	
	public Complex sub(Complex c) {
		double resReal = this.real - c.real;
		double resImaginary = this.imaginary - c.imaginary;
		return new Complex(resReal, resImaginary);
	}
	
	public Complex negate() {
		return new Complex(-1 * this.real, -1 * this.imaginary);
	}
	
	public Complex power(int n) {
		if(n < 0) throw new IllegalArgumentException("Power has to be bigger than 0. It is " + n +".");
		
		double magnitude = this.module();
		double angle = this.getAngle();
		double resReal = Math.pow(magnitude, n) * Math.cos(n*angle);
		double resImaginary = Math.pow(magnitude, n) * Math.sin(n*angle);
		
		return new Complex(resReal,resImaginary);
	}
	
	public List<Complex> root(int n){
		List<Complex> list = new ArrayList<>();
		double magnitude = this.module();
		double angle = this.getAngle();
		
		double rootMagn = Math.pow(magnitude, 1/n);
		
		for(int i = 0; i < n; i++) {
			double resReal = rootMagn*Math.cos((angle+2*i*Math.PI)/n);
			double resImaginary = rootMagn*Math.sin((angle+2*i*Math.PI)/n);
			list.add(new Complex(Math.round(resReal),Math.round(resImaginary)));
		}
		
		return list;
	}
	
	@Override
	public String toString() {
		String s = "";
		
		if(this.real == 0 && this.imaginary == 0) {
			s = "0.0+0.0i";
			return s;
		}
		
		boolean realZero = true;
		if(this.real != 0) {
			s += Double.toString(this.real);
			realZero = false;
		}
		if(this.imaginary > 0 && !realZero ) s = s + "+";
		
		if(this.imaginary != 0) {
			if(this.imaginary == 1.0) {
				s = s + "i";
			}else if(this.imaginary == -1.0) {
				s = s + "-i";
			}else {
				s = s + Double.toString(this.imaginary) + "i";
			}
		}
		return s;
	}
	
	public Complex scale(int n) {
		double r = this.real * n;
		double i = this.imaginary * n;
		return new Complex(r, i);
	}
	
	public static Complex parse(String s) {
		
		if(!(s.contains("i"))) {
			char[] array = s.toCharArray();
			String result = "";
			int pom = 0;
			boolean hasNumbers = false;
			boolean dot = false;
			if(array[0] == '+' || array[0] == '-') {
				result += String.valueOf(array[0]);
				pom = 1;
			}
			for(int i = pom; i < array.length; i++) {
				if(Character.isDigit(array[i])) {
					result += String.valueOf(array[i]);
					hasNumbers = true;
				}else if(array[i] == '.' && hasNumbers && !dot) {
					result += String.valueOf(array[i]);
					dot = true;
					continue;
				}else {
					throw new IllegalArgumentException("Unable to parse Complex number!");
				}
			}
			
			return new Complex(Double.parseDouble(result), 0);
			
		}else if(s.endsWith("i")) {
			
			if(s.equals("i") || s.equals("+i")) {
				return new Complex(0, 1);
			}
			
			if(s.equals("-i")) {
				return new Complex(0, -1);
			}
			
			if(s.split("\\+|-").length > 3) {
				throw new IllegalArgumentException("Unable to parse Complex number!");
			}
			
			String real = "";
			String imaginary = "";
			
			char[] array = s.toCharArray();
			
			if(array[0] == '+' || array[0] == '-') {
				real += String.valueOf(array[0]);
			}
			
			String[] str = s.split("[-+i]");
			int i = 0;
			for(; i < str.length; i++) {
				if(!(str[i].equals(""))) {
					real += str[i];
					break;
				}
			}
			i++;
			
			if(s.contains("-")) {
				int index = s.lastIndexOf("-");
				if(index != 0) {
					imaginary += "-";
				}
			}
			
			if(s.contains("+")) {
				int index = s.lastIndexOf("+");
				if(index != 0) {
					imaginary += "+";
				}
			}
			
			for(; i < str.length; i++) {
				if(!(str[i].equals(""))) {
					imaginary += str[i];
					break;
				}
			}
			
			if(imaginary.equals("-")) {
				imaginary = "-1";
			}else if(imaginary.equals("+")) {
				imaginary = "1";
			}else if(imaginary.equals("")) {
				imaginary = real;
				real = "0";
			}
			
			return new Complex(Double.parseDouble(real), Double.parseDouble(imaginary));
			
		}else {
			throw new IllegalArgumentException("Unable to parse Complex number!");
		}
		
	}
	
	public Complex copy() {
		return new Complex(this.real, this.imaginary);
	}
	
	
}
