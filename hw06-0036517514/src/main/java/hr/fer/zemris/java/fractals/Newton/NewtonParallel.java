package hr.fer.zemris.java.fractals.Newton;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import hr.fer.zemris.java.fractals.viewer.FractalViewer;
import hr.fer.zemris.java.fractals.viewer.IFractalProducer;
import hr.fer.zemris.java.fractals.viewer.IFractalResultObserver;
import hr.fer.zemris.math.Complex;
import hr.fer.zemris.math.ComplexPolynomial;
import hr.fer.zemris.math.ComplexRootedPolynomial;

public class NewtonParallel {
	
	public static class PosaoIzracuna implements Runnable {
		double reMin;
		double reMax;
		double imMin;
		double imMax;
		int width;
		int height;
		int yMin;
		int yMax;
		int m;
		short[] data;
		AtomicBoolean cancel;
		public static PosaoIzracuna NO_JOB = new PosaoIzracuna();
		private ComplexPolynomial polynomial;
		private ComplexRootedPolynomial cpr;
		private double convergence_threshold;
		private double root_threshold;
		private int offset;
		private int maxIter;
		
		private PosaoIzracuna() {
		}
		
		public PosaoIzracuna(double reMin, double reMax, double imMin,
				double imMax, int width, int height, int yMin, int yMax, 
				int m, short[] data, AtomicBoolean cancel, ComplexPolynomial polynomial,
				ComplexRootedPolynomial cpr, double convergence_threshold, double root_threshold, 
				int offset, int maxIter) {
			super();
			this.reMin = reMin;
			this.reMax = reMax;
			this.imMin = imMin;
			this.imMax = imMax;
			this.width = width;
			this.height = height;
			this.yMin = yMin;
			this.yMax = yMax;
			this.m = m;
			this.data = data;
			this.cancel = cancel;
			this.polynomial = polynomial;
			this.cpr = cpr;
			this.convergence_threshold = convergence_threshold;
			this.root_threshold = root_threshold;
			this.offset = offset;
			this.maxIter = maxIter;
		}
		
		@Override
		public void run() {
			
			offset = yMin * width;
			
			for(int y = yMin; y <= yMax; y++) {
				if(cancel.get()) break;
				for(int x = 0; x < width; x++) {
					double cre = x / (width-1.0) * (reMax - reMin) + reMin;
					double cim = (height-1.0-y) / (height-1) * (imMax - imMin) + imMin;
					Complex zn = new Complex(cre, cim);
					
					int iters = 0;
					double module = 0;
					do {
						//System.out.println("dogadja se " + iters + ". iter");
						Complex numerator = polynomial.apply(zn);
						Complex denominator = polynomial.derive().apply(zn);
						Complex znOld = zn.copy();
						Complex fraction = numerator.divide(denominator);
						zn = zn.sub(fraction);
						module = znOld.sub(zn).module();
						iters++;
					}while(module > convergence_threshold && iters < maxIter);
					int index = cpr.indexOfClosestRootFor(zn, root_threshold);
					//System.out.println(index);
					data[offset++] = (short) (index + 1);
				}
			}
			
		}
	}
	
	public static class MojProducer implements IFractalProducer{
			
		private ComplexPolynomial polynomial;
		private ComplexRootedPolynomial cpr;
		private int workers;
		private int tracks;
		private static final double CONVERGENCE_THRESHOLD = 0.001;
		private static final double ROOT_THRESHOLD = 0.002;
			
		public MojProducer(List<Complex> args, int workers, int tracks) {
			Complex[] roots = new Complex[args.size()];
			for(int i = 0; i < args.size(); i++) {
				roots[i] = args.get(i);
			}
			cpr = new ComplexRootedPolynomial(new Complex(1,0), roots);
			System.out.println(cpr.toString());
			this.polynomial = cpr.toComplexPolynom();
			System.out.println(polynomial.toString());
			this.workers = workers;
			this.tracks = tracks;
		}
			
		@Override
		public void produce(double reMin, double reMax, double imMin, double imMax,
				int width, int height, long requestNo, IFractalResultObserver observer, AtomicBoolean cancel) {
				
			System.out.println("Zapocinjem izracun");
			
			int maxIter = 16*16;
			int offset = 0;
			short[] data = new short[width*height];
			int workers = this.workers;
			int tracks = this.tracks;
			
			if(this.workers == -1) {
				workers = Runtime.getRuntime().availableProcessors();
			}
			
			if(this.tracks == -1) {
				tracks = 4 * Runtime.getRuntime().availableProcessors();
			}else if(this.tracks > height) {
				tracks = height;
			}
			
			System.out.println("workers: " + workers + " tracks: " + tracks);
			
			int numberYPerTrack = height/tracks;
			
			final BlockingQueue<PosaoIzracuna> queue = new LinkedBlockingQueue<>();

			Thread[] radnici = new Thread[workers];
			for(int i = 0; i < radnici.length; i++) {
				radnici[i] = new Thread(new Runnable() {
					@Override
					public void run() {
						while(true) {
							PosaoIzracuna p = null;
							try {
								p = queue.take();
								if(p==PosaoIzracuna.NO_JOB) break;
							} catch (InterruptedException e) {
								continue;
							}
							p.run();
						}
					}
				});
			}
			for(int i = 0; i < radnici.length; i++) {
				radnici[i].start();
			}
			
			for(int i = 0; i < tracks; i++) {
				int yMin = i*numberYPerTrack;
				int yMax = (i+1)*numberYPerTrack-1;
				if(i==tracks-1) {
					yMax = height-1;
				}
				PosaoIzracuna posao = new PosaoIzracuna(reMin, reMax, imMin, imMax, width, height, yMin, yMax, maxIter, data, cancel, polynomial, cpr, CONVERGENCE_THRESHOLD,
						ROOT_THRESHOLD, offset, maxIter);
				while(true) {
					try {
						queue.put(posao);
						break;
					} catch (InterruptedException e) {
					}
				}
			}
			for(int i = 0; i < radnici.length; i++) {
				while(true) {
					try {
						queue.put(PosaoIzracuna.NO_JOB);
						break;
					} catch (InterruptedException e) {
					}
				}
			}
			
			for(int i = 0; i < radnici.length; i++) {
				while(true) {
					try {
						radnici[i].join();
						break;
					} catch (InterruptedException e) {
					}
				}
			}
			
			System.out.println("Racunanje gotovo. Idem obavijestiti promatraca tj. GUI!");
			observer.acceptResult(data, (short)(polynomial.order() + 1), requestNo);
		}
	}
	
	public static void main(String[] args) {
		
		if(args.length > 2) {
			throw new IllegalArgumentException("Too many arguments");
		}
		
		int workers = -1;
		int tracks = -1;
		
		if(args.length == 2) {
			for(String s : args) {
				String[] temp = s.split("=");
				if(temp[0].equals("--workers") || temp[0].equals("-w")) {
					workers = Integer.parseInt(temp[1]);
				}else if(temp[0].equals("--tracks") || temp[0].equals("-t")) {
					tracks = Integer.parseInt(temp[1]);
				}else {
					throw new IllegalArgumentException();
				}
			}
		}else if(args.length == 1) {
			String[] temp = args[0].split("=");
			if(temp[0].equals("--workers") || temp[0].equals("-w")) {
				workers = Integer.parseInt(temp[1]);
			}else if(temp[0].equals("--tracks") || temp[0].equals("-t")) {
				tracks = Integer.parseInt(temp[1]);
			}else {
				throw new IllegalArgumentException();
			}
		}
		
		System.out.println("Welcome to Newton-Raphson iteration-based fractal viewer.");
		System.out.println("Please enter at least two roots, one root per line. Enter 'done' when done.");
		
		int rootCounter = 1;
		List<Complex> roots = new ArrayList<>();
		String line = null;
		
		try(Scanner sc = new Scanner(System.in)){
			
			do {
				System.out.print("Root " + rootCounter + "> ");
				line = sc.nextLine();
				if(!line.equals("done")) {
					
					try {
						roots.add(Complex.parse(line));
					}catch(IllegalArgumentException e) {
						System.out.println("Unable to parse complex number. Try again");
						continue;
					}
					
					rootCounter++;
				}
			}while(!line.equals("done"));
			
			System.out.println("Image of fractal will apear shortly. Thank you");
			
		}
		
		FractalViewer.show(new MojProducer(roots, workers, tracks));
		
	}

}
