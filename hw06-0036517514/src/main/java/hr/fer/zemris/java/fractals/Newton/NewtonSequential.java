package hr.fer.zemris.java.fractals.Newton;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

import hr.fer.zemris.java.fractals.viewer.FractalViewer;
import hr.fer.zemris.java.fractals.viewer.IFractalProducer;
import hr.fer.zemris.java.fractals.viewer.IFractalResultObserver;
import hr.fer.zemris.math.*;

public class NewtonSequential {
	
	public static class MojProducer implements IFractalProducer{
		
		private ComplexPolynomial polynomial;
		private ComplexRootedPolynomial cpr;
		private static final double CONVERGENCE_THRESHOLD = 0.001;
		private static final double ROOT_THRESHOLD = 0.002;
		
		public MojProducer(List<Complex> args) {
			Complex[] roots = new Complex[args.size()];
			for(int i = 0; i < args.size(); i++) {
				roots[i] = args.get(i);
			}
			cpr = new ComplexRootedPolynomial(new Complex(1,0), roots);
			System.out.println(cpr.toString());
			this.polynomial = cpr.toComplexPolynom();
			System.out.println(polynomial.toString());
		}
		
		@Override
		public void produce(double reMin, double reMax, double imMin, double imMax,
				int width, int height, long requestNo, IFractalResultObserver observer, AtomicBoolean cancel) {
			
			System.out.println("Zapocinjem izracun");
			
			int maxIter = 16*16;
			int offset = 0;
			short[] data = new short[width*height];
			for(int y = 0; y < height; y++) {
				if(cancel.get()) break;
				for(int x = 0; x < width; x++) {
					double cre = x / (width-1.0) * (reMax - reMin) + reMin;
					double cim = (height-1.0-y) / (height-1) * (imMax - imMin) + imMin;
					Complex zn = new Complex(cre, cim);
					
					int iters = 0;
					double module = 0;
					do {
						//System.out.println("dogadja se " + iters + ". iter");
						Complex numerator = polynomial.apply(zn);
						ComplexPolynomial derived = polynomial.derive();
						Complex denominator = derived.apply(zn);
						Complex znOld = zn.copy();
						Complex fraction = numerator.divide(denominator);
						zn = zn.sub(fraction);
						module = zn.sub(znOld).module();
						iters++;
					}while(module > CONVERGENCE_THRESHOLD && iters < maxIter);
					int index = cpr.indexOfClosestRootFor(zn, ROOT_THRESHOLD);
					//System.out.println(index);
					data[offset++] = (short) (index + 1);
				}
			}
			
			System.out.println("Racunanje gotovo. Idem obavijestiti promatraca tj. GUI!");
			observer.acceptResult(data, (short)(polynomial.order() + 1), requestNo);
		}
	}

	public static void main(String[] args) {
		
		System.out.println("Welcome to Newton-Raphson iteration-based fractal viewer.");
		System.out.println("Please enter at least two roots, one root per line. Enter 'done' when done.");
		
		int rootCounter = 1;
		List<Complex> roots = new ArrayList<>();
		String line = null;
		
		try(Scanner sc = new Scanner(System.in)){
			
			do {
				System.out.print("Root " + rootCounter + "> ");
				line = sc.nextLine();
				if(!line.equals("done")) {
					
					try {
						roots.add(Complex.parse(line));
					}catch(IllegalArgumentException e) {
						System.out.println("Unable to parse complex number. Try again");
						continue;
					}
					
					rootCounter++;
				}
			}while(!line.equals("done"));
			
			System.out.println("Image of fractal will apear shortly. Thank you");
			
		}
		
		FractalViewer.show(new MojProducer(roots));

	}

}
