package hr.fer.oprpp1.custom.collections;

public class Dictionary<K,V> {
	
	private static class Pair<K,V>{
		private K key;
		private V value;
		
		private Pair(K key, V value) {
			if(key == null) throw new NullPointerException("Key must not be null");
			this.key = key;
			this.value = value;
		}
		
		/**Getter for pairs key.
		 * @return key of type K.
		 */
		private K getKey() {
			return this.key;
		}
		
		/**Getter for pairs value.
		 * @return value of type V.
		 */
		private V getValue() {
			return this.value;
		}
		
		/**Setter for value of pair.
		 * @param value of type V.
		 */
		private void setValue(V value) {
			this.value = value;
		}
		
		
	}
	
	private ArrayIndexedCollection<Pair<K,V>> collection;
	
	public Dictionary() {
		this.collection = new ArrayIndexedCollection<>();
	}
	
	/**Method returns true if the dictionary is empty, false otherwise.
	 * @return boolean true if the dictionary is empty, false otherwise.
	 */
	public boolean isEmpty() {
		if(this.collection.isEmpty()) return true;
		return false;
	}
	
	/**Method returns number of Key,Value pairs in dictionary.
	 * @return size
	 */
	public int size() {
		return this.collection.size();
	}
	
	/**Method clears the dictionary.
	 * 
	 */
	public void clear() {
		this.collection.clear();
	}
	
	/**Method puts the Key,Value pair into the dictionary. If the key already exists, the method overrides the old
	 * value with new value. If the pair does not exist in dictionary then it makes new pair in it.
	 * @param key
	 * @param value
	 * @return oldValue if the key already exists in dictionary, null if not.
	 * @throws NullPointerException
	 */
	public V put(K key, V value) {
		if(key == null) throw new NullPointerException("Key must not be null");
		
		V oldValue = null;
		for(int i = 0; i < this.collection.size(); i++) {
			if(key.equals(this.collection.get(i).getKey())) {
				oldValue = this.collection.get(i).getValue();
				this.collection.get(i).setValue(value);
				return oldValue;
			}
		}
		
		this.collection.add(new Pair<K,V>(key,value));
		return null;
	}
	
	/**Method returns the value of the specified key. If a key does not exist in the dictionary, null is returned.
	 * @param key
	 * @return value under a specified key, null if there is no such key.
	 * @throws NullPointerException
	 */
	public V get(Object key) {
		if(key == null) throw new NullPointerException("Key must not be null");
		
		for(int i = 0; i < this.collection.size(); i++) {
			if(key.equals(this.collection.get(i).getKey())) {
				V value = this.collection.get(i).getValue();
				return value;
			}
		}
		return null;
	}
	
	/**Method removes key,value pair
	 * @param key
	 * @return value that was under a specified key before removal if such pair exists in dictionary, null otherwise.
	 * @throws NullPointerException
	 */
	public V remove(K key) {
		if(key == null) throw new NullPointerException("Key must not be null");
		
		for(int i = 0; i < this.collection.size(); i++) {
			if(key.equals(this.collection.get(i).getKey())) {
				V value = this.collection.get(i).getValue();
				this.collection.remove(i);
				return value;
			}
		}
		return null;
	}

}
