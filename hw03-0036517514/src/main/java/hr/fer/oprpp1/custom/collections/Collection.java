package hr.fer.oprpp1.custom.collections;

public interface Collection<T> {
	
	/**
	 * Returns true if collection contains no objects and false otherwise.
	 */
	public default boolean isEmpty() {
		return this.size() == 0;
	}
	
	/**Returns the number of currently stored objects in collections.
	 * 
	 */
	public int size();
	
	/**Adds the given object into this collection.
	 * @param value that is being added to this collection.
	 */
	public void add(T value);
	
	/**Returns true only if the collection contains given value, as determined by equals method.
	 * @param value that is being examined if the collection contains it
	 * 
	 */
	public boolean contains (Object value);
	
	/**Returns true only if collection contains given value as determined by equals method 
	 * and removes one occurrence of it.
	 * @param value that is being removed if the collection contains it
	 * 
	 */
	public boolean remove(Object value);
	
	/**Allocates new array with size equals to the size of this collections, fills it with
	 * collection content and returns the array.
	 * @return Object[] array filled with collection content
	 */
	public Object[] toArray() ;
	
	/**Method calls processor.process(.) for each element of this collection. 
	 * The order in which elements will be sent is undefined in this class. 
	 * @param processor that is being called for each element of this collection.
	 */
	public default void forEach(Processor<? super T> processor) {
		ElementsGetter<? extends T> getter = this.createElementsGetter();
		while(getter.hasNext()) {
			processor.process(getter.getNextElement());
		}
	}
	
	/**Method adds into the current collection all elements from the given collection.
	 * This other collection remains unchanged.
	 * @param other collection that is added onto this collection
	 */
	public default void addAll(Collection<? extends T> other) {
		
		class LocalProcessor implements Processor<T>{
			
			public LocalProcessor() {
			}
			
			public void process(T value) {
				add(value);
			}
		}
		
		LocalProcessor processor = new LocalProcessor();
		other.forEach(processor);
	}
	
	/**Removes all elements from the collection.
	 * 
	 */
	public void clear();
	
	/**Method that creates ElementsGetter and returns new ElementsGeter
	 * @return new ElementsGetter
	 */
	public ElementsGetter<T> createElementsGetter();
	
	/**Method goes through the elements in col and adds the elements that pass the test to this collection.
	 * @param col that is being tested
	 * @param test that is being run for every element of collection col
	 */
	public void addAllSatisfying(Collection<? extends T> col, Tester<? super T> tester);
	
}
