package hr.fer.oprpp1.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

public class ArrayIndexedCollection<T> implements List<T>{
	
	private int size;
	private T[] elements;
	private long modificationCount;
	
	@SuppressWarnings("unchecked")
	public ArrayIndexedCollection(Collection<T> collection, int initialCapacity) {
		if(collection == null) throw new NullPointerException("Collection must not be null");
		
		if(collection.size() > initialCapacity) {
			//this.size = collection.size();
			this.elements = (T[])new Object[collection.size()];
		}else {
			//this.size = collection.size();
			this.elements = (T[])new Object[initialCapacity];
		}
		this.size = 0;
		this.modificationCount = 0;
		this.addAll(collection);
	}
	
	public ArrayIndexedCollection(Collection<T> collection) {
		this(collection, 16);
	}
	
	@SuppressWarnings("unchecked")
	public ArrayIndexedCollection(int initialCapacity) {
		if(initialCapacity < 1) {
			throw new IllegalArgumentException("Initial capacity must be larger than 1. Now it it is: " + initialCapacity);
		}else {
			this.size = 0;
			this.elements = (T[])new Object[initialCapacity];
			this.modificationCount = 0;
		}
	}
	
	public ArrayIndexedCollection() {
		this(16);
	}
	
	public static class ArrayIndexedGetter<T> implements ElementsGetter<T>{
		
		private int currentIndex;
		private ArrayIndexedCollection<T> collection;
		private long savedModificationCount;
		
		
		public ArrayIndexedGetter(ArrayIndexedCollection<T> collection) {
			this.currentIndex = 0;
			this.collection = collection;
			this.savedModificationCount = collection.modificationCount;
		}
		
		/**Method that checks whether there is another element in the collection
		 *@return true if the collection has next element, false otherwise
		 *@throws ConcurrentModificationException
		 */
		public boolean hasNext() {
			if(this.savedModificationCount != collection.modificationCount) throw new ConcurrentModificationException();
			
			if(currentIndex >= collection.elements.length) return false;
			return true;
		}
		
		/**Method that returns the next element in collection.
		 *@return next Object in collection
		 *@throws ConcurrentModificationException
		 */
		public T getNextElement() {
			if(savedModificationCount != collection.modificationCount) throw new ConcurrentModificationException();
			
			if(hasNext()) {	
				T obj = collection.elements[currentIndex];
				currentIndex++;
				return obj;
			}
			throw new NoSuchElementException();
		}
		
	}
	
	/**Method goes through the elements in col and adds the elements that pass the test to this collection.
	 * @param col that is being tested
	 * @param test that is being run for every element of collection col
	 */
	@Override
	public void addAllSatisfying(Collection<? extends T> col, Tester<? super T> tester) {
		ElementsGetter<? extends T> getter = col.createElementsGetter();
		while(getter.hasNext()) {
			T obj = getter.getNextElement();
			if(tester.test(obj)) {
				this.add(obj);
			}
		}
	}
	
	/**Method that creates ElementsGetter and returns new ElementsGeter
	 * @return new ElementsGetter
	 */
	@Override
	public ElementsGetter<T> createElementsGetter() {
		return new ArrayIndexedGetter<T>(this);
	}
	
	
	/**Adds the given object into this collection. Reference is added into first empty place
	 * in the elements array; if the elements array is full, it should be reallocated by 
	 * doubling its size.
	 * 
	 * @throws NullPointerException if you try to add null as element
	 * @param value that is being added
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void add(T value) {
		if(value == null) throw new NullPointerException("Value cannot be null");
		
		if(size < this.elements.length) {
			this.elements[size] = value;
			this.size++;
		}else {
			int newSize = this.elements.length * 2;
			T[] newElements = (T[])new Object[newSize];
			for(int i = 0; i < this.elements.length; i++) {
				newElements[i] = this.elements[i];
			}
			
			this.elements = newElements;
			this.elements[size] = value;
			this.size++;
			this.modificationCount++;
		}
	}
	
	/**Returns the object that is stored in backing array at position index.
	 * Valid indexes are 0 to size-1.
	 * 
	 * @param index of the searched object
	 * @return object that is under the index we are looking for
	 * @throws IndexOutOfBoundsException if the index we are searching for is out of bounds
	 */
	@Override
	public T get(int index) {
		if(index < 0 || index > (this.size - 1)) 
			throw new IndexOutOfBoundsException("Index you are searching for must be in range of 0 to " + (this.size - 1) + ".");
		
		return this.elements[index];
	}
	
	/**Removes all elements from the collection. The allocated array is left at current 
	 * capacity. 
	 *
	 */
	@Override
	public void clear() {
		for(int i = 0; i < this.size; i++) {
			this.elements[i] = null;
		}
		this.size = 0;
		this.modificationCount++;
	}
	
	/**Inserts the given value at the given position in array. The method does not overwrite
	 * current Object but instead shifts all the other Objects one place towards the end.
	 * Legal positions are 0 to size.
	 * @param value that is being inserted
	 * @param position at which the value is being inserted at
	 * @throws IndexOutOfBoundsException
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void insert(T value, int position) {
		if(position < 0 || position > this.size)
			throw new IndexOutOfBoundsException("Position is out of bounds");
		
		//check if the last index of the elements array is null so that other Objects can be shifted -> if not -> make array bigger

		if(this.size == this.elements.length) {
			int newSize = this.elements.length * 2;
			T[] newElements = (T[])new Object[newSize];
			for(int i = 0; i < this.elements.length; i++) {
				newElements[i] = this.elements[i];
			}
			
			this.elements = newElements;
		}
		
		for(int i = this.size - 1; i >= position; i--) {
			this.elements[i + 1] = this.elements[i];
		}
		
		this.elements[position] = value;
		this.size++;
		this.modificationCount++;
		
	}
	
	/**Searches the collection and returns the index of the first occurrence of the given
	 * value or -1 if the value is not found. 
	 * @param value that is being searched for. Can be null.
	 * @return index of the searched value if its found or -1 if its not found.
	 */
	@Override
	public int indexOf(Object value) {
		if(value == null) return -1;
		
		for(int i = 0; i < this.size; i++) {
			if(value.equals(this.elements[i])) 
				return i;
		}
		
		return -1;
	}
	
	/**Removes element at specified index from collection. Other elements after the removed
	 * element are shifted to the left.
	 * @param index of the element that is to be removed. Legal indexes are 0 to size-1
	 * @throws IndexOutOfBoundsException
	 */
	@Override
	public void remove(int index) {
		if(index < 0 || index > this.size - 1) 
			throw new IndexOutOfBoundsException("Index is out of bounds, must be within 0 to " + (this.size-1) + ".");
		
		for(int i = index; i < this.size - 1; i++) {
			this.elements[i] = this.elements[i + 1];
		}
		
		this.elements[this.size - 1] = null;
		this.size--;
		this.modificationCount++;
	}
	
	/**Returns the number of currently stored objects in this collection.
	 *
	 */
	@Override
	public int size() {
		return this.size;
	}
	
	/**Returns true if collection contains no objects and false otherwise.
	 *
	 */
	@Override
	public boolean isEmpty() {
		if(this.size() == 0) return true;
		return false;
	}
	
	/**Returns true only if the collection contains given value, as determined by equals method.
	 * @param value that is being examined if the collection contains it
	 * @return true if the collection contains the value, false if not
	 */
	@Override
	public boolean contains(Object value) {
		for(int i = 0; i < this.size; i++) {
			if(value.equals(this.elements[i])) 
				return true;
		}
		
		return false;
	}
	
	/**Returns true only if collection contains given value as determined by equals method 
	 * and removes one occurrence of it.
	 * @param value that is being removed if the collection contains it
	 * @return true if the value has been removed, false if not
	 */
	@Override
	public boolean remove(Object value) {
		boolean removed = false;
		for(int i = 0; i < this.size; i++) {
			if(value.equals(this.elements[i])) {
				this.elements[i] = null;
				for(int j = i; j < this.size - 1; j++) {
					this.elements[j] = this.elements[j + 1];
				}
				removed = true;
				this.size--;
				this.modificationCount++;
				break;
			}
		}
		
		return removed;
	}
	
	/**Allocates new array with size equals to the size of this collections, fills it with
	 * collection content and returns the array.
	 * @return Object[] array filled with collection content
	 */
	@Override
	public Object[] toArray() {
		Object[] array = new Object[this.size];
		for(int i = 0; i < this.size; i++) {
			array[i] = this.elements[i];
		}
		
		return array;
	}
	
	/**Method adds into the current collection all elements from the given collection.
	 * This other collection remains unchanged.
	 * @param other collection that is added onto this collection
	 */
	@Override
	public void addAll(Collection<? extends T> other) {
		
		class LocalProcessor implements Processor<T>{
			
			public LocalProcessor() {
			}
			
			public void process(T value) {
				add(value);
			}
		}
		
		LocalProcessor processor = new LocalProcessor();
		other.forEach(processor);
	}
	
	/**Prints the content of the collection.
	 * 
	 */
	public void print() {
		for(int i = 0; i < this.size; i++) {
			System.out.println(this.elements[i]);
		}
	}
	
}
