package hr.fer.oprpp1.math;

public class Demo {
	
	public static void main(String[] args) {
		Vector2D v1 = new Vector2D(1.5, 2.5);
		double angle = 15.0;
		v1.rotate(angle);
		System.out.println(v1.getX() + " " + v1.getY());
		
		int powerOfTwo = 1;
		for(int i = 0; powerOfTwo < 1; i++) {
			powerOfTwo = (int)Math.pow(2.0, i);
		}
		System.out.println(powerOfTwo);
	}

}
