package hr.fer.oprpp1.custom.scripting.nodes;

import hr.fer.oprpp1.custom.scripting.elems.Element;

public class EchoNode extends Node{
	
	private Element[] elements;
	
	public EchoNode(Element[] ele) {
		int counter = 0;
		for(int i = 0; i < ele.length; i++) {
			if(ele[i] != null) {
				counter++;
			}
		}
		this.elements = new Element[counter];
		for(int i = 0; i < counter; i++) {
			this.elements[i] = ele[i];
		}
	}
	
	@Override
	public String toString() {
		String str = "{$= ";
		for(int i = 0; i < this.elements.length; i++) {
			str += this.elements[i].asText() + " ";
		}
		
		str += "$}";
		
		return str;
	}
	
}
