package hr.fer.oprpp1.custom.scripting.parser;

import hr.fer.oprpp1.custom.collections.ObjectStack;
import hr.fer.oprpp1.custom.scripting.elems.Element;
import hr.fer.oprpp1.custom.scripting.elems.ElementConstantDouble;
import hr.fer.oprpp1.custom.scripting.elems.ElementConstantInteger;
import hr.fer.oprpp1.custom.scripting.elems.ElementFunction;
import hr.fer.oprpp1.custom.scripting.elems.ElementOperator;
import hr.fer.oprpp1.custom.scripting.elems.ElementString;
import hr.fer.oprpp1.custom.scripting.elems.ElementVariable;
import hr.fer.oprpp1.custom.scripting.lexer.Lexer;
import hr.fer.oprpp1.custom.scripting.lexer.Token;
import hr.fer.oprpp1.custom.scripting.lexer.TokenType;
import hr.fer.oprpp1.custom.scripting.nodes.DocumentNode;
import hr.fer.oprpp1.custom.scripting.nodes.EchoNode;
import hr.fer.oprpp1.custom.scripting.nodes.ForLoopNode;
import hr.fer.oprpp1.custom.scripting.nodes.TextNode;


public class SmartScriptParser {
	
	private Lexer lexer;
	private ObjectStack objStack;
	private DocumentNode docNode;
	
	public SmartScriptParser(String documentBody) {
		this.lexer = new Lexer(documentBody);
		this.objStack = new ObjectStack();
		this.docNode = new DocumentNode();
		this.parse();
	}
	
	/**Helper method for determining which type is token 
	 * @param token
	 * @return String that is saying which type is token
	 */
	private static String checkType(Token token) {
		if(token.getType() == TokenType.VARIABLE) {
			return "var";
		}else if(token.getType() == TokenType.NUMBER) {
			String number = token.getValue().toString();
			if(number.contains(".")) {
				return "double";
			}else {
				return "integer";
			}
		}else if(token.getType() == TokenType.STRING){
			return "string";
		}else if(token.getType() == TokenType.FUNCTION) {
			return "function";
		}else if(token.getType() == TokenType.OPERATOR) {
			return "operator";
		}else {
			throw new SmartScriptParserException("Bad TokenType");
		}
	}
	
	/**Method parses the input string that the class recieves. First the string goes to the lexer which generates tokens. 
	 * Based on the tokens that the lexer produces, parser creates corresponding document model.
	 */
	private void parse() {
		
		Element[] elemArray = new Element[30];
		int eleIndex = 0;
		this.objStack.push(this.docNode);
		
		Token token = lexer.nextToken();
		int state = 0; //0 - documentNode, 1 - ForLoopNode
		while(token.getType() != TokenType.EOF) {
			
			if(token.getType() == TokenType.TEXT) {
				TextNode textNode = new TextNode((String)token.getValue());
				if(state == 0) {
					this.docNode = (DocumentNode)this.objStack.pop();
					this.docNode.addChildNode(textNode);
					this.objStack.push(this.docNode);
				}else {
					ForLoopNode node = (ForLoopNode) this.objStack.pop();
					node.addChildNode(textNode);
					this.objStack.push(node);
				}
				
				
				token = lexer.nextToken();
				continue;
			}
			
			if(token.getType() == TokenType.FOR) {
				state = 1;
				token = lexer.nextToken();
				ElementVariable var = null;
				Element startExpr = null;
				Element endExpr = null;
				Element stepExpr = null;
				int counter = 0;
				while(token.getType() != TokenType.CLOSE_TAG) {
					if(counter == 0) {
						if(token.getType() != TokenType.VARIABLE) throw new SmartScriptParserException("First element in for tag is not variable");
						var = new ElementVariable((String)token.getValue());
						counter++;
						token = lexer.nextToken();
						continue;
					}else if(counter == 1) {
						String type = checkType(token);
						if(type.equals("var")) {
							startExpr = new ElementVariable((String)token.getValue());
						}else if(type.equals("integer")) {
							startExpr = new ElementConstantInteger((int)token.getValue());
						}else if(type.equals("double")) {
							startExpr = new ElementConstantDouble((double)token.getValue());
						}else if(type.equals("string")) {
							startExpr = new ElementString((String)token.getValue());
						}else {
							throw new SmartScriptParserException("For loop needs to have either var, num or string.");
						}
						counter++;
						token = lexer.nextToken();
						continue;
					}else if(counter == 2) {
						String type = checkType(token);
						if(type.equals("var")) {
							stepExpr = new ElementVariable((String)token.getValue());
						}else if(type.equals("integer")) {
							stepExpr = new ElementConstantInteger((int)token.getValue());
						}else if(type.equals("double")) {
							stepExpr = new ElementConstantDouble((double)token.getValue());
						}else if(type.equals("string")) {
							stepExpr = new ElementString((String)token.getValue());
						}else {
							throw new SmartScriptParserException("For loop needs to have either var, num or string.");
						}
						counter++;
						token = lexer.nextToken();
						continue;
					}else if(counter == 3) {
						String type = checkType(token);
						if(type.equals("var")) {
							endExpr = new ElementVariable((String)token.getValue());
						}else if(type.equals("integer")) {
							endExpr = new ElementConstantInteger((int)token.getValue());
						}else if(type.equals("double")) {
							endExpr = new ElementConstantDouble((double)token.getValue());
						}else if(type.equals("string")) {
							endExpr = new ElementString((String)token.getValue());
						}else {
							throw new SmartScriptParserException("For loop needs to have either var, num or string.");
						}
						counter++;
						token = lexer.nextToken();
						continue;
					}
				}
				//checking if there is a step expression or not
				if(counter == 4) {
					ForLoopNode forNode = new ForLoopNode(var, startExpr, stepExpr, endExpr);
					this.objStack.push(forNode);
				}else {
					ForLoopNode forNode = new ForLoopNode(var, startExpr, null, stepExpr);
					this.objStack.push(forNode);
				}
				
				state = 1;
				token = lexer.nextToken();
				continue;
			}
			
			if(token.getType() == TokenType.END) {
				if(state != 1) throw new SmartScriptParserException("End token without corresponding for token");
				
				ForLoopNode forNode = (ForLoopNode) this.objStack.pop();
				this.docNode = (DocumentNode) this.objStack.pop();
				this.docNode.addChildNode(forNode);
				this.objStack.push(this.docNode);
				state = 0;
				token = lexer.nextToken();
				continue;
			}
			
			if(token.getType() == TokenType.EQUALS) {
				if(state != 1) throw new SmartScriptParserException("Equals token must be in for loop");
				
				token = lexer.nextToken();
				while(token.getType() != TokenType.CLOSE_TAG) {
					switch (checkType(token)) {
					case "integer":
						ElementConstantInteger elemInt = new ElementConstantInteger((int)token.getValue());
						elemArray[eleIndex++] = elemInt;
						break;
					case "double":
						ElementConstantDouble elemDouble = new ElementConstantDouble((double)token.getValue());
						elemArray[eleIndex++] = elemDouble;
						break;
					case "function":
						ElementFunction elemFunc = new ElementFunction((String)token.getValue());
						elemArray[eleIndex++] = elemFunc;
						break;
					case "operator":
						ElementOperator elemOp = new ElementOperator((String)token.getValue());
						elemArray[eleIndex++] = elemOp;
						break;
					case "string":
						ElementString elemStr = new ElementString((String)token.getValue());
						elemArray[eleIndex++] = elemStr;
						break;
					case "var":
						ElementVariable elemVar = new ElementVariable((String)token.getValue());
						elemArray[eleIndex++] = elemVar;
						break;
					default:
						throw new SmartScriptParserException("Unexpected token type in echo node.");
					}
					token = lexer.nextToken();
				}
				
				EchoNode echoNode = new EchoNode(elemArray);
				ForLoopNode forNode = (ForLoopNode)this.objStack.pop();
				forNode.addChildNode(echoNode);
				this.objStack.push(forNode);
				
				elemArray = new Element[30];
				eleIndex = 0;
			}
			
			token = lexer.nextToken();
			
		}
		
	}
	
	/**Getter method for returning DocumentNode.
	 * @return DocumentNode
	 */
	public DocumentNode getDocument() {
		return this.docNode;
	}
	
}
