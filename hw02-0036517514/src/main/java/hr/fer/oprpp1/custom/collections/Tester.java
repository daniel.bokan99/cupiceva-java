package hr.fer.oprpp1.custom.collections;

public interface Tester {

	/**Method receives an object and tests if its valid. 
	 * @param obj that is to be tested
	 * @return true if the object is valid, false otherwise
	 */
	public boolean test(Object obj);
	
}
