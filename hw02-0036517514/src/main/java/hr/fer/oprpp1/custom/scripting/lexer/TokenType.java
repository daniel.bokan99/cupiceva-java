package hr.fer.oprpp1.custom.scripting.lexer;

public enum TokenType {
	FOR, END, EQUALS, TEXT, CLOSE_TAG, OPEN_TAG, FUNCTION, OPERATOR, SYMBOL, NUMBER, EOF, VARIABLE, STRING
}
