package hr.fer.oprpp1.custom.scripting.elems;

public class ElementString extends Element{
	
	private String value;
	
	public ElementString(String value) {
		this.value = value;
	}
	
	
	/**Method returns ElementString value as String
	 *@return new String 
	 */
	@Override
	public String asText() {
		return this.value;
	}
}
