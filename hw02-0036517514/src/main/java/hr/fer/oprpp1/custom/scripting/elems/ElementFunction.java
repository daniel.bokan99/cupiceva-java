package hr.fer.oprpp1.custom.scripting.elems;

public class ElementFunction extends Element{
	
	public String name;
	
	public ElementFunction(String name) {
		this.name = name;
	}
	
	/**Method returns the name parameter of this ElementFunction.
	 *@return name 
	 */
	@Override
	public String asText() {
		return this.name;
	}

}
