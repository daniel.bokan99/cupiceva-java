package hr.fer.oprpp1.custom.scripting.nodes;

import hr.fer.oprpp1.custom.collections.ArrayIndexedCollection;

public class Node {
	
	private ArrayIndexedCollection collection;
	
	/**Method adds child node to the current node.
	 * @param child that is to be added.
	 */
	public void addChildNode(Node child) {
		if(collection == null) {
			collection = new ArrayIndexedCollection();
		}
		collection.add(child);
	}
	
	/**Method returns the number of children in this node.
	 * @return int number of children
	 */
	public int numberOfChildren() {
		return collection.size();
	}
	
	/**Method returns node that is at the specified index in current node.
	 * @param index 
	 * @return node that is at the specified index in the node.
	 */
	public Node getChild(int index) {
		return (Node) collection.get(index);
	}
	
	
	
}
