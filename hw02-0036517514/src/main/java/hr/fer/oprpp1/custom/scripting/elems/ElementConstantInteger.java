package hr.fer.oprpp1.custom.scripting.elems;

public class ElementConstantInteger extends Element {
	
	public int value;
	
	public ElementConstantInteger(int value) {
		this.value = value;
	}
	
	/**Method returns the value of this ElementConstantInteger
	 *@return value of the ElementConstantInteger as String
	 */
	@Override
	public String asText() {
		return Integer.toString(this.value);
	}
	
}
