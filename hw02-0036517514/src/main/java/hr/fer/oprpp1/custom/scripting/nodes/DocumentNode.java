package hr.fer.oprpp1.custom.scripting.nodes;

import hr.fer.oprpp1.custom.collections.ArrayIndexedCollection;

public class DocumentNode extends Node{
	
	private ArrayIndexedCollection collection;
	
	@Override
	public String toString() {
		String str = "";
		int numberOfChildren = this.numberOfChildren();
		for(int i = 0; i < numberOfChildren; i++) {
			str += this.getChild(i).toString() + " ";
		}
		
		return str;
	}
	
	@Override
	public boolean equals(Object o) {
		if(!(o instanceof DocumentNode)) return false;
		DocumentNode otherNode = (DocumentNode) o;
		String string1 = this.toString().replaceAll(" ", "");
		String string2 = otherNode.toString().replaceAll(" ", "");
		if(string1.equals(string2)) return true;
		return false;
	}
	
}
