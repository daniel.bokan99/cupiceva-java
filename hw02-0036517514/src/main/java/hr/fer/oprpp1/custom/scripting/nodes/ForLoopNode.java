package hr.fer.oprpp1.custom.scripting.nodes;

import hr.fer.oprpp1.custom.scripting.elems.Element;
import hr.fer.oprpp1.custom.scripting.elems.ElementVariable;

public class ForLoopNode extends Node{

	private ElementVariable variable;
	private Element startExpression;
	private Element endExpression;
	private Element stepExpression;
	
	public ForLoopNode(ElementVariable variable, Element startExpression, Element stepExpression, Element endExpression) {
		this.variable = variable;
		this.startExpression = startExpression;
		this.endExpression = endExpression;
		this.stepExpression = stepExpression;
	}
	
	@Override
	public String toString() {
		int numberOfChildren = this.numberOfChildren();
		String str = "{$ FOR " + this.variable.asText() + " " + this.startExpression.asText() + " ";
		if(stepExpression != null) {
			str += stepExpression.asText() + " ";
		}
		str += endExpression.asText() + "$}";
		for(int i = 0; i < numberOfChildren; i++) {
			str += this.getChild(i).toString() + " ";
		}
		
		return str + " {$END$}";
	}
	
	
}
