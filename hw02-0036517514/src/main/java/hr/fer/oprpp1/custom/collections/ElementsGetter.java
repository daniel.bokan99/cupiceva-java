package hr.fer.oprpp1.custom.collections;

public interface ElementsGetter {
	
	/**Method that checks whether there is another element in the collection
	 *@return true if the collection has next element, false otherwise
	 */
	public boolean hasNext();
	
	/**Method that returns the next element in collection.
	 *@return next Object in collection
	 */
	public Object getNextElement();
	
	
	/**Method calls the processor p for all the other remaining elements of the collection.
	 * @param processor p that is being called
	 */
	public default void processRemaining(Processor p) {
		while(hasNext()) {
			p.process(getNextElement());
		}
	}
}
