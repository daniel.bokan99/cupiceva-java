package hr.fer.oprpp1.custom.scripting.elems;

public class ElementConstantDouble extends Element {

	private double value;
	
	public ElementConstantDouble(double value) {
		this.value = value;
	}
	
	/**Method returns the value of this ElementConstantDouble
	 *@return value as string of ElementConstantDouble
	 */
	@Override
	public String asText() {
		return Double.toString(this.value);
	}
}
