package hr.fer.oprpp1.custom.scripting.elems;

public class ElementOperator extends Element{
	
	public String symbol;
	
	public ElementOperator(String symbol) {
		this.symbol = symbol;
	}
	
	/**Method returns the symbol as string of this ElementOperator
	 *@return symbol
	 */
	@Override
	public String asText() {
		return this.symbol;
	}

}
