package hr.fer.oprpp1.custom.scripting.elems;

public class ElementVariable extends Element {
	
	private String name;
	
	public ElementVariable(String name) {
		this.name = name;
	}
	
	/**Method returns the name of this ElementVariable
	 * @return name of ElementVariable
	 */
	@Override
	public String asText() {
		return this.name;
	}
	
	
	
}
