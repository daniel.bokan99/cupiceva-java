package hr.fer.oprpp1.custom.scripting.lexer;

import hr.fer.oprpp1.custom.scripting.parser.SmartScriptParser;

public class Lexer {
	
	private char[] data;
	private int currentIndex;
	private Token token;
	private LexerState state;
	
	public Lexer(String text) {
		this.data = text.toCharArray();
		this.state = LexerState.OUT_TAG;
		this.currentIndex = 0;
	}
	
	/**Method returns next token from this lexer.
	 * @return next token type Token.
	 */
	public Token nextToken() {
		if(data.length == 0) throw new LexerException();
		
		if(this.state == LexerState.OUT_TAG) {
			String text = "";
			boolean string = false;
			for(int i = this.currentIndex; i < data.length; i++) {
				
				if(data[i] == '\\') {
					if(data[i+1] == '\\' || data[i+1] == '{' || string) {
						text += String.valueOf(data[i]) + String.valueOf(data[i + 1]);
						i++;
						continue;
					}else {
						throw new LexerException("Invalid escaping");
					}
				}
				
				if(data[i] == '\"') {
					if(string) {
						string = false;
					}else {
						string = true;
					}
					text += String.valueOf(data[i]);
					continue;
				}
				
				
				if(data[i] != '{') {
					text += String.valueOf(data[i]);
					continue;
				}
				
				if(data[i] == '{') {
					if(i < data.length - 1 && data[i + 1] == '$') {
						if(!(text.equals(""))) {
							this.token = new Token(TokenType.TEXT, text);
							this.currentIndex = i;
							return this.token;
						}
						
						this.token = new Token(TokenType.OPEN_TAG, "{$");
						this.currentIndex = i + 2;
						this.setState(LexerState.IN_TAG);
						return this.token;
					}else {
						text += String.valueOf(data[i]);
						continue;
					}
				}	
				
			}
			
			if(!(text.equals(""))) {
				this.token = new Token(TokenType.TEXT, text);
				this.currentIndex = data.length;
				return this.token;
			}
		}else {
			
			String var = "";
			String number = "";
			String function = "";
			String string = "";
			boolean isDouble = false;
			if(this.getToken().getType() == TokenType.OPEN_TAG) {
				if(Character.isWhitespace(data[this.currentIndex])) {
					this.currentIndex++;
				}
				if(data[this.currentIndex] == '=') {
					this.token = new Token(TokenType.EQUALS, "EQUALS");
					this.currentIndex++ ;
					return this.token;
				}else if(data[this.currentIndex] == 'f' || data[this.currentIndex] == 'F') {
					this.token = new Token(TokenType.FOR, "FOR");
					this.currentIndex += 3;
					return this.token;
				}else if(data[this.currentIndex] == 'e' || data[this.currentIndex] == 'E') {
					this.token = new Token(TokenType.END, "END");
					this.currentIndex += 3;
					return this.token;
				}
			}
			
			for(int i = this.currentIndex; i < data.length; i++) {
				
				if(Character.isWhitespace(data[i])) {
					if(!(var.equals("") && number.equals(""))) {
						if(!(number.equals(""))) {
							this.token = new Token(TokenType.NUMBER, number);
							this.currentIndex = i;
							return this.token;
						}else if(!(var.equals(""))) {
							this.token = new Token(TokenType.VARIABLE, var);
							this.currentIndex = i;
							return this.token;
						}
					}
					continue;
				}
				
				if(Character.isLetter(data[i])) {
					if(!(number.equals(""))) {
						this.token = new Token(TokenType.NUMBER, number);
						this.currentIndex = i;
						return this.token;
					}
					var += String.valueOf(data[i]);
					i++;
					while(Character.isLetter(data[i]) || Character.isDigit(data[i]) || data[i] == '_' ) {
						var += String.valueOf(data[i]);
						i++;
					}
					
					this.token = new Token(TokenType.VARIABLE, var);
					this.currentIndex = i;
					return this.token;
					
				}
				
				if(Character.isDigit(data[i])) {
					if(!(var.equals(""))) {
						this.token = new Token(TokenType.VARIABLE, var);
						this.currentIndex = i;
						return this.token;
					}
					
					if(data[i - 1] == '-') {
						number += String.valueOf(data[i - 1]);
					}
					
					while(Character.isDigit(data[i]) || data[i] == '.' ) {
						if(data[i] == '.') isDouble = true;
						number += String.valueOf(data[i]);
						i++;
					}
					
					if(isDouble) {
						this.token = new Token(TokenType.NUMBER, Double.parseDouble(number));
						this.currentIndex = i;
						return this.token;
					}else {
						this.token = new Token(TokenType.NUMBER, Integer.parseInt(number));
						this.currentIndex = i;
						return this.token;
					}
					
				}
				
				if(data[i] == '@') {
					function += String.valueOf(data[i]);
					i++;
					while(Character.isLetter(data[i]) || Character.isDigit(data[i]) || data[i] == '_') {
						function += String.valueOf(data[i]);
						i++;
					}
					
					this.token = new Token(TokenType.FUNCTION, function);
					this.currentIndex = i;
					return this.token;
					
				}
				
				if(data[i] == '\"') {
					string += String.valueOf(data[i]);
					i++;
					while(!(Character.isWhitespace(data[i]))) {
						string += String.valueOf(data[i]);
						i++;
					}
					
					this.token = new Token(TokenType.STRING, string);
					this.currentIndex = i;
					return this.token;
				}
				
				if(data[i] == '$') {
					if(i < data.length - 1 && data[i + 1] == '}') {						
						this.token = new Token(TokenType.CLOSE_TAG, "$}");
						this.currentIndex = i + 2;
						this.setState(LexerState.OUT_TAG);
						return this.token;
					}
				}
				
				if(data[i] == '*' || data[i] == '+' || data[i] == '-' || data[i] == '/') {
					if(data[i] == '-' && Character.isDigit(data[i + 1])) {
						continue;
					}
					this.token = new Token(TokenType.OPERATOR, String.valueOf(data[i]));
					this.currentIndex = i + 1;
					return this.token;
				}
				
				this.token = new Token(TokenType.SYMBOL, String.valueOf(data[i]));
				this.currentIndex = i + i;
				return this.token;
				
			}
			
		}
		
		return new Token(TokenType.EOF, null);
	}
	
	/**Method returns current token without consuming it from lexer.
	 * @return token type Token
	 */
	public Token getToken() {
		return this.token;
	}
	
	/**Method sets the state in which the lexer operates. There are two states: In tag or out Tag.
	 * @param state of type LexerState
	 */
	public void setState(LexerState state) {
		if(state == null) throw new LexerException();
		this.state = state;
	}
}
