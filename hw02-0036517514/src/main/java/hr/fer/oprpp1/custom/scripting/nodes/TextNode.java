package hr.fer.oprpp1.custom.scripting.nodes;

public class TextNode extends Node{
	
	private String text;
	
	public TextNode(String text) {
		this.text = text;
	}
	
	/**Method returns text from this TextNode
	 * @return String text
	 */
	public String getText() {
		return this.text;
	}
	
	@Override
	public String toString() {
		return getText();
	}
}
