package hr.fer.oprpp1.hw02.prob1;

public class Lexer {
	
	private char[] data;
	private Token token;
	private int currentIndex;
	private LexerState state;
	
	public Lexer(String text) {
		this.data = text.toCharArray();
		this.currentIndex = 0;
		this.state = LexerState.BASIC;
	}
	
	/**Method that returns the next token in the data sequence.
	 * @return Token
	 * @throws LexerException
	 */
	public Token nextToken() {
		if(this.token != null && this.token.getType() == TokenType.EOF) throw new LexerException();
		
		if(data.length == 0) {
			Token currentToken = new Token(TokenType.EOF, null);
			this.token = currentToken;
			return currentToken;
		}
		
		String word = "";
		String number = "";
		if(this.state == LexerState.BASIC) {
			for(int i = this.currentIndex; i < data.length; i++) {
				if(data[i] == '\r' || data[i] == '\n' || data[i] == '\t' || data[i] == ' ') {
					if(!(word.equals(""))) {
						this.currentIndex = i;
						this.token = new Token(TokenType.WORD, word);
						return this.token;
					}
					continue;
				}
				
				if(data[i] == '\\') {
					if(i < data.length - 1 && !(Character.isLetter(data[i + 1]))) {
						word += data[i + 1];
						i++;
						continue;
					}else 
						throw new LexerException();
				}
				
				if(Character.isLetter(data[i])) {
					if(!(number.equals(""))){
						this.token = new Token(TokenType.NUMBER, Long.parseLong(number));
						this.currentIndex = i;
						return this.token;
					}
					if(i < data.length - 1 && data[i + 1] != '\\' && !(Character.isLetter(data[i+1])) ) {
						word += String.valueOf(data[i]);
						this.token = new Token(TokenType.WORD, word);
						this.currentIndex = i + 1;
						return this.token;
					}
					word += String.valueOf(data[i]);
					
					if(i == data.length - 1) {
						this.token = new Token(TokenType.WORD, word);
						this.currentIndex = i + 1;
						return this.token;
					}else {
						continue;
					}
				}
				
				if(Character.isDigit(data[i])) {
					if(!(word.equals(""))){
						this.token = new Token(TokenType.WORD, word);
						this.currentIndex = i;
						return this.token;
					}
					number += String.valueOf(data[i]);
					if(i < data.length - 1 && !(Character.isDigit(data[i + 1]))) {
						try {
							this.token = new Token(TokenType.NUMBER, Long.parseLong(number));
							this.currentIndex = i + 1;
							return this.token;
						}catch(NumberFormatException e) {
							throw new LexerException();
						}
					}
					
					if(i == data.length - 1) {
						this.token = new Token(TokenType.NUMBER, Long.parseLong(number));
						this.currentIndex = i + 1;
						return this.token;
					}else {
						continue;
					}
				}
				
				if(data[i] == '#') {
					this.setState(LexerState.EXTENDED);
					this.token = new Token(TokenType.SYMBOL, Character.valueOf(data[i]));
					this.currentIndex = i + 1;
					return this.token;
				}
				
				this.token = new Token(TokenType.SYMBOL, Character.valueOf(data[i]));
				this.currentIndex = i + 1;
				return this.token;
				
			}
		}else {
			for(int i = currentIndex; i < data.length; i++) {
				if(data[i] == '\r' || data[i] == '\n' || data[i] == '\t'){
					continue;
				}
				
				if(data[i] == ' ') {
					if(!(word.equals(""))) {
						this.token = new Token(TokenType.WORD, word);
						this.currentIndex = i + 1;
						return this.token;
					}else {
						continue;
					}
				}
				
				if(data[i] == '#') {
					if(!(word.equals(""))) {
						this.token = new Token(TokenType.WORD, word);
						this.currentIndex = i;
						return this.token;
					}
					this.setState(LexerState.BASIC);
					this.token = new Token(TokenType.SYMBOL, Character.valueOf(data[i]));
					this.currentIndex = i + 1;
					return this.token;
				}
				
				word += String.valueOf(data[i]);
				continue;
				
			}
		}
		
		this.token = new Token(TokenType.EOF, null);
		return this.token;
	}
	
	/**Method that gets the current token
	 * @return Token
	 */
	public Token getToken() {
		return this.token;
	}
	
	/**Method that's used for changing the state in which the lexer operates. Default state is BASIC
	 * and the other state is EXTENDED.
	 * @param state that we want to set
	 */
	public void setState(LexerState state) {
		if(state == null) throw new NullPointerException();
		this.state = state;
	}
	
}
