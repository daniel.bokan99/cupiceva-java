package hr.fer.oprpp1.hw02.prob1;

public class LexerException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public LexerException() {
	}
	
	public LexerException(String msg) {
		super(msg);
	}

}
