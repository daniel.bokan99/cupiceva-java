package hr.fer.oprpp1.hw02.prob1;

public class Token {
	
	private TokenType type;
	private Object value;
	
	public Token(TokenType type, Object value) {
		this.type = type;
		this.value = value;
	}
	
	/**Method returns the value of the token.
	 * @return token value
	 */
	public Object getValue() {
		return this.value;
	}
	
	/**Method returns type of the token.
	 * @return token type
	 */
	public TokenType getType() {
		return this.type;
	}
	
}
