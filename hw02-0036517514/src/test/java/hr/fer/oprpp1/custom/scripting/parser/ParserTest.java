package hr.fer.oprpp1.custom.scripting.parser;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;

import hr.fer.oprpp1.custom.scripting.lexer.LexerException;
import hr.fer.oprpp1.custom.scripting.nodes.DocumentNode;
import hr.fer.oprpp1.custom.scripting.nodes.ForLoopNode;
import hr.fer.oprpp1.custom.scripting.nodes.TextNode;

public class ParserTest {
	
	@Test
	public void testOne() {
		String text = "Ovo je \r\n"
				+ "sve jedan text node\r\n"
				+ "";
		SmartScriptParser parser = new SmartScriptParser(text);
		DocumentNode document = parser.getDocument();
		assertEquals(1, document.numberOfChildren());
		assertEquals(document.getChild(0).getClass(), TextNode.class);
	}
	
	@Test
	public void testTwo() {
		String text = "Ovo je \r\n"
				+ "sve jedan \\{$ text node\r\n"
				+ "";
		SmartScriptParser parser = new SmartScriptParser(text);
		DocumentNode document = parser.getDocument();
		assertEquals(1, document.numberOfChildren());
		assertEquals(document.getChild(0).getClass(), TextNode.class);
	}
	
	@Test
	public void testThree() {
		String text = "Ovo je \r\n"
				+ "sve jedan \\\\\\{$text node";
		SmartScriptParser parser = new SmartScriptParser(text);
		DocumentNode document = parser.getDocument();
		assertEquals(1, document.numberOfChildren());
		assertEquals(document.getChild(0).getClass(), TextNode.class);
		
	}
	
	@Test
	public void testFour() {
		String text = "Ovo se ruši s iznimkom \\n \r\n"
				+ "jer je escape ilegalan ovdje.\r\n"
				+ "";
		assertThrows(LexerException.class, () -> {
			SmartScriptParser parser = new SmartScriptParser(text);
			DocumentNode document = parser.getDocument();
		});
	}
	
	@Test
	public void testFive() {
		String text = "Ovo se ruši \"s iznimkom \\n\" \r\n"
				+ "jer je escape ilegalan ovdje.\r\n"
				+ "";
		SmartScriptParser parser = new SmartScriptParser(text);
		DocumentNode document = parser.getDocument();
		assertEquals(1, document.numberOfChildren());
		assertEquals(document.getChild(0).getClass(), TextNode.class);
	}
	
	@Test
	public void testSix() {
		String text = "Ovo je OK ${ = \"String ide\r\n"
				+ "u više redaka\r\n"
				+ "čak tri\" $}\r\n"
				+ "";
		SmartScriptParser parser = new SmartScriptParser(text);
		DocumentNode document = parser.getDocument();
		assertEquals(1, document.numberOfChildren());
		assertEquals(document.getChild(0).getClass(), TextNode.class);
	}
	
	@Test
	public void testSeven() {
		String text = "Ovo je isto OK ${ = \"String ide\r\n"
				+ "u \\\"više\\\" \\nredaka\r\n"
				+ "ovdje a stvarno četiri\" $}\r\n"
				+ "";
		SmartScriptParser parser = new SmartScriptParser(text);
		DocumentNode document = parser.getDocument();
		assertEquals(1, document.numberOfChildren());
		assertEquals(document.getChild(0).getClass(), TextNode.class);
	}
	
	@Test
	public void testEight() {
		String text = "Ovo se ne ruši ${ = \"String ide\r\n"
				+ "u više \\{$ redaka\r\n"
				+ "čak tri\" $}\r\n"
				+ "";
		SmartScriptParser parser = new SmartScriptParser(text);
		DocumentNode document = parser.getDocument();
		assertEquals(1, document.numberOfChildren());
		assertEquals(document.getChild(0).getClass(), TextNode.class);
	}
	
	@Test
	public void testNine() {
		String text = "Ovo se ruši ${ = \\n $}\r\n"
				+ "";
		assertThrows(LexerException.class, () -> {
			SmartScriptParser parser = new SmartScriptParser(text);
			DocumentNode document = parser.getDocument();
		});
	}
	
	@Test
	public void testTen() {
		String text = "This is sample text.\r\n"
				+ "{$ FOR i 1 10 1 $}\r\n"
				+ " This is {$= i $}-th time this message is generated.\r\n"
				+ "{$END$}\r\n"
				+ "{$FOR i 0 10 2 $}\r\n"
				+ " sin({$=i$}^2) = {$= i i * @sin \"0.000\" @decfmt $}\r\n"
				+ "{$END$}";
		SmartScriptParser parser = new SmartScriptParser(text);
		DocumentNode document = parser.getDocument();
		assertEquals(4, document.numberOfChildren());
		assertEquals(document.getChild(0).getClass(), TextNode.class);
		assertEquals(document.getChild(1).getClass(), ForLoopNode.class);
		assertEquals(document.getChild(2).getClass(), TextNode.class);
		assertEquals(document.getChild(3).getClass(), ForLoopNode.class);
	}
	
	@Test
	public void testEleven() {
		String text = "This is sample text.\r\n"
				+ "{$ FOR i 1.57 -10 1 $}\r\n"
				+ " This is {$= i $}-th time this message is generated.\r\n"
				+ "{$END$}\r\n"
				+ "{$FOR for 0 10 2 $}\r\n"
				+ " sin({$=i$}^2) = {$= i i * @sin \"0.000\" @decfmt $}\r\n"
				+ "{$END$}";
		SmartScriptParser parser = new SmartScriptParser(text);
		DocumentNode document = parser.getDocument();
		//System.out.println(document.toString());
		assertEquals(4, document.numberOfChildren());
		assertEquals(document.getChild(0).getClass(), TextNode.class);
		assertEquals(document.getChild(1).getClass(), ForLoopNode.class);
		assertEquals(document.getChild(2).getClass(), TextNode.class);
		assertEquals(document.getChild(3).getClass(), ForLoopNode.class);
	}
	
	@Test
	public void testTwelve() {
		String text = "This is sample text.\r\n"
				+ "{$ FORi 1.57-10\"1\" $}\r\n"
				+ " This is {$= i $}-th time this message is generated.\r\n"
				+ "{$END$}\r\n"
				+ "{$FOR for 0 10 2 $}\r\n"
				+ " sin({$=i$}^2) = {$= i i * @sin \"0.000\" @decfmt $}\r\n"
				+ "{$END$}";
		SmartScriptParser parser = new SmartScriptParser(text);
		DocumentNode document = parser.getDocument();
		System.out.println(document.toString());
		assertEquals(4, document.numberOfChildren());
		assertEquals(document.getChild(0).getClass(), TextNode.class);
		assertEquals(document.getChild(1).getClass(), ForLoopNode.class);
		assertEquals(document.getChild(2).getClass(), TextNode.class);
		assertEquals(document.getChild(3).getClass(), ForLoopNode.class);
	}
	
	
}
