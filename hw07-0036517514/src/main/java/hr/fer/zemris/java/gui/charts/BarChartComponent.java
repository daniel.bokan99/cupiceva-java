package hr.fer.zemris.java.gui.charts;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import javax.swing.JComponent;

public class BarChartComponent extends JComponent{
	
	private static final long serialVersionUID = 1L;
	private BarChart barChart;
	
	public BarChartComponent(BarChart barChart) {
		this.barChart = barChart;
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		
		Graphics2D g2 = (Graphics2D) g;
		drawDescriptions(g2);
		drawAxis(g2);
		
		
	}
	
	private void drawDescriptions(Graphics2D g2) {
		
		AffineTransform defaultAt = g2.getTransform();
		
		AffineTransform at = AffineTransform.getQuadrantRotateInstance(3);
		g2.setTransform(at);
		String yDesc = barChart.getyDescription();
		setFont(getFont().deriveFont(20f));
		
		int w = getWidth();
		int h = getHeight();
		
		FontMetrics fm = g2.getFontMetrics();
		
		g2.drawString(yDesc, -h/2 - fm.stringWidth(yDesc)/2, fm.getAscent());
		
		g2.setTransform(defaultAt);
		
		String xDesc = barChart.getxDescription();
		g2.drawString(xDesc, w/2 - fm.stringWidth(xDesc)/2, h - fm.getAscent() + 10);
	}
	
	private void drawAxis(Graphics2D g2) {
		
		int w = getWidth();
		int h = getHeight();
		
		int minY = barChart.getMinY();
		int maxY = barChart.getMaxY();
		int difY = barChart.getDifY();
		
		FontMetrics fm = g2.getFontMetrics();
		
		//Y AXIS COORDINATES
		int yAxisStartx = fm.getAscent() + 30 + fm.stringWidth(String.valueOf(maxY));
		int yAxisStarty = 30;
		int yAxisEndx = fm.getAscent() + 30 + fm.stringWidth(String.valueOf(maxY));
		int yAxisEndy = h - fm.getAscent() - 40;
		
		//Y AXIS
		g2.drawLine(yAxisStartx, yAxisStarty, yAxisEndx, yAxisEndy);
		
		//X AXIS COORDINATES
		int xAxisStartx = fm.getAscent() + 20+ fm.stringWidth(String.valueOf(maxY));
		int xAxisStarty = h - fm.getAscent() - 30 - fm.getHeight();
		int xAxisEndx = w - 30;
		int xAxisEndy = h - fm.getAscent() - 30 - fm.getHeight();
		
		//X AXIS
		g2.drawLine(xAxisStartx, xAxisStarty, xAxisEndx, xAxisEndy);
		
		int[] xPoints = {xAxisEndx, xAxisEndx, xAxisEndx + 15};
		int[] yPoints = {xAxisEndy - 10, xAxisEndy + 10, xAxisEndy};
		
		g2.drawPolygon(xPoints, yPoints, 3);
		
		int[] xPoints2 = {yAxisStartx - 10, yAxisStartx + 10, yAxisStartx};
		int[] yPoints2 = {yAxisStarty, yAxisStarty, yAxisStarty - 15};
		
		g2.drawPolygon(xPoints2, yPoints2, 3);
		
		double yChange = (xAxisStarty - (yAxisStarty + 5)) / (maxY/difY);
		
		int x, y;
		x = xAxisStartx;
		y = xAxisStarty;
		
//		int[] yValues = new int[(maxY/difY) + 1];
		
		//NUMBERS ON Y AXIS
		for(int i = minY; i <= maxY; i = i + difY) {
			
			g2.drawLine(x, y, yAxisStartx, y);
			
//			yValues[i/difY] = y;
			
			g2.drawString(String.valueOf(i), x - fm.stringWidth(String.valueOf(i)), y + 7);
			
			y -= (int)yChange;
			
		}
		
		var list = barChart.getList();
		
		double xChange = ((xAxisEndx - 2) - yAxisStartx ) / list.size();
		
		y = yAxisEndy + 10;
		x = (int) (yAxisEndx + xChange/2);
		int lineX = (int) (yAxisEndx + xChange);
		int rectX = yAxisStartx;
		
		for(int i = 0; i < list.size(); i++) {
//			int rectYIndex = list.get(i).getY()/difY;
			//int rectY = yValues[rectYIndex];
			
			int rectY = (int)(xAxisStarty - (yChange * ((double)(list.get(i).getY() - minY)/difY)));
			
			g2.drawLine(lineX, xAxisEndy, lineX, yAxisEndy);
			
			g2.drawString(String.valueOf(list.get(i).getX()), x, y);
			
			
			g2.drawRect(rectX, rectY, (int) xChange, xAxisStarty - rectY);
			
			g2.setColor(Color.orange);
			g2.fillRect(rectX + 1, rectY + 1, (int) xChange - 2, xAxisStarty - rectY - 2);
			
			g2.setColor(Color.black);
			
			x += xChange;
			lineX += xChange;
			rectX += xChange;
			
		}
		
		
	}
	
	
	
}
