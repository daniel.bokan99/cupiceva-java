package hr.fer.zemris.java.gui.calc.Calculator;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import hr.fer.zemris.java.gui.calc.model.CalcModel;
import hr.fer.zemris.java.gui.calc.model.CalcValueListener;

public class JPrikaz extends JLabel implements CalcValueListener{
	
	private static final long serialVersionUID = 1L;
	private CalcModel model;
	
	public JPrikaz(CalcModel model) {
		this.model = model;
		setHorizontalAlignment(SwingConstants.RIGHT);
		setFont(getFont().deriveFont(50f));
		setBorder(BorderFactory.createLineBorder(Color.black, 2));
		model.addCalcValueListener(this);
	}

	@Override
	public void valueChanged(CalcModel model) {
		this.model = model;
		repaint();
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		g.setColor(getBackground());
		g.fillRect(0, 0, getWidth(), getHeight());
		
		g.setColor(getForeground());
		String broj = model.toString();
		FontMetrics fm = g.getFontMetrics();
		
		int w = getWidth();
		int stringW = fm.stringWidth(broj);
		
		int h = getHeight();
		Insets ins = getInsets();
		
		
		g.drawString(broj, w - stringW - ins.right, h/2 + 10);
	}
	
	

}
