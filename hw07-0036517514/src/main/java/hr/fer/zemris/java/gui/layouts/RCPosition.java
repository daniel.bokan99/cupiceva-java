package hr.fer.zemris.java.gui.layouts;

public class RCPosition {
	
	private int row;
	private int column;
	
	public RCPosition(int row, int column) {
		this.row = row;
		this.column = column;
	}
	
	public int getRow() {
		return row;
	}
	
	public int getColumn() {
		return column;
	}
	
	public static RCPosition parse(String text) {
		
		String[] args = text.split(",");
		if(args.length != 2) {
			throw new IllegalArgumentException("Invalid argument for parsing.");
		}
		
		int row = Integer.parseInt(args[0]);
		int column = Integer.parseInt(args[1]);
		
		return new RCPosition(row, column);
		
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RCPosition other = (RCPosition) obj;
		if (column != other.column)
			return false;
		if (row != other.row)
			return false;
		return true;
	}
	
	
	
	
	

}
