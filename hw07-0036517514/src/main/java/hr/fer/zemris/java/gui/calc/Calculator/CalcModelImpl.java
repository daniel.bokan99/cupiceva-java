package hr.fer.zemris.java.gui.calc.Calculator;

import java.util.ArrayList;
import java.util.List;
import java.util.function.DoubleBinaryOperator;

import hr.fer.zemris.java.gui.calc.model.CalcModel;
import hr.fer.zemris.java.gui.calc.model.CalcValueListener;
import hr.fer.zemris.java.gui.calc.model.CalculatorInputException;

public class CalcModelImpl implements CalcModel{
	
	private boolean isEditable;
	private boolean isPositive;
	private String inputNumbers;
	private double valueOFInputNumbers;
	private String frozenValue;
	private Double activeOperand;
	private DoubleBinaryOperator pendingOperation;
	private List<CalcValueListener> listeners;
	
	public CalcModelImpl() {
		isPositive = true;
		isEditable = true;
		this.inputNumbers = "";
		this.valueOFInputNumbers = 0;
		frozenValue = null;
		activeOperand = null;
		pendingOperation = null;
		listeners = new ArrayList<>();
	}
	
	@Override
	public void insertDigit(int digit) throws CalculatorInputException, IllegalArgumentException {
		
		if(!isEditable)
			throw new CalculatorInputException("Model is not editable.");
		
		if(digit == 0 && inputNumbers.equals("0")) {
			return;
		}else if(digit != 0 && inputNumbers.equals("0")) {
			inputNumbers = "";
		}
		
		String tempString = inputNumbers + String.valueOf(digit);
		
		try {
			valueOFInputNumbers = Double.parseDouble(tempString);
			inputNumbers += String.valueOf(digit);
			if(inputNumbers.length() > 308)
				throw new CalculatorInputException("Number too big");
			frozenValue = null;
		}catch(NumberFormatException e) {
			throw new CalculatorInputException("Cannot parse value into a double");
		}
		
		for(var l : listeners) {
			l.valueChanged(this);
		}
		
	}
	
	@Override
	public String toString() {
		if(frozenValue != null) {
			return isPositive ? "" : "-" + frozenValue;
		}
		
		if(inputNumbers.equals("")) {
			return isPositive ? "0" : "-0";
		}else {
			return isPositive ? inputNumbers : (inputNumbers);
		}
		
	}
	
	@Override
	public double getValue() {
		return isPositive ? valueOFInputNumbers : (0 - valueOFInputNumbers);
	}
	
	@Override
	public void setValue(double value) {
		valueOFInputNumbers = value;
		if(Double.compare(Math.round(value), value) == 0) {
			inputNumbers = String.valueOf(value);
			inputNumbers = inputNumbers.substring(0, inputNumbers.length() - 2);
		}else {
			inputNumbers = String.valueOf(value);
		}
		
		isPositive = value < 0 ? false : true;
		
		isEditable = false;
		for(var l : listeners) {
			l.valueChanged(this);
		}
	}
	
	@Override
	public void swapSign() throws CalculatorInputException {
		if(!isEditable)
			throw new CalculatorInputException("Model is not editable.");
		
		isPositive = !isPositive;
		frozenValue = null;
		for(var l : listeners) {
			l.valueChanged(this);
		}
	}
	
	@Override
	public void insertDecimalPoint() throws CalculatorInputException {
		if(!isEditable)
			throw new CalculatorInputException("Model is not editable");
		
		if(inputNumbers.length() == 0) {
			throw new CalculatorInputException("Cannot insert decimal point at the beggining");
		}
		
		if(inputNumbers.contains(".")) {
			throw new CalculatorInputException("Decimal point already exists");
		}else {
			inputNumbers += ".";
		}
		for(var l : listeners) {
			l.valueChanged(this);
		}
		
	}
	
	@Override
	public void setActiveOperand(double activeOperand) {
		this.activeOperand = activeOperand;
		inputNumbers = "";
		
	}
	
	@Override
	public void setPendingBinaryOperation(DoubleBinaryOperator op) {
		if(frozenValue != null)
			throw new CalculatorInputException("Cannot add binary operator while there is a frozen value.");
		
		if(pendingOperation != null) {
			
		}
		
		pendingOperation = op;
	}
	
	@Override
	public void clear() {
		inputNumbers = "";
		isEditable = true;
//		for(var l : listeners) {
//			l.valueChanged(this);
//		}
	}
	
	@Override
	public void clearAll() {
		inputNumbers = "";
		clearActiveOperand();
		pendingOperation = null;
		isEditable = true;
		isPositive = true;
		for(var l : listeners) {
			l.valueChanged(this);
		}
	}

	@Override
	public void addCalcValueListener(CalcValueListener l) {
		listeners.add(l);
		
	}

	@Override
	public void removeCalcValueListener(CalcValueListener l) {
		listeners.remove(l);
	}

	@Override
	public boolean isEditable() {
		return isEditable;
	}

	@Override
	public boolean isActiveOperandSet() {
		return activeOperand == null ? false : true;
	}

	@Override
	public double getActiveOperand() throws IllegalStateException {
		if(activeOperand == null) {
			throw new IllegalStateException();
		}
		return activeOperand;
	}

	@Override
	public void clearActiveOperand() {
		activeOperand = null;
	}

	@Override
	public DoubleBinaryOperator getPendingBinaryOperation() {
		return pendingOperation;
	}

	@Override
	public void freezeValue(String value) {
		frozenValue = value;
	}

	@Override
	public boolean hasFrozenValue() {
		return frozenValue != null ? true : false;
	}

}
