package hr.fer.zemris.java.gui.charts;

import java.util.List;

public class BarChart {
	
	private List<XYValue> list;
	private String xDescription;
	private String yDescription;
	private int minY;
	private int maxY;
	private int difY;
	public BarChart(List<XYValue> list, String xDescription, String yDescription, int minY, int maxY, int difY) {
		if(minY < 0) throw new IllegalArgumentException("Y min cannot be less than 0");
		if(maxY <= minY) throw new IllegalArgumentException("Y max must be greater than Y min");
		
		for(var value : list) {
			if(value.getY() < minY) throw new IllegalArgumentException(String.valueOf(value.getY()) + "is lesser than Y min");
		}
		
		if((maxY - minY) % difY != 0) {
			int newMaxY = maxY;
			while((newMaxY - minY) % difY != 0){
				newMaxY++;
			}
			maxY = newMaxY;
			
//			for(int i = 0; i < list.size(); i++) {
//				if(list.get(i).get)
//			}
			
		}
		
		this.list = list;
		this.xDescription = xDescription;
		this.yDescription = yDescription;
		this.minY = minY;
		this.maxY = maxY;
		this.difY = difY;
	}
	/**
	 * @return the list
	 */
	public List<XYValue> getList() {
		return list;
	}
	/**
	 * @return the xDescription
	 */
	public String getxDescription() {
		return xDescription;
	}
	/**
	 * @return the yDescription
	 */
	public String getyDescription() {
		return yDescription;
	}
	/**
	 * @return the minY
	 */
	public int getMinY() {
		return minY;
	}
	/**
	 * @return the maxY
	 */
	public int getMaxY() {
		return maxY;
	}
	/**
	 * @return the difY
	 */
	public int getDifY() {
		return difY;
	}
}
