package hr.fer.zemris.java.gui.calc.Calculator;

import java.awt.Color;
import java.awt.Container;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.function.DoubleBinaryOperator;
import java.util.function.DoubleUnaryOperator;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import hr.fer.zemris.java.gui.calc.model.CalculatorInputException;
import hr.fer.zemris.java.gui.layouts.CalcLayout;
import hr.fer.zemris.java.gui.layouts.RCPosition;

public class Calculator extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private CalcModelImpl model;
	private List<Pair<DoubleUnaryOperator>> unaryOperations;
	private List<DoubleUnaryOperator>invUnaryOperations;
	private List<Pair<DoubleBinaryOperator>> binaryOperations;
	private Stack<Double> stack;
	
	public Calculator(CalcModelImpl model) {
		
		binaryOperations = new ArrayList<>();
		unaryOperations = new ArrayList<>();
		invUnaryOperations = new ArrayList<>();
		stack = new Stack<>();
		this.model = model;
		setTitle("Calculator V1.0");
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		initGUI();
		pack();
	}
	
	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new CalcLayout());
		
		JPrikaz prikaz = new JPrikaz(model);
		prikaz.setBackground(Color.yellow);
		prikaz.setFont(prikaz.getFont().deriveFont(30f));
		cp.add(prikaz, new RCPosition(1,1));
		
		addDigitButton(0, new RCPosition(5, 3), cp);
		int digit = 1;
		
		for(int i = 4; i >= 2; i--) {
			for(int j = 3; j <= 5; j++) {
				addDigitButton(digit, new RCPosition(i, j), cp);
				digit++;
			}
		}
		
		
		JButton swapBtn = new JButton("+/-");
		swapBtn.addActionListener(e -> model.swapSign());
		cp.add(swapBtn, new RCPosition(5, 4));
		
		JButton dotBtn = new JButton(".");
		dotBtn.addActionListener(e -> model.insertDecimalPoint());
		cp.add(dotBtn, new RCPosition(5, 5));
		
		JCheckBox inv = new JCheckBox("inv");
		cp.add(inv, new RCPosition(5, 7));
		
		prepareUnaryOperations();
		
		for(int i = 0; i < unaryOperations.size(); i++) {
			RCPosition rcp = null;
			if(i < 4) rcp = new RCPosition(i + 2, 2);
			else rcp = new RCPosition(i - 1, 1); 
			
			addUnaryButton(unaryOperations.get(i).x, invUnaryOperations.get(i), inv, cp, rcp, unaryOperations.get(i).y);	
		}
		
		JButton recipro = new JButton("1/x");
		recipro.addActionListener(e -> {
			double value = model.getValue();
			DoubleUnaryOperator op = x -> 1/x;
			model.setValue(op.applyAsDouble(value));
		});
		cp.add(recipro, new RCPosition(2, 1));
		
		JButton clr = new JButton("clr");
		clr.addActionListener(e -> model.clear());
		cp.add(clr, new RCPosition(1,7));
		
		JButton reset = new JButton("reset");
		reset.addActionListener(e -> model.clearAll());
		cp.add(reset, new RCPosition(2,7));
		
		JButton push = new JButton("push");
		push.addActionListener(e -> {
			double value = model.getValue();
			stack.push(value);
		});
		cp.add(push, new RCPosition(3,7));
		
		JButton pop = new JButton("pop");
		pop.addActionListener(e -> {
			double value = stack.pop();
			model.setValue(value);
		});
		cp.add(pop, new RCPosition(4,7));
		
		prepareBinaryOperations();
		
		for(int i = 0; i < binaryOperations.size(); i++) {
			RCPosition rcp = new RCPosition(i + 2, 6);
			addBinaryButton(binaryOperations.get(i).x, cp, rcp, binaryOperations.get(i).y);
		}
		
		JButton equals = new JButton("=");
		equals.addActionListener(e -> {
			double value = model.getValue();
			double activeOperand = model.getActiveOperand();
			DoubleBinaryOperator operator = model.getPendingBinaryOperation();
			value = operator.applyAsDouble(activeOperand, value);
			model.setValue(value);
		});
		cp.add(equals, new RCPosition(1,6));
		
		JButton power = new JButton("X^n");
		power.addActionListener(e ->{
			double value = model.getValue();
			model.setActiveOperand(value);
			model.setPendingBinaryOperation((x,y) -> Math.pow(x, y));
			model.clear();
		});
		cp.add(power, new RCPosition(5,1));
		
	}
	
	private void addBinaryButton(DoubleBinaryOperator operation, Container cp, RCPosition rcp, String text) {
		JButton btn = new JButton(text);
		btn.addActionListener(e -> {
			if(model.hasFrozenValue()) {
				throw new CalculatorInputException("Model has a frozen value");
			}
			double value = model.getValue();
			model.setActiveOperand(value);
			model.setPendingBinaryOperation(operation);
			model.freezeValue(String.valueOf(value));
			model.clear();
		});
		btn.setFont(btn.getFont().deriveFont(20f));
		cp.add(btn, rcp);
	}
	
	private void prepareBinaryOperations() {
		binaryOperations.add(new Pair<DoubleBinaryOperator>((x,y) -> x/y, "/"));
		binaryOperations.add(new Pair<DoubleBinaryOperator>((x,y) -> x*y, "*"));
		binaryOperations.add(new Pair<DoubleBinaryOperator>((x,y) -> x-y, "-"));
		binaryOperations.add(new Pair<DoubleBinaryOperator>((x,y) -> x+y, "+"));
	}
	
	private void addUnaryButton(DoubleUnaryOperator operation1 ,DoubleUnaryOperator operation2, JCheckBox inv, Container cp, RCPosition rcp, String text) {
		JButton opBtn = new JButton(text);
		opBtn.addActionListener(e -> {
			if(model.hasFrozenValue()) {
				throw new CalculatorInputException("Model has a frozen value");
			}
			double value = model.getValue();
			if(inv.isSelected()) {
				value = operation2.applyAsDouble(value);
			}else {
				value = operation1.applyAsDouble(value);
			}
			model.setValue(value);
		});
		
		cp.add(opBtn, rcp);
	}
	
	private void prepareUnaryOperations() {
		unaryOperations.add(new Pair<DoubleUnaryOperator>(Math::sin, "sin"));
		unaryOperations.add(new Pair<DoubleUnaryOperator>(Math::cos, "cos"));
		unaryOperations.add(new Pair<DoubleUnaryOperator>(Math::tan, "tan"));
		unaryOperations.add(new Pair<DoubleUnaryOperator>(x -> 1/Math.tan(x), "ctg"));
		unaryOperations.add(new Pair<DoubleUnaryOperator>(Math::log10, "log"));
		unaryOperations.add(new Pair<DoubleUnaryOperator>(Math::log, "ln"));
		
		invUnaryOperations.add(Math::asin);
		invUnaryOperations.add(Math::acos);
		invUnaryOperations.add(Math::atan);
		invUnaryOperations.add(x -> 1/Math.atan(x));
		invUnaryOperations.add(x -> Math.pow(10, x));
		invUnaryOperations.add(x -> Math.pow(Math.E, x));
		
	}
	
	private void addDigitButton(int digit, RCPosition rcp, Container cp) {
		JButton btn = new JButton(String.valueOf(digit));
		btn.addActionListener(e -> model.insertDigit(digit));
		btn.setFont(btn.getFont().deriveFont(30f));
		cp.add(btn, rcp);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() ->{
			new Calculator(new CalcModelImpl()).setVisible(true);
		});
	}

}
