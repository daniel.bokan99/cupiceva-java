package hr.fer.zemris.java.gui.prim;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;


public class PrimListModel implements ListModel<Integer>{
	
	private List<Integer> numbers;
	private List<ListDataListener> listeners = new ArrayList<>();
	
	public PrimListModel() {
		numbers = new ArrayList<>();
		numbers.add(1);
	}
	
	public void next() {
		int last = numbers.get(numbers.size() - 1);
		int current;
		boolean isPrim = false;
		for(int i = last + 1; ; i++) {
			int counter = 0;
			for(int j = 2; j < i; j++) {
				if(i % j == 0) {
					break;
				}else {
					counter++;
				}
			}
			if(counter == i - 2) {
				current = i;
				break;
			}
		}
		
		numbers.add(current);
		
		int size = numbers.size() -1;
		ListDataEvent e = new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, size, size);
		for(ListDataListener l : listeners) {
			l.intervalAdded(e);
		}
	}

	@Override
	public int getSize() {
		return numbers.size();
	}

	@Override
	public Integer getElementAt(int index) {
		return numbers.get(index);
	}

	@Override
	public void addListDataListener(ListDataListener l) {
		listeners.add(l);
	}

	@Override
	public void removeListDataListener(ListDataListener l) {
		listeners.remove(l);
	}
	
}
