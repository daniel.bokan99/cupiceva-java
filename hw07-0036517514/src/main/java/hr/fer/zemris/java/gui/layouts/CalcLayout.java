package hr.fer.zemris.java.gui.layouts;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

public class CalcLayout implements LayoutManager2{
	
	private int gap;
	private HashMap<RCPosition, Component> zauzetePozicije;
	
	public CalcLayout(int gap) {
		this.gap = gap;
		zauzetePozicije = new HashMap<>();
	}
	
	public CalcLayout() {
		this(0);
	}
	

	@Override
	public void addLayoutComponent(String name, Component comp) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public void removeLayoutComponent(Component comp) {
		
		var positions = zauzetePozicije.keySet();
		for(var rcp : positions) {
			if(zauzetePozicije.get(rcp).equals(comp)) {
				zauzetePozicije.remove(rcp);
			}
		}
		
		
	}

	@Override
	public Dimension preferredLayoutSize(Container parent) {
		var components = parent.getComponents();
		Dimension maxD = components[0].getPreferredSize();
		for(Component c : components) {
			int area = c.getPreferredSize().height * c.getPreferredSize().width;
			if(area > (maxD.height * maxD.width)) {
				maxD = c.getPreferredSize();
			}
		}
			
		return new Dimension(maxD.width*7, maxD.height*5);
	}

	@Override
	public Dimension minimumLayoutSize(Container parent) {
		var components = parent.getComponents();
		Dimension maxD = components[0].getMinimumSize();
		for(Component c : components) {
			int area = c.getMinimumSize().height * c.getMinimumSize().width;
			if(area > (maxD.height * maxD.width)) {
				maxD = c.getMinimumSize();
			}
		}
			
		return new Dimension(maxD.width*7, maxD.height*5);
	}

	@Override
	public void layoutContainer(Container parent) {
		
		Insets ins = parent.getInsets();
		int w = parent.getWidth();
		int h = parent.getHeight();
		
		double columnWidth = w/7;
		double rowHeigth = h/5;
		
		for(var key : zauzetePozicije.keySet()) {
			
			Component c = zauzetePozicije.get(key);
			if(key.getRow() == 1 && key.getColumn() == 1){
				c.setBounds(ins.left + gap, ins.top + gap,(int) (5 * columnWidth), (int)rowHeigth);
			}else {
				int row = key.getRow();
				int column = key.getColumn();
				c.setBounds((int)(ins.left + (column-1) * (columnWidth + gap)), (int)(ins.top + (row - 1) * (rowHeigth + gap) ), (int)columnWidth, (int)rowHeigth);
			}
			
		}
		
		
	}

	@Override
	public void addLayoutComponent(Component comp, Object constraints) {
		
		if(!(constraints instanceof String) && !(constraints instanceof RCPosition)) {
			throw new IllegalArgumentException("Constraint must be a string or an instance of RCPosition");
		}
		
		if(comp == null || constraints == null) {
			throw new NullPointerException("Component and constraint cannot be null");
		}
		
		RCPosition rcp = null;
		if(constraints instanceof String) {
			try {
				rcp = RCPosition.parse((String)constraints);
			}catch(Exception e) {
				throw new IllegalArgumentException("Unable to parse constraint.");
			}
		}else {
			rcp = (RCPosition) constraints;
		}
		
		int row = rcp.getRow();
		int column = rcp.getColumn();
		if(row < 1 || row > 5 || column < 1 || column > 7) {
			throw new CalcLayoutException("Illegal row or column number.");
		}
		if(row == 1 && column > 1 && column < 6) {
			throw new CalcLayoutException("Illegal row or column number.");
		}
		if(zauzetePozicije.get(rcp) != null) {
			throw new CalcLayoutException("Constraint already defined.");
		}
		
		zauzetePozicije.put(rcp, comp);
	}

	@Override
	public Dimension maximumLayoutSize(Container target) {
		
		var components = target.getComponents();
		Dimension maxD = components[0].getMaximumSize();
		for(Component c : components) {
			int area = c.getMaximumSize().height * c.getMaximumSize().width;
			if(area > (maxD.height * maxD.width)) {
				maxD = c.getMaximumSize();
			}
		}
			
		return new Dimension(maxD.width*7, maxD.height*5);
	}

	@Override
	public float getLayoutAlignmentX(Container target) {
		return 0;
	}

	@Override
	public float getLayoutAlignmentY(Container target) {
		return 0;
	}

	@Override
	public void invalidateLayout(Container target) {
		// TODO Auto-generated method stub
		
	}
	
	public static class DemoFrame1 extends JFrame{
		
		private static final long serialVersionUID = 1L;

		public DemoFrame1() {
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			initGUI();
			pack();
		}
		
		private void initGUI() {
			Container cp = getContentPane();
			cp.setLayout(new CalcLayout(3));
			cp.add(l("tekst 1"), new RCPosition(1,1));
			cp.add(l("nesto"), new RCPosition(1, 6));
			cp.add(l("nesto"), new RCPosition(1, 7));
			cp.add(l("tekst 2"), new RCPosition(2,3));
			cp.add(l("tekst stvarno najdulji"), new RCPosition(2,7));
			cp.add(l("tekst kraći"), new RCPosition(4,2));
			cp.add(l("tekst srednji"), new RCPosition(4,5));
			cp.add(l("tekst"), new RCPosition(4,7));
		}
		
		private JLabel l(String text) {
			JLabel l = new JLabel(text);
			l.setBackground(Color.YELLOW);
			l.setOpaque(true);
			return l;
		}
		
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(()->{
			new DemoFrame1().setVisible(true);
		});
	}

	
	
}
