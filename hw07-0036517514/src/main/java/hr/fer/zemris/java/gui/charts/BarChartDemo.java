package hr.fer.zemris.java.gui.charts;

import java.awt.BorderLayout;
import java.awt.Container;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

public class BarChartDemo extends JFrame{

	private static final long serialVersionUID = 1L;
	private BarChartComponent bcc;
	private String path;
	
	public BarChartDemo(BarChartComponent bcc, String path) {
		this.bcc = bcc;
		this.path = path;
		initGUI();
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		pack();
		setTitle("BarChartDemo v1.0");
	}
	
	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		cp.add(bcc, BorderLayout.CENTER);
		
		JLabel label = new JLabel(path);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		cp.add(label, BorderLayout.PAGE_START);
	}

	public static void main(String[] args) {
		String line = null;
		int counter = 0;
		List<String> inputData = new ArrayList<>();
		
		try(BufferedReader br = new BufferedReader(new FileReader(args[0]))) {
			
			while((line = br.readLine()) != null && counter < 6) {
				inputData.add(line);
				counter++;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		String xDescription = inputData.get(0);
		String yDescription = inputData.get(1);
		
		String[] temp = inputData.get(2).split("\\s+");
		
		List<XYValue> list = new ArrayList<>();
		
		for(String s : temp) {
			String[] arguments = s.split(",");
			list.add(new XYValue(Integer.parseInt(arguments[0]), Integer.parseInt(arguments[1])));
		}
		
		int minY = Integer.parseInt(inputData.get(3));
		int maxY = Integer.parseInt(inputData.get(4));
		int difY = Integer.parseInt(inputData.get(5));
		
		BarChart bc = new BarChart(list, xDescription, yDescription, minY, maxY, difY);
		
		SwingUtilities.invokeLater(() ->{
			new BarChartDemo(new BarChartComponent(bc), args[0]).setVisible(true);;
		});
		
		
	}
	

}
