package hr.fer.zemris.java.gui.calc.Calculator;

public class Pair<T>{
	
	public T x;
	public String y;
	
	public Pair(T x, String y) {
		this.x = x;
		this.y = y;
	}

}
