package hr.fer.oprpp1.custom.collections;

public class ObjectStack {
	
	private ArrayIndexedCollection collection;
	
	public ObjectStack() {
		this.collection = new ArrayIndexedCollection();
	}
	
	/**Returns true if collection contains no objects and false otherwise.
	 *
	 */
	public boolean isEmpty() {
		return collection.isEmpty();
	}
	
	/**Returns the number of currently stored objects in this collection.
	 *
	 */
	public int size() {
		return collection.size();
	}
	
	/**Pushes value on top of the stack.
	 * @param value that is being pushed on top of stack
	 * @throws IllegalArgumentException
	 */
	public void push(Object value) {
		if(value == null)
			throw new IllegalArgumentException("Value cannot be null");
		collection.insert(value, 0);
	}
	
	/**Removes last value pushed on stack from stack and returns it.
	 * @return
	 * @throws EmptyStackException
	 */
	public Object pop() {
		if(collection.isEmpty())
			throw new EmptyStackException("Cannot pop while stack is empty");
		
		Object obj = collection.get(0);
		collection.remove(0);
		
		return obj;
	}
	
	/**Returns the last element placed on stack but does not delete it from stack.
	 * @return object on top of stack
	 * @throws EmptyStackException
	 */
	public Object peek() {
		if(collection.isEmpty())
			throw new EmptyStackException("Cannot peek while stack is empty");
		
		Object obj = collection.get(0);
		return obj;
	}
	
	/**Removes all elements from stack.
	 * 
	 */
	public void clear() {
		collection.clear();
	}
	
	
}
