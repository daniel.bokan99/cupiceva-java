package hr.fer.oprpp1.custom.collections.demo;

import hr.fer.oprpp1.custom.collections.ObjectStack;

public class StackDemo {
	
	public static void main(String[] args) {
		String expression = args[0];
		String[] splitExpr = expression.split(" ");
		
		ObjectStack objStack = new ObjectStack();
		
		for(String s : splitExpr) {
			try {
				int number = Integer.parseInt(s);
				objStack.push(number);
				continue;
			}catch(NumberFormatException e) {
				int secondNumber = (int) objStack.pop();
				int firstNumber = (int) objStack.pop();
				int result;
				switch (s) {
				case "+":
					result = firstNumber + secondNumber;
					objStack.push(result);
					break;
				case "-":
					result = firstNumber - secondNumber;
					objStack.push(result);
					break;
				case "*":
					result = firstNumber*secondNumber;
					objStack.push(result);
					break;
				case "/":
					if(secondNumber == 0)
						throw new IllegalArgumentException("Cannot divide with 0");
					result = firstNumber/secondNumber;
					objStack.push(result);
					break;
				default:
					throw new IllegalArgumentException("Unexpected value: " + s);
				}
				continue;
			}
		}
		if(objStack.size() != 1)
			throw new RuntimeException("Stack size should be 1, and its " + objStack.size());
		else
			System.out.println(objStack.pop());
	}
}
