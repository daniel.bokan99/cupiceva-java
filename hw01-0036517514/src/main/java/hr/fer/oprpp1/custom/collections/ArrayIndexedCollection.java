package hr.fer.oprpp1.custom.collections;

public class ArrayIndexedCollection extends Collection{
	
	private int size;
	private Object[] elements;
	
	public ArrayIndexedCollection(Collection collection, int initialCapacity) {
		if(collection == null) throw new NullPointerException("Collection must not be null");
		
		if(collection.size() > initialCapacity) {
			//this.size = collection.size();
			this.elements = new Object[collection.size()];
		}else {
			//this.size = collection.size();
			this.elements = new Object[initialCapacity];
		}
		this.size = 0;
		this.addAll(collection);
	}
	
	public ArrayIndexedCollection(Collection collection) {
		this(collection, 16);
	}
	
	public ArrayIndexedCollection(int initialCapacity) {
		if(initialCapacity < 1) {
			throw new IllegalArgumentException("Initial capacity must be larger than 1. Now it it is: " + initialCapacity);
		}else {
			this.size = 0;
			this.elements = new Object[initialCapacity];
		}
	}
	
	public ArrayIndexedCollection() {
		this(16);
	}
	
	/**Adds the given object into this collection. Reference is added into first empty place
	 * in the elements array; if the elements array is full, it should be reallocated by 
	 * doubling its size.
	 * 
	 * @throws NullPointerException if you try to add null as element
	 * @param value that is being added
	 */
	@Override
	public void add(Object value) {
		if(value == null) throw new NullPointerException("Value cannot be null");
		
		if(size < this.elements.length) {
			this.elements[size] = value;
			this.size++;
		}else {
			int newSize = this.elements.length * 2;
			Object[] newElements = new Object[newSize];
			for(int i = 0; i < this.elements.length; i++) {
				newElements[i] = this.elements[i];
			}
			
			this.elements = newElements;
			this.elements[size] = value;
			this.size++;
		}
	}
	
	/**Returns the object that is stored in backing array at position index.
	 * Valid indexes are 0 to size-1.
	 * 
	 * @param index of the searched object
	 * @return object that is under the index we are looking for
	 * @throws IndexOutOfBoundsException if the index we are searching for is out of bounds
	 */
	public Object get(int index) {
		if(index < 0 || index > (this.size - 1)) 
			throw new IndexOutOfBoundsException("Index you are searching for must be in range of 0 to " + (this.size - 1) + ".");
		
		return this.elements[index];
	}
	
	/**Removes all elements from the collection. The allocated array is left at current 
	 * capacity. 
	 *
	 */
	@Override
	public void clear() {
		for(int i = 0; i < this.size; i++) {
			this.elements[i] = null;
		}
		this.size = 0;
	}
	
	/**Inserts the given value at the given position in array. The method does not overwrite
	 * current Object but instead shifts all the other Objects one place towards the end.
	 * Legal positions are 0 to size.
	 * @param value that is being inserted
	 * @param position at which the value is being inserted at
	 * @throws IndexOutOfBoundsException
	 */
	public void insert(Object value, int position) {
		if(position < 0 || position > this.size)
			throw new IndexOutOfBoundsException("Position is out of bounds");
		
		//check if the last index of the elements array is null so that other Objects can be shifted -> if not -> make array bigger

		if(this.size == this.elements.length) {
			int newSize = this.elements.length * 2;
			Object[] newElements = new Object[newSize];
			for(int i = 0; i < this.elements.length; i++) {
				newElements[i] = this.elements[i];
			}
			
			this.elements = newElements;
		}
		
		for(int i = this.size - 1; i >= position; i--) {
			this.elements[i + 1] = this.elements[i];
		}
		
		this.elements[position] = value;
		this.size++;
		
	}
	
	/**Searches the collection and returns the index of the first occurrence of the given
	 * value or -1 if the value is not found. 
	 * @param value that is being searched for. Can be null.
	 * @return index of the searched value if its found or -1 if its not found.
	 */
	public int indexOf(Object value) {
		if(value == null) return -1;
		
		for(int i = 0; i < this.size; i++) {
			if(value.equals(this.elements[i])) 
				return i;
		}
		
		return -1;
	}
	
	/**Removes element at specified index from collection. Other elements after the removed
	 * element are shifted to the left.
	 * @param index of the element that is to be removed. Legal indexes are 0 to size-1
	 * @throws IndexOutOfBoundsException
	 */
	public void remove(int index) {
		if(index < 0 || index > this.size - 1) 
			throw new IndexOutOfBoundsException("Index is out of bounds, must be within 0 to " + (this.size-1) + ".");
		
		for(int i = index; i < this.size - 1; i++) {
			this.elements[i] = this.elements[i + 1];
		}
		
		this.elements[this.size - 1] = null;
		this.size--;
	}
	
	/**Returns the number of currently stored objects in this collection.
	 *
	 */
	@Override
	public int size() {
		return this.size;
	}
	
	/**Returns true if collection contains no objects and false otherwise.
	 *
	 */
	@Override
	public boolean isEmpty() {
		if(this.size() == 0) return true;
		return false;
	}
	
	/**Returns true only if the collection contains given value, as determined by equals method.
	 * @param value that is being examined if the collection contains it
	 * @return true if the collection contains the value, false if not
	 */
	@Override
	public boolean contains(Object value) {
		for(int i = 0; i < this.size; i++) {
			if(value.equals(this.elements[i])) 
				return true;
		}
		
		return false;
	}
	
	/**Returns true only if collection contains given value as determined by equals method 
	 * and removes one occurrence of it.
	 * @param value that is being removed if the collection contains it
	 * @return true if the value has been removed, false if not
	 */
	@Override
	public boolean remove(Object value) {
		boolean removed = false;
		for(int i = 0; i < this.size; i++) {
			if(value.equals(this.elements[i])) {
				this.elements[i] = null;
				for(int j = i; j < this.size - 1; j++) {
					this.elements[j] = this.elements[j + 1];
				}
				removed = true;
				this.size--;
				break;
			}
		}
		
		return removed;
	}
	
	/**Allocates new array with size equals to the size of this collections, fills it with
	 * collection content and returns the array.
	 * @return Object[] array filled with collection content
	 */
	@Override
	public Object[] toArray() {
		Object[] array = new Object[this.size];
		for(int i = 0; i < this.size; i++) {
			array[i] = this.elements[i];
		}
		
		return array;
	}
	
	/**Method calls processor.process(.) for each element of this collection. 
	 * The order in which elements will be sent is undefined in this class. 
	 * @param processor that is being called for each element of this collection.
	 */
	@Override
	public void forEach(Processor processor) {
		for(int i = 0; i < this.size; i++) {
			processor.process(this.elements[i]);
		}
	}
	
	/**Method adds into the current collection all elements from the given collection.
	 * This other collection remains unchanged.
	 * @param other collection that is added onto this collection
	 */
	@Override
	public void addAll(Collection other) {
		
		class LocalProcessor extends Processor{
			
			public LocalProcessor() {
			}
			
			public void process(Object value) {
				add(value);
			}
		}
		
		LocalProcessor processor = new LocalProcessor();
		other.forEach(processor);
	}
	
	/**Prints the content of the collection.
	 * 
	 */
	public void print() {
		for(int i = 0; i < this.size; i++) {
			System.out.println(this.elements[i]);
		}
	}
	
}
