package hr.fer.oprpp1.custom.collections;

public class Collection {
	
	public Collection() {}
	
	/**
	 * Returns true if collection contains no objects and false otherwise.
	 * @return true
	 */
	public boolean isEmpty() {
		if(this.size() == 0) return true;
		return false;
	}
	
	/**Returns the number of currently stored objects in collections.
	 * @return 0
	 */
	public int size() {
		return 0;
	}
	
	/**Adds the given object into this collection.
	 * @param value that is being added to this collection.
	 */
	public void add(Object value) {}
	
	/**Returns true only if the collection contains given value, as determined by equals method.
	 * @param value that is being examined if the collection contains it
	 * @return false
	 */
	public boolean contains (Object value) {
		return false;
	}
	
	/**Returns true only if collection contains given value as determined by equals method 
	 * and removes one occurrence of it.
	 * @param value that is being removed if the collection contains it
	 * @return false
	 */
	public boolean remove(Object value) {
		return false;
	}
	
	/**Allocates new array with size equals to the size of this collections, fills it with
	 * collection content and returns the array.
	 * @return Object[] array filled with collection content
	 */
	public Object[] toArray() {
		throw new UnsupportedOperationException();
	}
	
	/**Method calls processor.process(.) for each element of this collection. 
	 * The order in which elements will be sent is undefined in this class. 
	 * @param processor that is being called for each element of this collection.
	 */
	public void forEach(Processor processor) {}
	
	/**Method adds into the current collection all elements from the given collection.
	 * This other collection remains unchanged.
	 * @param other collection that is added onto this collection
	 */
	public void addAll(Collection other) {
		
		class LocalProcessor extends Processor{
			
			public LocalProcessor() {
			}
			
			public void process(Object value) {
				add(value);
			}
		}
		
		LocalProcessor processor = new LocalProcessor();
		other.forEach(processor);
	}
	
	/**Removes all elements from the collection.
	 * 
	 */
	public void clear() {}
	
}
