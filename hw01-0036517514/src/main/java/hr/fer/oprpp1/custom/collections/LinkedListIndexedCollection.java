package hr.fer.oprpp1.custom.collections;

public class LinkedListIndexedCollection extends Collection {
	
	private static class ListNode{
		ListNode previous;
		ListNode next;
		Object value;
	}
	
	private int size;
	private ListNode first;
	private ListNode last;
	
	LinkedListIndexedCollection() {
		this.size = 0;
		this.first = null;
		this.last = null;
	}
	
	LinkedListIndexedCollection(Collection collection) {
		this.size = collection.size();
		this.addAll(collection);
	}
	
	/**Returns true if collection contains no objects and false otherwise.
	 *
	 */
	@Override
	public boolean isEmpty() {
		if(this.size == 0) return true;
		return false;
	}
	
	/**Returns the number of currently stored objects in this collection.
	 *
	 */
	@Override
	public int size() {
		return this.size;
	}
	
	/**Adds the given object into this collection.
	 *@throws NullPointerException
	 */
	@Override
	public void add(Object value) {
		if(value == null) throw new NullPointerException("Value that you are trying to insert cannot be null");
		
		ListNode node = new ListNode();
		node.value = value;
		
		if(this.first == null) {
			this.first = this.last = node;
			this.first.previous = null;
			this.last.next = null;
		}else {
			last.next = node;
			node.previous = last;
			last = node;
			last.next = null;
		}
		
		this.size++;
	}
	
	/**Returns the object that is stored in linked list at position index. Valid indexes are
	 * 0 to size - 1.
	 * @param index at which the object is that we wish to get
	 * @return Object that is at the wished index
	 * @throws IndexOutOfBoundsException
	 */
	public Object get(int index) {
		if(index < 0 || index > (this.size - 1)) 
			throw new IndexOutOfBoundsException("Index should be between 0 and " + (this.size - 1) + ". Now it is " + index + ".");
		
		int half = size/2;
		int current;
		ListNode currNode; 
		if(index <= half) {
			currNode = first;
			current = 0;
			while(current != index) {
				currNode = currNode.next;
				current++;
			}
			return currNode.value;
		}else {
			currNode = last;
			current = this.size - 1;
			while(current != index) {
				currNode = currNode.previous;
				current--;
			}
			return currNode.value;
		}
	}
	
	/**Removes all elements from the collection.
	 *
	 */
	@Override
	public void clear() {
		this.size = 0;
		this.first = null;
		this.last = null;
	}
	
	/**Inserts(does not overwrite) the given value at the given position in linked-list. 
	 * Elements starting from this position are shifted one position. The legal positions are 
	 * 0 to size. 
	 * @param value that is to be inserted 
	 * @param position at which the value is to be inserted
	 * @throws IndexOutOfBoundsException
	 */
	public void insert(Object value, int position) {
		if(position < 0 || position > this.size) 
			throw new IndexOutOfBoundsException("Position should be between 0 and " + this.size + ". Now it is " + position + ".");
		
		ListNode currNode = first;
		ListNode insertNode = new ListNode();
		insertNode.value = value;
		int current = 0;
		while(currNode != null && current != position) {
			currNode = currNode.next;
			current++;
		}
		
		if(current == this.size) {
			this.add(value);
		}
		else if(current == 0) {
			first.previous = insertNode;
			insertNode.next = first;
			first = insertNode;
			this.size++;
		}else {
			insertNode.next = currNode;
			insertNode.previous = currNode.previous;
			insertNode.previous.next = insertNode;
			currNode.previous = insertNode;
			this.size++;
		}
		return;
	}
	
	/**Searches the collection and returns the index of the first occurrence of the given value
	 * or -1 if the value is not found.
	 * @param value whose index is being searched
	 * @return value of index that is being searched for
	 */
	public int indexOf(Object value) {
		if(value == null) return -1;
		
		int index = 0;
		ListNode currNode = first;
		while(currNode != null) {
			if(currNode.value.equals(value)) 
				return index;
			index++;
			currNode = currNode.next;
		}
		return -1;
	}
	
	/**Removes element at specified index from collection. Legal indexes are 0 to size - 1.
	 * @param index at which the element should be removed
	 * @throws IndexOutOfBoundsException
	 */
	public void remove(int index) {
		if(index < 0 || index > this.size - 1)
			throw new IndexOutOfBoundsException("Index should be between 0 and " + (this.size - 1) + ". Now it is " + index + ".");
		
		int current = 0;
		ListNode currNode = first;
		
		if(index == 0) {
			first = currNode.next;
			first.previous = null;
		}else if(index == this.size - 1) {
			currNode = last;
			last = currNode.previous;
			last.next = null;
		}else {
			while(currNode != null) {
				if(current == index) {
					currNode.previous.next = currNode.next;
					currNode.next.previous = currNode.previous;
				}
				currNode = currNode.next;
				current++;
			}
		}
		
		this.size--;
		return;
	}
	
	/**Returns true only if the collection contains given value, as determined by equals method.
	 * @param value that is being examined if the collection contains it
	 * @return true if the collection contains the value, false if not
	 */
	@Override
	public boolean contains(Object value) {
		ListNode currNode = first;
		while(currNode != null) {
			if(currNode.value.equals(value))
				return true;
			currNode = currNode.next;
		}
		return false;
	}
	
	/**Allocates new array with size equals to the size of this collections, fills it with
	 * collection content and returns the array.
	 * @return Object[] array filled with collection content
	 */
	@Override
	public Object[] toArray() {
		Object[] array = new Object[this.size];
		int i = 0;
		ListNode currNode = first;
		while(currNode != null) {
			array[i] = currNode.value;
			i++;
			currNode = currNode.next;
		}
		return array;
	}
	
	/**Method calls processor.process(.) for each element of this collection. 
	 * The order in which elements will be sent is undefined in this class. 
	 * @param processor that is being called for each element of this collection.
	 */
	@Override
	public void forEach(Processor processor) {
		ListNode currNode = first;
		while(currNode != null) {
			processor.process(currNode.value);
			currNode = currNode.next;
		}
	}
	
	/**Method adds into the current collection all elements from the given collection.
	 * This other collection remains unchanged.
	 * @param other collection that is added onto this collection
	 */
	@Override
	public void addAll(Collection other) {
		
		class LocalProcessor extends Processor{
			
			public LocalProcessor() {
			}
			
			public void process(Object value) {
				add(value);
			}
		}
		
		LocalProcessor processor = new LocalProcessor();
		other.forEach(processor);
	}
	
	/**Returns true only if collection contains given value as determined by equals method 
	 * and removes one occurrence of it.
	 * @param value that is being removed if the collection contains it
	 * @return true if the value is removed, false if not
	 */
	@Override
	public boolean remove(Object value) {
		ListNode currNode = first;
		int current = 0;
		while(currNode != null) {
			if(currNode.value.equals(value)) {
				remove(current);
				return true;
			}
			currNode = currNode.next;
			current++;
		}
		this.size--;
		return false;
	}
	
	/**Prints the content of the list.
	 * 
	 */
	public void print() {
		ListNode currNode = first;
		while(currNode != null) {
			System.out.println(currNode.value);
			currNode = currNode.next;
		}
	}
	
}
