package hr.fer.oprpp1.hw01;

public class ComplexNumber {
	
	private double real;
	private double imaginary;
	
	public ComplexNumber(double real, double imaginary) {
		this.real = real;
		this.imaginary = imaginary;
	}
	
	/**Creates new ComplexNumber only from real parameter. Imaginary part is 0.
	 * @param real part used for the ComplexNumber
	 * @return new ComplexNumber
	 */
	public static ComplexNumber fromReal(double real) {
		ComplexNumber cn = new ComplexNumber(real, 0);
		return cn;
	}
	
	/**Creates new ComplexNumber only from imaginary parameter. Real part is 0.
	 * @param imaginary part for the ComplexNumber
	 * @return new ComplexNumber
	 */
	public static ComplexNumber fromImaginary(double imaginary) {
		ComplexNumber cn = new ComplexNumber(0, imaginary);
		return cn;
	}
	
	/**Creates new ComplexNumber from magnitude and angle.
	 * @param magnitude of the ComplexNumber
	 * @param angle of the ComplexNumber
	 * @return new ComplexNumber
	 */
	public static ComplexNumber fromMagnitudeAndAngle(double magnitude, double angle) {
		ComplexNumber cn = new ComplexNumber(Math.round(magnitude*Math.cos(angle)), Math.round( magnitude*Math.sin(angle)));
		return cn;
	}
	
	/**Creates new ComplexNumber from a string. 
	 * @param s string from which the ComplexNumber is created
	 * @return new ComplexNumber
	 * @throws IllegalArgumentException
	 */
	public static ComplexNumber parse(String s) {
		
		if(!(s.contains("i"))) {
			char[] array = s.toCharArray();
			String result = "";
			int pom = 0;
			boolean hasNumbers = false;
			boolean dot = false;
			if(array[0] == '+' || array[0] == '-') {
				result += String.valueOf(array[0]);
				pom = 1;
			}
			for(int i = pom; i < array.length; i++) {
				if(Character.isDigit(array[i])) {
					result += String.valueOf(array[i]);
					hasNumbers = true;
				}else if(array[i] == '.' && hasNumbers && !dot) {
					result += String.valueOf(array[i]);
					dot = true;
					continue;
				}else {
					throw new IllegalArgumentException("Unable to parse Complex number!");
				}
			}
			
			return fromReal(Double.parseDouble(result));
			
		}else if(s.endsWith("i")) {
			
			if(s.equals("i") || s.equals("+i")) {
				return fromImaginary(1);
			}
			
			if(s.equals("-i")) {
				return fromImaginary(-1);
			}
			
			if(s.split("\\+|-").length > 3) {
				throw new IllegalArgumentException("Unable to parse Complex number!");
			}
			
			String real = "";
			String imaginary = "";
			
			char[] array = s.toCharArray();
			
			if(array[0] == '+' || array[0] == '-') {
				real += String.valueOf(array[0]);
			}
			
			String[] str = s.split("[-+i]");
			int i = 0;
			for(; i < str.length; i++) {
				if(!(str[i].equals(""))) {
					real += str[i];
					break;
				}
			}
			i++;
			
			if(s.contains("-")) {
				int index = s.lastIndexOf("-");
				if(index != 0) {
					imaginary += "-";
				}
			}
			
			if(s.contains("+")) {
				int index = s.lastIndexOf("+");
				if(index != 0) {
					imaginary += "+";
				}
			}
			
			for(; i < str.length; i++) {
				if(!(str[i].equals(""))) {
					imaginary += str[i];
					break;
				}
			}
			
			if(imaginary.equals("-")) {
				imaginary = "-1";
			}else if(imaginary.equals("+")) {
				imaginary = "1";
			}else if(imaginary.equals("")) {
				imaginary = real;
				real = "0";
			}
			
			return new ComplexNumber(Double.parseDouble(real), Double.parseDouble(imaginary));
			
		}else {
			throw new IllegalArgumentException("Unable to parse Complex number!");
		}
		
	}
	
	
	/**Returns real part of the ComplexNumber.
	 * @return real part of ComplexNumber
	 */
	public double getReal() {
		return this.real;
	}
	
	/**Returns imaginary part of the ComplexNumber.
	 * @return imaginary part of the ComplexNumber
	 */
	public double getImaginary() {
		return this.imaginary;
	}
	
	/**Returns magnitude of the ComplexNumber.
	 * @return magnitude of the ComplexNumber.
	 */
	public double getMagnitude() {
		return Math.sqrt(real*real + imaginary*imaginary);
	}
	
	/**Returns angle of the ComlexNumber.
	 * @return angle of the ComplexNumber
	 */
	public double getAngle() {
		return Math.atan(imaginary/real);
	}
	
	/**Adds this and other ComplexNumber to form a new ComplexNumber
	 * @param c other ComplexNumber	
	 * @return new added ComplexNumber
	 */
	public ComplexNumber add(ComplexNumber c) {
		double resReal = this.real + c.real;
		double resImaginary = this.imaginary + c.imaginary;
		return new ComplexNumber(resReal, resImaginary);
	}
	
	/**Subtracts other ComplexNumber from this one. 
	 * @param c other ComplexNumber
	 * @return new ComplexNumber
	 */
	public ComplexNumber sub(ComplexNumber c) {
		double resReal = this.real - c.real;
		double resImaginary = this.imaginary - c.imaginary;
		return new ComplexNumber(resReal, resImaginary);
	}
	
	/**Multiplies this and other ComplexNumber
	 * @param c other ComplexNumber
	 * @return new ComplexNumber
	 */
	public ComplexNumber mul(ComplexNumber c) {
		double resReal = (this.real*c.real - this.imaginary*c.imaginary);
		double resImaginary = (this.real*c.imaginary + this.imaginary*c.real);
		return new ComplexNumber(resReal, resImaginary);
	}
	
	/**Divides this and other ComplexNumber
	 * @param c other ComplexNumber
	 * @return new ComplexNumber
	 */
	public ComplexNumber div(ComplexNumber c) {
		double resReal = (this.real*c.real + this.imaginary*c.imaginary)/(c.real*c.real + c.imaginary*c.imaginary);
		double resImaginary = (this.imaginary*c.real - this.real*c.imaginary)/(c.real*c.real + c.imaginary*c.imaginary);
		return new ComplexNumber(resReal, resImaginary);
	}
	
	/**Returns the value of the ComplexNumber raised to the power of the argument. 
	 * @param n value to which we wish to raise our value to
	 * @return new ComplexNumber
	 */
	public ComplexNumber power(int n) {
		if(n < 0) throw new IllegalArgumentException("Power has to be bigger than 0. It is " + n +".");
		
		double magnitude = this.getMagnitude();
		double angle = this.getAngle();
		double resReal = Math.pow(magnitude, n) * Math.cos(n*angle);
		double resImaginary = Math.pow(magnitude, n) * Math.sin(n*angle);
		
		return new ComplexNumber(Math.round(resReal),Math.round(resImaginary));
	}
	
	/**Returns array of ComplexNumbers after using n-th root on it
	 * @param n root
	 * @return array of ComplexNumbers
	 */
	public ComplexNumber[] root(int n) {
		ComplexNumber[] array  = new ComplexNumber[n];
		double magnitude = this.getMagnitude();
		double angle = this.getAngle();
		
		double rootMagn = Math.pow(magnitude, 1/n);
		
		for(int i = 0; i < n; i++) {
			double resReal = rootMagn*Math.cos((angle+2*i*Math.PI)/n);
			double resImaginary = rootMagn*Math.sin((angle+2*i*Math.PI)/n);
			array[i] = new ComplexNumber(Math.round(resReal),Math.round(resImaginary));
		}
		
		return array;
	}
	
	@Override
	public String toString() {
		String s = "";
		
		if(this.real == 0 && this.imaginary == 0) {
			s = "0.0+0.0i";
			return s;
		}
		
		boolean realZero = true;
		if(this.real != 0) {
			s += Double.toString(this.real);
			realZero = false;
		}
		if(this.imaginary > 0 && !realZero ) s = s + "+";
		
		if(this.imaginary != 0) {
			if(this.imaginary == 1.0) {
				s = s + "i";
			}else if(this.imaginary == -1.0) {
				s = s + "-i";
			}else {
				s = s + Double.toString(this.imaginary) + "i";
			}
		}
		return s;
	}
	
}
