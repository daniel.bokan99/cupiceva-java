package hr.fer.oprpp1.hw01;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class ComplexNumberTest {
	
	@Test
	public void fromRealTest() {
		ComplexNumber cn = ComplexNumber.fromReal(10);
		assertEquals(10, cn.getReal());
	}
	
	@Test
	public void fromImaginaryTest() {
		ComplexNumber cn = ComplexNumber.fromImaginary(10);
		assertEquals(10, cn.getImaginary());
	}
	
	@Test
	public void fromMagnitudeAndAngleTest() {
		ComplexNumber cn = ComplexNumber.fromMagnitudeAndAngle(5, 0.6435029);
		ComplexNumber expected = new ComplexNumber(4,3);
		assertEquals(expected.getReal(), cn.getReal());
		assertEquals(expected.getImaginary(), cn.getImaginary());
	}
	
	
	@Test
	public void getRealTest() {
		ComplexNumber cn = new ComplexNumber(10, 10);
		assertEquals(10, cn.getReal());
	}
	
	@Test
	public void getImaginaryTest() {
		ComplexNumber cn = new ComplexNumber(10, 10);
		assertEquals(10, cn.getImaginary());
	}
	
	@Test
	public void getMagnitudeTest() {
		ComplexNumber cn = new ComplexNumber(10, 10);
		assertEquals(14.142135623730951, cn.getMagnitude());
	}
	
	@Test
	public void getAngleTest() {
		ComplexNumber cn = new ComplexNumber(10, 10);
		assertEquals(0.7853981633974483, cn.getAngle());
	}
	
	@Test
	public void addTest() {
		ComplexNumber cn1 = new ComplexNumber(10, 10);
		ComplexNumber cn2 = new ComplexNumber(11, 11);
		ComplexNumber cn3 = cn1.add(cn2);
		assertEquals(21, cn3.getReal());
		assertEquals(21, cn3.getImaginary());	
	}
	
	@Test
	public void subTest() {
		ComplexNumber cn1 = new ComplexNumber(10, 10);
		ComplexNumber cn2 = new ComplexNumber(11, 11);
		ComplexNumber cn3 = cn1.sub(cn2);
		assertEquals(-1, cn3.getReal());
		assertEquals(-1, cn3.getImaginary());
	}
	
	@Test
	public void mulTest() {
		ComplexNumber cn1 = new ComplexNumber(2, 3);
		ComplexNumber cn2 = new ComplexNumber(1, 5);
		ComplexNumber cn3 = cn1.mul(cn2);
		assertEquals(-13, cn3.getReal());
		assertEquals(13, cn3.getImaginary());
	}
	
	@Test
	public void divTest() {
		ComplexNumber cn1 = new ComplexNumber(1, -3);
		ComplexNumber cn2 = new ComplexNumber(1, 2);
		ComplexNumber cn3 = cn1.div(cn2);
		assertEquals(-1, cn3.getReal());
		assertEquals(-1, cn3.getImaginary());
	}
	
	@Test
	public void powerTest() {
		ComplexNumber cn1 = new ComplexNumber(3,3);
		ComplexNumber cn2 = cn1.power(5);
		assertEquals(-972, cn2.getReal());
		assertEquals(-972, cn2.getImaginary());
	}
	
	@Test
	public void toStringTest() {
		ComplexNumber cn1 = new ComplexNumber(3,3);
		String s = "3.0+3.0i";
		assertEquals(s, cn1.toString());
	}
}
