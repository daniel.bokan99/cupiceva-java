package hr.fer.oprpp1.custom.collections;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class ArrayIndexedCollectionTest {
	
	@Test
	public void simplestConstructorTest() {
		ArrayIndexedCollection col = new ArrayIndexedCollection();
		int expectedSize = 0;
		int actualSize = col.size();
		assertEquals(expectedSize, actualSize);
	}
	
	@Test
	public void negativeCapacityTest() {
		assertThrows(IllegalArgumentException.class, () -> new ArrayIndexedCollection(-5));
	}
	
	@Test
	public void nullColectionTest() {
		assertThrows(NullPointerException.class, () -> new ArrayIndexedCollection(null));
	}
	
	@Test
	public void addingOtherCollectionTest() {
		ArrayIndexedCollection col1 = new ArrayIndexedCollection();
		col1.add(4);
		col1.add("Hello");
		col1.add("World");
		
		ArrayIndexedCollection col2 = new ArrayIndexedCollection(col1);
		assertEquals(col1.get(0), col2.get(0), "Objects at index 0 are different");
		assertEquals(col1.get(1), col2.get(1), "Objects at index 1 are different");
		assertEquals(col1.get(2), col2.get(2), "Objects at index 2 are different");
	}
	
	@Test
	public void addingNullObjectsTest() {
		assertThrows(NullPointerException.class, () -> {
		ArrayIndexedCollection col = new ArrayIndexedCollection();
		col.add(null);
		});
	}
	
	@Test
	public void addingObjectsTest() {
		assertDoesNotThrow(() -> {
			ArrayIndexedCollection col = new ArrayIndexedCollection(2);
			col.add(1);
			col.add(2);
			col.add(3);
		});
	}
	
	@Test
	public void getOutOfBoundsTest() {
		assertThrows(IndexOutOfBoundsException.class, () -> {
			ArrayIndexedCollection col = new ArrayIndexedCollection(2);
			col.add(1);
			col.add(2);
			col.add(3);
			col.get(6);
		});
	}
	
	@Test
	public void getTest() {
		ArrayIndexedCollection col = new ArrayIndexedCollection(2);
		col.add(1);
		col.add(2);
		col.add(3);
		assertEquals(1, col.get(0));
	}
	
	@Test
	public void clearTest() {
		ArrayIndexedCollection col = new ArrayIndexedCollection(2);
		col.add(1);
		col.add(2);
		col.add(3);
		col.clear();
		assertEquals(0, col.size());
	}
	
	@Test
	public void insertIndexOutOfBoundsTest() {
		assertThrows(IndexOutOfBoundsException.class, () -> {
			ArrayIndexedCollection col = new ArrayIndexedCollection(2);
			col.insert(10, 4);
		});
	}
	
	@Test
	public void insertTest() {
		ArrayIndexedCollection col = new ArrayIndexedCollection(2);
		col.add(1);
		col.add(2);
		col.add(3);
		col.insert(10, 2);
		assertEquals(10, col.get(2));
	}
	
	@Test
	public void indexOfTest() {
		ArrayIndexedCollection col = new ArrayIndexedCollection(2);
		col.add(1);
		col.add(2);
		col.add(3);
		assertEquals(1, col.indexOf(2));
	}
	
	@Test
	public void indexOfNullTest() {
		ArrayIndexedCollection col = new ArrayIndexedCollection(2);
		col.add(1);
		col.add(2);
		col.add(3);
		assertEquals(-1, col.indexOf(null));
	}
	
	@Test
	public void indexOfObjectNotFoundTest() {
		ArrayIndexedCollection col = new ArrayIndexedCollection(2);
		col.add(1);
		col.add(2);
		col.add(3);
		assertEquals(-1, col.indexOf(5));
	}
	
	@Test
	public void removeIndexOutOfBoundsTest() {
		assertThrows(IndexOutOfBoundsException.class, () -> {
			ArrayIndexedCollection col = new ArrayIndexedCollection(2);
			col.add(1);
			col.add(2);
			col.add(3);
			col.remove(8);
		});
	}
	
	@Test
	public void removeTest() {
		ArrayIndexedCollection col = new ArrayIndexedCollection(2);
		col.add(1);
		col.add(2);
		col.add(3);
		col.remove(0);
		assertEquals(2, col.get(0));
	}
	
}
