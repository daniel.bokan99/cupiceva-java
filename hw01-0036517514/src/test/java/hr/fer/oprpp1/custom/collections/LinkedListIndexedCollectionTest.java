package hr.fer.oprpp1.custom.collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class LinkedListIndexedCollectionTest {

	@Test
	public void simpleConstructorTest() {
		LinkedListIndexedCollection list = new LinkedListIndexedCollection();
		assertEquals(0, list.size());
	}
	
	@Test
	public void addingOtherCollectionConstructorTest() {
		LinkedListIndexedCollection list1 = new LinkedListIndexedCollection();
		list1.add(1);
		list1.add(2);
		LinkedListIndexedCollection list2 = new LinkedListIndexedCollection(list1);
		assertEquals(list1.get(0), list2.get(0));
		
	}
	
	@Test
	public void addingNullObjectTest() {
		assertThrows(NullPointerException.class, () -> {
			LinkedListIndexedCollection list = new LinkedListIndexedCollection();
			list.add(null);
		});
	}
	
	@Test
	public void addingTest() {
		LinkedListIndexedCollection list = new LinkedListIndexedCollection();
		list.add(2);
		assertEquals(2,list.get(0));
	}
	
	@Test
	public void getObjectIndexOutOfBoundsTest() {
		assertThrows(IndexOutOfBoundsException.class, () -> {
			LinkedListIndexedCollection list = new LinkedListIndexedCollection();
			list.add(1);
			list.add(2);
			list.get(5);
		});
	}
	
	@Test
	public void getTest() {
		LinkedListIndexedCollection list = new LinkedListIndexedCollection();
		list.add(2);
		list.add(4);
		list.add(6);
		assertEquals(6,list.get(2));
	}
	
	@Test
	public void clearTest() {
		LinkedListIndexedCollection list = new LinkedListIndexedCollection();
		list.add(2);
		list.add(4);
		list.add(6);
		list.clear();
		assertEquals(0, list.size());
	}
	
	@Test
	public void insertAtTheBegginingTest() {
		LinkedListIndexedCollection list = new LinkedListIndexedCollection();
		list.add(2);
		list.add(4);
		list.add(6);
		list.insert(10, 0);
		assertEquals(10,list.get(0));
	}
	
	@Test
	public void insertInTheMiddleTest() {
		LinkedListIndexedCollection list = new LinkedListIndexedCollection();
		list.add(2);
		list.add(4);
		list.add(6);
		list.insert(10, 1);
		assertEquals(10,list.get(1));
	}
	
	@Test
	public void insertAtTheEndTest() {
		LinkedListIndexedCollection list = new LinkedListIndexedCollection();
		list.add(2);
		list.add(4);
		list.add(6);
		list.insert(10,2);
		assertEquals(10,list.get(2));
	}
	
	@Test
	public void indexOfOutOfBoundsTest() {
		LinkedListIndexedCollection list = new LinkedListIndexedCollection();
		list.add(2);
		list.add(4);
		list.add(6);
		assertEquals(-1, list.indexOf(10));
	}
	
	@Test
	public void indexOfTest() {
		LinkedListIndexedCollection list = new LinkedListIndexedCollection();
		list.add(2);
		list.add(4);
		list.add(6);
		assertEquals(1, list.indexOf(4));
	}
	
	@Test
	public void removeOutOfBoundsTest() {
		assertThrows(IndexOutOfBoundsException.class, () -> {
			LinkedListIndexedCollection list = new LinkedListIndexedCollection();
			list.add(1);
			list.add(2);
			list.remove(10);
		});
	}
	
	@Test
	public void removeAtTheBegginingTest() {
		LinkedListIndexedCollection list = new LinkedListIndexedCollection();
		list.add(2);
		list.add(4);
		list.add(6);
		list.remove(0);
		assertEquals(4, list.get(0));
	}
	
	@Test
	public void removeInTheMiddleTest() {
		LinkedListIndexedCollection list = new LinkedListIndexedCollection();
		list.add(2);
		list.add(4);
		list.add(6);
		list.remove(1);
		assertEquals(6, list.get(1));
	}
	
	@Test
	public void removeAtTheEndTest() {
		LinkedListIndexedCollection list = new LinkedListIndexedCollection();
		list.add(2);
		list.add(4);
		list.add(6);
		list.remove(2);
		assertEquals(4, list.get(1));
	}
}
